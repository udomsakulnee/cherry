import resources, { LOCALE_THAI } from "i18n";
import { KEY_LOCALE } from "config";
import { getStorage } from "utils";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { isEmpty } from "lodash";

const locale = getStorage(KEY_LOCALE);
i18n.use(initReactI18next).init({
  resources,
  lng: !isEmpty(locale) ? locale : LOCALE_THAI,
  keySeparator: false,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
