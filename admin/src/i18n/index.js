import EN from "./locales/en.json";
import TH from "./locales/th.json";

export const LOCALE_ENGLISH = "EN";
export const LOCALE_THAI = "TH";

const resources = {
  [LOCALE_ENGLISH]: {
    translation: EN,
  },
  [LOCALE_THAI]: {
    translation: TH,
  },
};
export default resources;
