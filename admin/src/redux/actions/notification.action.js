import { CLEAR_NOTIFY, NOTIFY, HIDE_NOTIFY } from "../constants";

export const notify = (payload) => ({
  type: NOTIFY,
  payload: { ...payload, type: "default" },
});

export const notifyInfo = (payload) => ({
  type: NOTIFY,
  payload: { ...payload, type: "info" },
});

export const notifySuccess = (payload) => ({
  type: NOTIFY,
  payload: { ...payload, type: "success" },
});

export const notifyWarning = (payload) => ({
  type: NOTIFY,
  payload: { ...payload, type: "warning" },
});

export const notifyError = (payload) => ({
  type: NOTIFY,
  payload: { ...payload, type: "danger" },
});

export const hideNotify = () => ({
  type: HIDE_NOTIFY,
});

export const clearNotify = () => ({
  type: CLEAR_NOTIFY,
});
