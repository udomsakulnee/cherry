import { HIDE_ALERT, SHOW_ALERT } from "../constants";

export const showAlert = (payload) => ({
  type: SHOW_ALERT,
  payload: { ...payload, type: "default" },
});

export const showAlertInfo = (payload) => ({
  type: SHOW_ALERT,
  payload: { ...payload, type: "info" },
});

export const showAlertSuccess = (payload) => ({
  type: SHOW_ALERT,
  payload: { ...payload, type: "success" },
});

export const showAlertWarning = (payload) => ({
  type: SHOW_ALERT,
  payload: { ...payload, type: "warning" },
});

export const showAlertError = (payload) => ({
  type: SHOW_ALERT,
  payload: { ...payload, type: "danger" },
});

export const showConfirm = (payload) => ({
  type: SHOW_ALERT,
  payload: { ...payload, type: "confirm" },
});

export const hideAlert = () => ({
  type: HIDE_ALERT,
});
