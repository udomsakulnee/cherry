import { CLEAR_USER, SET_USER } from "../constants";

export const setUser = (payload) => ({
  type: SET_USER,
  payload: payload,
});

export const clearUser = () => ({
  type: CLEAR_USER,
});
