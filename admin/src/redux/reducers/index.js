import alert from "./alert.reducer";
import { combineReducers } from "redux";
import notification from "./notification.reducer";
import spinner from "./spinner.reducer";
import user from "./user.reducer";

export default combineReducers({
  user,
  alert,
  notification,
  spinner,
});
