import { CLEAR_USER, SET_USER } from "../constants";
import { KEY_USER } from "config";
import { getSession } from "utils";

const initialState = {
  ...getSession(KEY_USER),
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER:
      return payload;

    case CLEAR_USER:
      return {};

    default:
      return state;
  }
};
