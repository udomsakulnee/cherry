import { HIDE_SPINNER, SHOW_SPINNER } from "../constants";

const initialState = {
  isShow: false,
};

export default (state = initialState, { type }) => {
  switch (type) {
    case SHOW_SPINNER:
      return { isShow: true };

    case HIDE_SPINNER:
      return { isShow: false };

    default:
      return state;
  }
};
