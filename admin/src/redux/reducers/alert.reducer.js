import { HIDE_ALERT, SHOW_ALERT } from "../constants";

const initialState = {
  isShow: false,
  title: "",
  text: "",
  type: "",
  confirmText: "",
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SHOW_ALERT:
      return { ...initialState, ...payload, isShow: true };

    case HIDE_ALERT:
      return initialState;

    default:
      return state;
  }
};
