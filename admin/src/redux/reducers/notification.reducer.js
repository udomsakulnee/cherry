import { CLEAR_NOTIFY, NOTIFY, HIDE_NOTIFY } from "../constants";

const initialState = {
  title: "",
  text: "",
  type: "",
  isShow: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case NOTIFY:
      return { ...initialState, ...payload, isShow: true };

    case HIDE_NOTIFY:
      return { ...state, isShow: false };

    case CLEAR_NOTIFY:
      return initialState;

    default:
      return state;
  }
};
