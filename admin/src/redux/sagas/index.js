import notificationSaga from "./notification.saga";
import userSaga from "./user.saga";
import { fork } from "redux-saga/effects";

// eslint-disable-next-line import/no-anonymous-default-export
export default function* () {
  // eslint-disable-next-line no-unused-expressions
  yield fork(userSaga);
  yield fork(notificationSaga);
}
