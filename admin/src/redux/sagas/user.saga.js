import { SET_USER } from "../constants";
import { takeEvery } from "redux-saga/effects";
import { KEY_USER } from "config";
import { setSession } from "utils";

function* setUserSaga({ payload }) {
  setSession(KEY_USER, payload);
}

export default function* userSaga() {
  yield takeEvery(SET_USER, setUserSaga);
}
