import { NOTIFY } from "../constants";
import { takeEvery, delay, put } from "redux-saga/effects";
import { clearNotify } from "../actions/notification.action";

function* notifySaga() {
  yield delay(1000);
  yield put(clearNotify());
}

export default function* notificationSaga() {
  yield takeEvery(NOTIFY, notifySaga);
}
