export const Colors = {
  default: "#172b4d",
  primary: "#5e72e4",
  secondary: "#f4f5f7",
  info: "#11cdef",
  success: "#2dce89",
  danger: "#f5365c",
  warning: "#fb6340",
};
