import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { Notification, SweetAlert } from "components/Alert";
import AdminLayout from "layouts/Admin";
import AuthLayout from "layouts/Auth";
import { ENV } from "config";
import PrivacyPolicyView from "views/PrivacyPolicy";
import React from "react";
import { Spinner } from "components/Shared";
import { isEmpty } from "lodash";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";

const App = () => {
  const { t } = useTranslation();
  const user = useSelector((state) => state.user);
  const isLogin = !isEmpty(user);

  return (
    <BrowserRouter>
      {ENV === "development" && (
        <h3 className="for-test-only">{t("FOR_TEST_ONLY")}</h3>
      )}
      <Spinner />
      <Notification />
      <SweetAlert />
      <Switch>
        {isLogin && (
          <Route path="/admin" render={(props) => <AdminLayout {...props} />} />
        )}
        <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
        <Route
          path="/privacy-policy"
          render={(props) => <PrivacyPolicyView {...props} />}
        />
        <Redirect from="*" to={isLogin ? "/admin" : "/auth"} />
      </Switch>
    </BrowserRouter>
  );
};
export default App;
