import { getSession, setSession, clearSession } from "utils";
import { API_URL, KEY_TOKEN, KEY_USER, KEY_AUTH } from "config";
import axios from "axios";
import { isEmpty } from "lodash";

// const refreshToken = async () => {
//   const token = getSession(KEY_TOKEN);
//   const {status, data} = await axios.post(`${API_URL}/users-permissions/refreshtoken`, {token});
//   if (status === 200) {
//     const {jwt, user} = data;
//     setSession(KEY_TOKEN, jwt);
//     setSession(KEY_USER, user);
//     return jwt;
//   }
//   return null;
// };

const refreshToken = async () => {
  try {
    const request = getSession(KEY_AUTH);
    const { status, data } = await axios.post(`${API_URL}/auth/local`, request);
    if (status === 200) {
      const { jwt, user } = data;
      setSession(KEY_TOKEN, jwt);
      setSession(KEY_USER, user);
      return jwt;
    }
    clearSession();
    // eslint-disable-next-line no-restricted-globals
    location.reload();
    return;
  } catch (err) {
    clearSession();
    // eslint-disable-next-line no-restricted-globals
    location.reload();
    return;
  }
};

const defaultOptions = {
  baseURL: API_URL,
  method: "get",
  headers: {
    "Content-Type": "application/json; charset=utf-8",
  },
};

const axiosInstance = axios.create(defaultOptions);

axiosInstance.interceptors.request.use(
  async (config) => {
    let newConfig = {};
    const token = getSession(KEY_TOKEN);
    if (!isEmpty(token)) {
      newConfig = {
        ...config,
        headers: { authorization: `Bearer ${token}` },
      };
      return newConfig;
    } else {
      return config;
    }
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    if (error.response && error.response.status === 401) {
      const token = await refreshToken();
      const config = {
        ...error.config,
        headers: { ...error.config.headers, authorization: `Bearer ${token}` },
      };
      return axiosInstance(config);
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;
