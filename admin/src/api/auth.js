import axios from "./interceptor";
import { KEY_AUTH, KEY_TOKEN } from "config";
import { setSession } from "utils";
import { get } from "lodash";

export const loginAPI = async (identifier, password) => {
  try {
    const request = {
      identifier,
      password,
    };
    const { status, data } = await axios.post("/auth/local", request);
    if (status === 200) {
      const { jwt, user } = data;
      setSession(KEY_TOKEN, jwt);
      setSession(KEY_AUTH, request);
      return user;
    }
  } catch (err) {
    const message = get(err, "response.data.data[0].messages[0].message");
    throw message;
  }
};

export const registerAPI = async (request) => {
  try {
    const { status, data } = await axios.post("/auth/local/register", request);
    if (status === 200) {
      return data;
    }
  } catch (err) {
    const message = get(err, "response.data.data[0].messages[0].message");
    throw message;
  }
};

export const getUserAPI = async (token) => {
  try {
    const { status, data } = await axios.post("/users-permissions/user", {
      token,
    });
    if (status === 200) {
      return data;
    }
  } catch (err) {}
};
