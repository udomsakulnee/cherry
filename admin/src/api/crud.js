import axios from "./interceptor";
import { KEY_LIMIT, API_UPLOAD } from "config";
import { hideSpinner, showSpinner } from "redux/actions";
import { stringify } from "query-string";
import { getStorage } from "utils";

export const findAll = async (contentType, params = {}, dispatch) => {
  try {
    dispatch && dispatch(showSpinner());
    const queryString = stringify({ _sort: "id:desc", ...params });
    const response = await axios.get(`${contentType}?${queryString}`);
    const { status, data } = response;
    if (status === 200) {
      dispatch && dispatch(hideSpinner());
      return data;
    }
    dispatch && dispatch(hideSpinner());
    return [];
  } catch (err) {
    dispatch && dispatch(hideSpinner());
    return [];
  }
};

export const findOne = async (contentType, id, dispatch) => {
  try {
    dispatch && dispatch(showSpinner());
    const response = await axios.get(`${contentType}/${id}`);
    const { status, data } = response;
    if (status === 200) {
      dispatch && dispatch(hideSpinner());
      return data;
    }
    dispatch && dispatch(hideSpinner());
    return null;
  } catch (err) {
    dispatch && dispatch(hideSpinner());
    return null;
  }
};

export const findLimit = async (contentType, params = {}, dispatch) => {
  try {
    dispatch && dispatch(showSpinner());
    const defaultLimit = getStorage(KEY_LIMIT);
    const queryString = stringify({
      _start: 0,
      _limit: defaultLimit || 10,
      _sort: "id:desc",
      ...params,
    });
    const response = await axios.get(`${contentType}/limit?${queryString}`);
    const { status, data } = response;
    if (status === 200) {
      dispatch && dispatch(hideSpinner());
      return data;
    }
    dispatch && dispatch(hideSpinner());
    return [];
  } catch (err) {
    dispatch && dispatch(hideSpinner());
    return [];
  }
};

export const countRecords = async (contentType) => {
  try {
    const { status, data } = await axios.get(`${contentType}/count`);
    if (status === 200) {
      return data;
    }
    return -1;
  } catch (err) {
    return -1;
  }
};

export const createData = async (contentType, params = {}, dispatch) => {
  try {
    dispatch && dispatch(showSpinner());
    const { status, data } = await axios.post(contentType, params);
    if (status === 200) {
      dispatch && dispatch(hideSpinner());
      return data;
    }
    dispatch && dispatch(hideSpinner());
    return null;
  } catch (err) {
    dispatch && dispatch(hideSpinner());
    return null;
  }
};

export const updateData = async (contentType, id, params = {}, dispatch) => {
  try {
    dispatch && dispatch(showSpinner());
    const { status, data } = await axios.put(`${contentType}/${id}`, params);
    if (status === 200) {
      dispatch && dispatch(hideSpinner());
      return data;
    }
    dispatch && dispatch(hideSpinner());
    return null;
  } catch (err) {
    dispatch && dispatch(hideSpinner());
    return null;
  }
};

export const deleteData = async (contentType, id, dispatch) => {
  try {
    dispatch && dispatch(showSpinner());
    const { status, data } = await axios.delete(`${contentType}/${id}`);
    if (status === 200) {
      dispatch && dispatch(hideSpinner());
      return data;
    }
    dispatch && dispatch(hideSpinner());
    return null;
  } catch (err) {
    dispatch && dispatch(hideSpinner());
    return null;
  }
};

export const uploadFile = async (files) => {
  try {
    const config = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    const formData = new FormData();
    formData.append("files", files);
    const { status, data } = await axios.post(API_UPLOAD, formData, config);
    if (status === 200) {
      return data;
    }
    return [];
  } catch (err) {
    return [];
  }
};

export const deleteUploadFile = async (id) => {
  try {
    const { status, data } = await axios.delete(`${API_UPLOAD}/files/${id}`);
    if (status === 200) {
      return data;
    }
    return null;
  } catch (err) {
    return null;
  }
};
