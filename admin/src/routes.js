// import Alternative from 'views/pages/dashboards/Alternative';
// import Dashboard from 'views/pages/dashboards/Dashboard';
import Cart from "pages/Admin/Product/Cart";
import Catalog from "pages/Admin/Product/Catalog";
import Dealer from "pages/Admin/Dealer/Dealer";
import DealerDetail from "pages/Admin/Dealer/DealerDetail";
import GoodsIssued from "pages/Admin/Product/GoodsIssued";
import GoodsIssuedDetail from "pages/Admin/Product/GoodsIssuedDetail";
import GoodsReceived from "pages/Admin/Product/GoodsReceived";
import GoodsReceivedDetail from "pages/Admin/Product/GoodsReceivedDetail";
import Login from "pages/Auth/Login/Login";
import Order from "pages/Admin/Product/Order";
import OrderDetail from "pages/Admin/Product/OrderDetail";
import Product from "pages/Admin/Product/Product";
import ProductDetail from "pages/Admin/Product/ProductDetail";
import Profile from "pages/Admin/Profile/Profile";
import Register from "pages/Auth/Register/Register";
import ReportProductSales from "pages/Admin/Report/ReportProductSales";
import Version from "pages/Admin/Version/Version";

const routes = [
  {
    collapse: true,
    views: [
      {
        path: "/login",
        component: Login,
        layout: "/auth",
      },
      {
        path: "/register/:id",
        component: Register,
        layout: "/auth",
      },
      {
        path: "/register",
        component: Register,
        layout: "/auth",
      },
    ],
  },
];
export default routes;

export const adminRoutes = [
  // {
  //   collapse: true,
  //   name: 'DASHBOARD',
  //   icon: 'ni ni-shop text-primary',
  //   state: 'dashboardsCollapse',
  //   views: [
  //     {
  //       path: '/dashboard',
  //       name: 'DASHBOARD',
  //       miniName: 'D',
  //       component: Dashboard,
  //       layout: '/admin',
  //     },
  //   ],
  // },
  {
    collapse: true,
    name: "PRODUCT",
    icon: "ni ni-app text-red",
    state: "productsCollapse",
    views: [
      {
        path: "/products/:id",
        component: ProductDetail,
        layout: "/admin",
      },
      {
        path: "/products",
        name: "PRODUCT__LIST",
        miniName: "P",
        component: Product,
        layout: "/admin",
      },
      {
        path: "/goods-received/:id",
        component: GoodsReceivedDetail,
        layout: "/admin",
      },
      {
        path: "/goods-received",
        name: "GOODS__RECEIVED",
        miniName: "R",
        component: GoodsReceived,
        layout: "/admin",
      },
      {
        path: "/goods-issued/:id",
        component: GoodsIssuedDetail,
        layout: "/admin",
      },
      {
        path: "/goods-issued",
        name: "GOODS__ISSUED",
        miniName: "I",
        component: GoodsIssued,
        layout: "/admin",
      },
      {
        path: "/orders/:id",
        component: OrderDetail,
        layout: "/admin",
      },
      {
        path: "/orders",
        name: "ORDER__LIST",
        miniName: "O",
        component: Order,
        layout: "/admin",
      },
    ],
  },
  {
    collapse: true,
    name: "REPORT",
    icon: "ni ni-paper-diploma text-primary",
    state: "reportsCollapse",
    views: [
      {
        path: "/report/product-sales",
        name: "REPORT__PRODUCT_SALES",
        miniName: "R",
        component: ReportProductSales,
        layout: "/admin",
      },
    ],
  },
  {
    collapse: true,
    name: "DEALER",
    icon: "ni ni-single-02 text-success",
    state: "dealersCollapse",
    views: [
      {
        path: "/dealers/:id",
        component: DealerDetail,
        layout: "/admin",
      },
      {
        path: "/dealers",
        name: "DEALER__LIST",
        miniName: "D",
        component: Dealer,
        layout: "/admin",
      },
    ],
  },
  {
    collapse: true,
    views: [
      {
        path: "/profile",
        component: Profile,
        layout: "/admin",
      },
      {
        path: "/version",
        component: Version,
        layout: "/admin",
      },
    ],
  },
];

export const dealerRoutes = [
  // {
  //   collapse: true,
  //   name: 'DASHBOARD',
  //   icon: 'ni ni-shop text-primary',
  //   state: 'dashboardsCollapse',
  //   views: [
  //     {
  //       path: '/dashboard',
  //       name: 'DASHBOARD',
  //       miniName: 'D',
  //       component: Dashboard,
  //       layout: '/admin',
  //     },
  //   ],
  // },
  {
    collapse: true,
    name: "PRODUCT",
    icon: "ni ni-app text-red",
    state: "productsCollapse",
    views: [
      {
        path: "/products",
        name: "PRODUCT__LIST",
        miniName: "P",
        component: Catalog,
        layout: "/admin",
      },
      {
        path: "/cart",
        component: Cart,
        layout: "/admin",
      },
      {
        path: "/orders/:id",
        component: OrderDetail,
        layout: "/admin",
      },
      {
        path: "/orders",
        name: "ORDER__LIST",
        miniName: "O",
        component: Order,
        layout: "/admin",
      },
    ],
  },
  {
    collapse: true,
    views: [
      {
        path: "/version",
        component: Version,
        layout: "/admin",
      },
    ],
  },
  {
    collapse: true,
    views: [
      {
        path: "/profile",
        component: Profile,
        layout: "/admin",
      },
      {
        path: "/version",
        component: Version,
        layout: "/admin",
      },
    ],
  },
];
