import { API_URL, API_USER_ME } from "config";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardImg,
  CardTitle,
  Col,
  Container,
  Form,
  Row,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { findAll, updateData } from "api";
import { notifyError, notifySuccess, setUser } from "redux/actions";
import { useDispatch, useSelector } from "react-redux";
import ChangePasswordCard from "components/ChangePasswordCard/ChangePasswordCard";
import DealerCard from "components/DealerCard/DealerCard";
import { FormInput } from "components/Forms";
import { NumberFormat } from "components/Shared";
import UploadModal from "components/Modal/UploadModal";
import { get } from "lodash";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
const Avatar = styled.img`
  width: 170px;
  height: 170px;
  padding: 4px;
  background-color: #fff;
  object-fit: cover;
`;

const IconCamera = styled.i`
  right: 10px;
  bottom: 10px;
  font-size: 30px;
  color: #333;
  background-color: #eee;
  padding: 8px;
  border-radius: 100%;
`;

const Profile = () => {
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm();
  const imgCoverProfile = require("assets/img/theme/img-1-1000x600.jpg");
  const bgCover = require("assets/img/theme/profile-cover.jpg");
  const [userMe, setUserMe] = useState({});
  const [info, setInfo] = useState({});
  const [isEdit, setIsEdit] = useState(false);
  const [isOpenUploadModal, setIsOpenUploadModal] = useState(false);
  const user = useSelector((state) => state.user);
  const { avatar, mSex } = user;

  let userAvatar = get(avatar, "url", "");
  if (userAvatar) {
    userAvatar = `${API_URL}${userAvatar}`;
  } else {
    const sex = get(mSex, "nameEN", "Male").toLowerCase();
    userAvatar = require(`assets/img/icons/common/${sex}.svg`);
  }

  const extractData = (data) => ({
    ...data,
    parent: get(data, "parent.id"),
    role: get(data, "role.id"),
    mSex: get(data, "mSex.id"),
    mLevel: get(data, "mLevel.id"),
  });

  useEffect(() => {
    const getUser = async () => {
      const response = await findAll(API_USER_ME);
      setUserMe(response.orderSummary);
      const data = extractData(response);
      setInfo(data);
      reset(data);
    };

    getUser();
  }, [reset]);

  const onSubmit = async (data) => {
    const response = await updateData(
      API_USER_ME,
      "",
      {
        email: data.email,
        facebook: data.facebook,
        firstnameEN: data.firstnameEN,
        firstnameTH: data.firstnameTH,
        idcard: data.idcard,
        instagram: data.instagram,
        lastnameEN: data.lastnameEN,
        lastnameTH: data.lastnameTH,
        line: data.line,
        nicknameEN: data.nicknameEN,
        nicknameTH: data.nicknameTH,
        phoneNumber: data.phoneNumber,
      },
      dispatch
    );
    if (response) {
      const data = extractData(response);
      setInfo(data);
      reset(data);
      dispatch(setUser(response));

      const message = {
        text: t("UPDATE_SUCCESS"),
      };
      dispatch(notifySuccess(message));
      setIsEdit(false);
    } else {
      const message = {
        text: t("UPDATE_FAIL"),
      };
      dispatch(notifyError(message));
    }
  };

  const openUploadModal = (isOpen) => {
    setIsOpenUploadModal(isOpen);
  };

  const handleOnChange = async (value) => {
    let data = { avatar: null };
    if (value.length > 0) {
      data = { ...data, avatar: value[0].id };
    }

    const response = await updateData(API_USER_ME, "", data, dispatch);
    if (response) {
      dispatch(setUser(response));
    }
  };

  return (
    <>
      <div
        className="header pb-6 d-flex align-items-center"
        style={{
          minHeight: "120px",
          backgroundImage: `url(${bgCover})`,
          backgroundSize: "cover",
          backgroundPosition: "center top",
        }}
      >
        <span className="mask bg-gradient-default opacity-8"></span>
      </div>
      <Container className="mt--6" fluid>
        <Row className="mb-4">
          <Col xl="4">
            <Card className="card-profile position-relative">
              <CardImg
                src={imgCoverProfile.default}
                alt="Image placeholder"
                className="card-img-top"
                style={{ minHeight: "100px", maxHeight: "225px" }}
              />
              <Row className="d-flex justify-content-center row position-relative">
                <Col className="d-flex justify-content-center col-lg-3 order-lg-2 row col">
                  <div
                    className="d-flex justify-content-center position-absolute"
                    style={{ cursor: "pointer", top: "-100px" }}
                    onClick={() => openUploadModal(true)}
                  >
                    <Avatar
                      src={`${userAvatar}`}
                      className="rounded-circle avatar"
                    />
                    <div className="d-flex justify-content-center">
                      <IconCamera className="fas fa-camera position-absolute" />
                    </div>
                  </div>
                </Col>
              </Row>
              <div
                className="text-center pt-7"
                style={{
                  height: "225px",
                }}
              >
                <h2>
                  {info[`firstname${i18n.language}`]}{" "}
                  {info[`lastname${i18n.language}`]}
                  <span className="font-weight-light"></span>
                </h2>
                <p>{user.mLevel[`name${i18n.language}`]}</p>
              </div>
            </Card>
            <div className="card-wrapper">
              {user.mLevel.level > 1 && user.mLevel.level < 12 && (
                <Card>
                  <CardHeader>
                    <h3 className="mb-0">{t("DEALER_CARD")}</h3>
                  </CardHeader>
                  <CardBody>
                    <DealerCard />
                  </CardBody>
                </Card>
              )}
            </div>
            <ChangePasswordCard />
          </Col>
          <Col xl="8">
            <Row>
              <Col lg="6">
                <Card className="bg-gradient-info border-0">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          className="text-uppercase text-muted mb-0 text-white"
                          tag="h5"
                        >
                          {t("TOTAL_PRICE")}
                        </CardTitle>
                        <span className="h2 font-weight-bold mb-0 text-white">
                          <NumberFormat value={userMe.totalPrice} />
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                          <i className="ni ni-money-coins"></i>
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
              <Col lg="6">
                <Card className="bg-gradient-success border-0" tag="h5">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle className="text-uppercase text-muted mb-0 text-white">
                          {t("POINT")}
                        </CardTitle>
                        <span className="h2 font-weight-bold mb-0 text-white">
                          <NumberFormat value={userMe.totalPoint} />
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                          <i className="ni ni-trophy"></i>
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            <Card>
              <CardHeader>
                <div className="row align-items-center">
                  <div className="col-8">
                    <h3 className="mb-0">{t("EDIT_PROFILE")}</h3>
                  </div>
                  {!isEdit && (
                    <div className="col-4 text-right">
                      <a
                        href="javascript:void(0)"
                        onClick={() => setIsEdit(!isEdit)}
                      >
                        <i className="fas fa-pen" /> {t("EDIT")}
                      </a>
                    </div>
                  )}
                </div>
              </CardHeader>
              <CardBody>
                <Form onSubmit={handleSubmit(onSubmit)}>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("FIRSTNAME")} (${t("EN")}) `}
                        name="firstnameEN"
                        control={control}
                        displayType={!isEdit ? "text" : "input"}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("LASTNAME")} (${t("EN")}) `}
                        name="lastnameEN"
                        control={control}
                        displayType={!isEdit ? "text" : "input"}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("FIRSTNAME")} (${t("TH")}) `}
                        name="firstnameTH"
                        control={control}
                        displayType={!isEdit ? "text" : "input"}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("LASTNAME")} (${t("TH")}) `}
                        name="lastnameTH"
                        control={control}
                        displayType={!isEdit ? "text" : "input"}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("NICKNAME")} (${t("EN")}) `}
                        name="nicknameEN"
                        displayType={!isEdit ? "text" : "input"}
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("NICKNAME")} (${t("TH")}) `}
                        name="nicknameTH"
                        displayType={!isEdit ? "text" : "input"}
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("EMAIL")}
                        name="email"
                        type="email"
                        control={control}
                        displayType={!isEdit ? "text" : "input"}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("IDCARD")}
                        name="idcard"
                        type="idcard"
                        control={control}
                        displayType={!isEdit ? "text" : "input"}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("PHONE_NUMBER")}
                        name="phoneNumber"
                        type="tel"
                        control={control}
                        displayType={!isEdit ? "text" : "input"}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("LINE")}
                        name="line"
                        displayType={!isEdit ? "text" : "input"}
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("FACEBOOK")}
                        name="facebook"
                        displayType={!isEdit ? "text" : "input"}
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("INSTAGRAM")}
                        name="instagram"
                        displayType={!isEdit ? "text" : "input"}
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} className="text-right">
                      {isEdit && (
                        <>
                          <Button
                            className="btn-icon btn-3"
                            color="success"
                            type="submit"
                          >
                            <span className="btn-inner--icon">
                              <i className="fas fa-save"></i>
                            </span>
                            <span className="btn-inner--text">{t("SAVE")}</span>
                          </Button>
                          <Button
                            className="btn-icon btn-3"
                            color="secondary"
                            type="button"
                            onClick={() => setIsEdit(!isEdit)}
                          >
                            <span className="btn-inner--icon">
                              <i className="fa fa-ban"></i>
                            </span>
                            <span className="btn-inner--text">
                              {t("CANCEL")}
                            </span>
                          </Button>{" "}
                        </>
                      )}
                    </Col>
                  </Row>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {isOpenUploadModal && (
          <UploadModal
            isOpenModal={isOpenUploadModal}
            onClose={openUploadModal}
            onChange={handleOnChange}
            value={avatar}
          />
        )}
      </Container>
    </>
  );
};

export default Profile;
