import { API_REPORT_PRODUCT_SALES } from "config";
import { Card, CardHeader, Container, Row } from "reactstrap";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { NumberFormat } from "components/Shared";
import SimpleHeader from "components/Headers/SimpleHeader";
import { SmartTable } from "components/Shared";
import { dayjs } from "utils";
import { findAll } from "api";
import { get } from "lodash";
import { useTranslation } from "react-i18next";

const ReportProductSales = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const [total, setTotal] = useState(0);
  const [reportElement, setReportElement] = useState([]);
  const [reportExcel, setReportExcel] = useState([]);
  const [summary, setSummary] = useState([0, 0, 0, 0, 0, 0, 0]);

  const getReport = async (params) => {
    const data = await findAll(API_REPORT_PRODUCT_SALES, params, dispatch);
    if (data.length > 0) {
      generateReport(data);
      setTotal(data.length);
    } else {
      setReportElement([]);
      setReportExcel([]);
      setTotal(0);
    }
  };

  const generateReport = (data) => {
    let totalQuantity = 0;
    let totalWholeSalePrice = 0;
    let totalShipingFee = 0;
    let totalProductCost = 0;
    let totalShipingCost = 0;
    let totalPackageCost = 0;

    const dataExcel = [];
    const reportEl = data.map((order, index) => {
      let productCost = 0;
      const reportEl = order.tOrderLists.map((orderList, index) => {
        const name = get(orderList, `product.name${i18n.language}`, "");
        const quantity = orderList.quantity;
        const totalPrice = orderList.totalPrice;
        const cost =
          get(orderList, "product.stock.averageCost", 0) * orderList.quantity;
        productCost += cost;

        dataExcel.push([
          {
            value: "",
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
          {
            value: name,
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
          {
            value: quantity,
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
          {
            value: totalPrice,
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
          {
            value: "",
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
          {
            value: cost,
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
              font: { color: { rgb: "FFF5365C" } },
              numFmt: "0.00",
            },
          },
          {
            value: "",
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
          {
            value: "",
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
          {
            value: "",
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
          {
            value: "",
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "FFFFFFFF" } },
            },
          },
        ]);
        return (
          <tr key={index}>
            <th scope="row"></th>
            <th>{name}</th>
            <td>
              <NumberFormat value={quantity} />
            </td>
            <td>
              <NumberFormat value={totalPrice} type="money" />
            </td>
            <td></td>
            <td className="text-danger">
              <NumberFormat value={cost} type="money" />
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        );
      });

      const isPickup = order.isPickup;
      const id = order.id;
      const createAt = dayjs(order.created_at).format("D MMMM YYYY H:mm");
      const quantity = order.totalQuantity;
      const price = order.totalPrice;
      const shippingFee = order.shippingFee;
      const shippingCost = order.shippingCost;
      const packageCost = order.packageCost;
      const profit = isPickup
        ? price - productCost
        : price + shippingFee - productCost - shippingCost - packageCost;
      const dealer = `${get(order, `user.firstname${i18n.language}`)} ${get(
        order,
        `user.lastname${i18n.language}`
      )}`;
      dataExcel.push([
        {
          value: id,
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
          },
        },
        {
          value: createAt,
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
          },
        },
        {
          value: quantity,
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
          },
        },
        {
          value: price,
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
          },
        },
        {
          value: !isPickup && shippingFee ? shippingFee : "",
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
          },
        },
        {
          value: productCost,
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
            font: { color: { rgb: "FFF5365C" } },
            numFmt: "0.00",
          },
        },
        {
          value: !isPickup && shippingCost ? shippingCost : "",
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
            font: { color: { rgb: "FFF5365C" } },
          },
        },
        {
          value: !isPickup && packageCost ? packageCost : "",
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
            font: { color: { rgb: "FFF5365C" } },
          },
        },
        {
          value: profit,
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
            font: { color: { rgb: profit < 0 ? "FFF5365C" : "FF2DCE89" } },
            numFmt: "0.00",
          },
        },
        {
          value: dealer,
          style: {
            fill: { patternType: "solid", fgColor: { rgb: "FFF6F9FC" } },
          },
        },
      ]);
      reportEl.push(
        <tr key={index} className="table-light">
          <th scope="row">{id}</th>
          <th>{createAt}</th>
          <td>
            <NumberFormat value={quantity} />
          </td>
          <td>
            <NumberFormat value={price} type="money" />
          </td>
          <td>
            {!isPickup && <NumberFormat value={shippingFee} type="money" />}
          </td>
          <td className="text-danger">
            <NumberFormat value={productCost} type="money" />
          </td>
          <td className="text-danger">
            {!isPickup && <NumberFormat value={shippingCost} type="money" />}
          </td>
          <td className="text-danger">
            {!isPickup && <NumberFormat value={packageCost} type="money" />}
          </td>
          <td className={profit < 0 ? "text-danger" : "text-success"}>
            <NumberFormat value={profit} type="money" />
          </td>
          <td>{dealer}</td>
        </tr>
      );

      totalQuantity += order.totalQuantity;
      totalWholeSalePrice += order.totalPrice;
      totalProductCost += productCost;
      if (!isPickup) {
        totalShipingFee += order.shippingFee;
        totalShipingCost += order.shippingCost;
        totalPackageCost += order.packageCost;
      }
      return reportEl;
    });

    const totalProfit =
      totalWholeSalePrice +
      totalShipingFee -
      totalProductCost -
      totalShipingCost -
      totalPackageCost;
    dataExcel.push([
      {
        value: "",
        style: { fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } } },
      },
      {
        value: t("TOTAL"),
        style: { fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } } },
      },
      {
        value: totalQuantity,
        style: { fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } } },
      },
      {
        value: totalWholeSalePrice,
        style: { fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } } },
      },
      {
        value: totalShipingFee,
        style: { fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } } },
      },
      {
        value: totalProductCost,
        style: {
          fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } },
          font: { color: { rgb: "FFF5365C" } },
          numFmt: "0.00",
        },
      },
      {
        value: totalShipingCost,
        style: {
          fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } },
          font: { color: { rgb: "FFF5365C" } },
        },
      },
      {
        value: totalPackageCost,
        style: {
          fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } },
          font: { color: { rgb: "FFF5365C" } },
        },
      },
      {
        value: totalProfit,
        style: {
          fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } },
          font: { color: { rgb: totalProfit < 0 ? "FFF5365C" : "FF2DCE89" } },
          numFmt: "0.00",
        },
      },
      {
        value: "",
        style: { fill: { patternType: "solid", fgColor: { rgb: "FFE9ECEF" } } },
      },
    ]);
    const headingStyle = {
      font: { color: { rgb: "FFFFFFFF" } },
      fill: { patternType: "solid", fgColor: { rgb: "FF1C4595" } },
    };
    const reportEx = [
      {
        columns: [
          { title: "#", width: { wch: 10 }, style: headingStyle },
          {
            title: t("PRODUCT__NAME"),
            width: { wch: 40 },
            style: headingStyle,
          },
          { title: t("QUANTITY"), width: { wch: 10 }, style: headingStyle },
          {
            title: t("WHOLESALE_PRICE"),
            width: { wch: 15 },
            style: headingStyle,
          },
          { title: t("SHIPPING_FEE"), width: { wch: 15 }, style: headingStyle },
          { title: t("PRODUCT_COST"), width: { wch: 15 }, style: headingStyle },
          {
            title: t("SHIPPING_COST"),
            width: { wch: 15 },
            style: headingStyle,
          },
          { title: t("PACKAGE_COST"), width: { wch: 15 }, style: headingStyle },
          { title: t("PROFIT"), width: { wch: 15 }, style: headingStyle },
          { title: t("DEALER"), width: { wch: 40 }, style: headingStyle },
        ],
        data: dataExcel,
      },
    ];

    setSummary([
      totalQuantity,
      totalWholeSalePrice,
      totalShipingFee,
      totalProductCost,
      totalShipingCost,
      totalPackageCost,
      totalProfit,
    ]);
    setReportExcel(reportEx);
    setReportElement(reportEl);
  };

  return (
    <>
      <SimpleHeader
        name={t("REPORT__PRODUCT_SALES")}
        parentName={t("REPORT")}
      ></SimpleHeader>
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">{t("REPORT__PRODUCT_SALES")}</h3>
              </CardHeader>

              <SmartTable
                total={total}
                onReload={getReport}
                report={reportExcel}
                isClientSide
              >
                <thead>
                  <tr>
                    <th>#</th>
                    <th>{t("PRODUCT__NAME")}</th>
                    <th>{t("QUANTITY")}</th>
                    <th>{t("WHOLESALE_PRICE")}</th>
                    <th>{t("SHIPPING_FEE")}</th>
                    <th>{t("PRODUCT_COST")}</th>
                    <th>{t("SHIPPING_COST")}</th>
                    <th>{t("PACKAGE_COST")}</th>
                    <th>{t("PROFIT")}</th>
                    <th>{t("DEALER")}</th>
                  </tr>
                </thead>
                <tbody>{reportElement}</tbody>
                <tfoot>
                  <tr>
                    <td></td>
                    <td>{t("TOTAL")}</td>
                    <td>
                      <NumberFormat value={summary[0]} />
                    </td>
                    <td>
                      <NumberFormat value={summary[1]} type="money" />
                    </td>
                    <td>
                      <NumberFormat value={summary[2]} type="money" />
                    </td>
                    <td className="text-danger">
                      <NumberFormat value={summary[3]} type="money" />
                    </td>
                    <td className="text-danger">
                      <NumberFormat value={summary[4]} type="money" />
                    </td>
                    <td className="text-danger">
                      <NumberFormat value={summary[5]} type="money" />
                    </td>
                    <td
                      className={
                        summary[6] < 0 ? "text-danger" : "text-success"
                      }
                    >
                      <NumberFormat value={summary[6]} type="money" />
                    </td>
                    <td></td>
                  </tr>
                </tfoot>
              </SmartTable>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default ReportProductSales;
