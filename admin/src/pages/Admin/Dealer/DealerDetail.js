import {
  API_MASTER_LEVEL,
  API_MASTER_ROLE,
  API_MASTER_SEX,
  API_USER,
} from "config";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Form,
  Row,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { findAll, findOne, updateData } from "api";
import { get, isEmpty } from "lodash";
import {
  hideSpinner,
  notifyError,
  notifySuccess,
  showAlertWarning,
  showSpinner,
} from "redux/actions";
import { FormInput } from "components/Forms";
import { LOCALE_ENGLISH } from "i18n";
import SimpleHeader from "components/Headers/SimpleHeader";
import { dayjs } from "utils";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";

const DealerDetail = () => {
  const { t, i18n } = useTranslation();
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const [showId, setShowId] = useState();
  const [parents, setParents] = useState([]);
  const [roles, setRoles] = useState([]);
  const [sexes, setSexes] = useState([]);
  const [levels, setLevels] = useState([]);
  const [createdAt, setCreatedAt] = useState();
  const [updatedAt, setUpdatedAt] = useState();
  const [isBlocked, setIsBlocked] = useState(false);

  const { handleSubmit, control, reset, formState: { errors } } = useForm();

  const onSubmit = async (data) => {
    const request = {
      username: data.username.toLowerCase(),
      email: data.email.toLowerCase(),
      avatar: data.avatar,
      facebook: data.facebook,
      firstnameEN: data.firstnameEN,
      firstnameTH: data.firstnameTH,
      idcard: data.idcard,
      idcardImage: data.idcardImage,
      instagram: data.instagram,
      lastnameEN: data.lastnameEN,
      lastnameTH: data.lastnameTH,
      line: data.line,
      mLevel: data.mLevel,
      mSex: data.mSex,
      nicknameEN: data.nicknameEN,
      nicknameTH: data.nicknameTH,
      parent: data.parent,
      phoneNumber: data.phoneNumber,
      role: data.role,
      slip: data.slip,
    };
    console.log('request', request);
    const response = await updateData(API_USER, id, request, dispatch);
    if (response) {
      const message = {
        text: t("UPDATE_SUCCESS"),
      };
      dispatch(notifySuccess(message));
      history.push("/admin/dealers");
    } else {
      const message = {
        text: t("UPDATE_FAIL"),
      };
      dispatch(notifyError(message));
    }
  };

  const extractData = (data) => ({
    ...data,
    parent: get(data, "parent.id"),
    role: get(data, "role.id"),
    mSex: get(data, "mSex.id"),
    mLevel: get(data, "mLevel.id"),
  });

  const getProductDetail = async (id) => {
    dispatch(showSpinner());
    let response = await findOne(API_USER, id);
    const data = extractData(response);

    await getMasterData(data);
    setCreatedAt(data.created_at);
    setUpdatedAt(data.updated_at);

    reset(data);
    setIsBlocked(data.blocked);
    dispatch(hideSpinner());
  };

  const getMasterData = () => {
    return new Promise((resolve) => {
      let wait = 4;

      findAll(API_USER).then((data) => {
        const filterData = data.filter((item) => item.mLevel.level > 0);
        setParents(filterData);
        wait--;
        !wait && resolve();
      });

      findAll(API_MASTER_ROLE).then((data) => {
        const { roles = [] } = data;
        setRoles(roles);
        wait--;
        !wait && resolve();
      });

      findAll(API_MASTER_SEX).then((data) => {
        setSexes(data);
        wait--;
        !wait && resolve();
      });

      findAll(API_MASTER_LEVEL, { _sort: "level" }).then((data) => {
        setLevels(data);
        wait--;
        !wait && resolve();
      });
    });
  };

  const handleApprove = async () => {
    const response = await updateData(
      API_USER,
      id,
      { blocked: !isBlocked },
      dispatch
    );
    response && setIsBlocked(!isBlocked);
  };

  useEffect(() => {
    if (showId !== id) {
      setShowId(id);
      if (id !== "create") {
        getProductDetail(id);
      }
    }

    if (!isEmpty(errors)) {
      dispatch(showAlertWarning({ text: t("WARNING__DATA_INCOMPLETE") }));
    }
  }, [id, errors]);

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <SimpleHeader name={t("DEALER__EDIT")} parentName={t("DEALER")}>
        <Button
          className="btn-icon btn-3"
          color={isBlocked ? "success" : "danger"}
          type="button"
          onClick={handleApprove}
        >
          <span className="btn-inner--icon">
            <i
              className={`fas fa-${
                isBlocked ? "check-circle" : "times-circle"
              }`}
            ></i>
          </span>
          <span className="btn-inner--text">
            {isBlocked ? t("APPROVE") : t("NOT_APPROVE")}
          </span>
        </Button>
        <Button className="btn-icon btn-3" color="secondary" type="submit">
          <span className="btn-inner--icon">
            <i className="fas fa-save"></i>
          </span>
          <span className="btn-inner--text">{t("SAVE")}</span>
        </Button>
      </SimpleHeader>
      <Container className="mt--6" fluid>
        <Row>
          <Col lg="8">
            <div className="card-wrapper">
              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("PROFILE")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("FIRSTNAME")} (${t("EN")}) `}
                        name="firstnameEN"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("LASTNAME")} (${t("EN")}) `}
                        name="lastnameEN"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("FIRSTNAME")} (${t("TH")}) `}
                        name="firstnameTH"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("LASTNAME")} (${t("TH")}) `}
                        name="lastnameTH"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("NICKNAME")} (${t("EN")}) `}
                        name="nicknameEN"
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("NICKNAME")} (${t("TH")}) `}
                        name="nicknameTH"
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("USERNAME")}
                        name="username"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("EMAIL")}
                        name="email"
                        type="email"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("IDCARD")}
                        name="idcard"
                        type="idcard"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("IDCARD_IMAGE")}
                        name="idcardImage"
                        type="file"
                        maxFiles={1}
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("PHONE_NUMBER")}
                        name="phoneNumber"
                        type="tel"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("LINE")}
                        name="line"
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("FACEBOOK")}
                        name="facebook"
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("INSTAGRAM")}
                        name="instagram"
                        control={control}
                      />
                    </Col>
                  </Row>
                </CardBody>
              </Card>

              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("IMAGE")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("PROFILE_PICTURE")}
                        name="avatar"
                        type="file"
                        maxFiles={1}
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("SLIP")}
                        name="slip"
                        type="file"
                        maxFiles={1}
                        control={control}
                      />
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </div>
          </Col>

          <Col lg="4">
            <div className="card-wrapper">
              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("INFORMATION")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col sm="4">{t("CREATE_DATE")}</Col>
                    <Col sm="8">
                      <p>
                        {createdAt
                          ? dayjs(createdAt).format("D MMMM YYYY H:mm")
                          : "-"}
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="4">{t("LAST_UPDATE")}</Col>
                    <Col sm="8">
                      <p>
                        {updatedAt
                          ? dayjs(updatedAt).fromNow()
                          : t("MOMENT__FROM_NOW")}
                      </p>
                    </Col>
                  </Row>
                </CardBody>
              </Card>

              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("CHOOSE_ITEM")}</h3>
                </CardHeader>
                <CardBody>
                  <FormInput
                    label={t("PARENT")}
                    name="parent"
                    type="select"
                    control={control}
                    option={parents.map((item) => {
                      const name =
                        item[`firstname${i18n.language}`] &&
                        item[`lastname${i18n.language}`]
                          ? `${item[`firstname${i18n.language}`]} ${
                              item[`lastname${i18n.language}`]
                            }`
                          : item.username;
                      const level = get(
                        item,
                        `mLevel.name${i18n.language}`,
                        ""
                      );
                      return {
                        id: item.id,
                        text: `${name} (${level})`,
                      };
                    })}
                  />
                  <FormInput
                    label={t("ROLE")}
                    name="role"
                    type="select"
                    control={control}
                    rules={{ required: true }}
                    option={roles.map((item) => ({
                      id: item.id,
                      text: item[
                        i18n.language === LOCALE_ENGLISH
                          ? "name"
                          : "description"
                      ],
                    }))}
                  />
                  <FormInput
                    label={t("SEX")}
                    name="mSex"
                    type="select"
                    control={control}
                    rules={{ required: true }}
                    option={sexes.map((item) => ({
                      id: item.id,
                      text: item[`name${i18n.language}`],
                    }))}
                  />
                  <FormInput
                    label={t("LEVEL")}
                    name="mLevel"
                    type="select"
                    control={control}
                    rules={{ required: true }}
                    option={levels.map((item) => ({
                      id: item.id,
                      text: item[`name${i18n.language}`],
                    }))}
                  />
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </Container>
    </Form>
  );
};

export default DealerDetail;
