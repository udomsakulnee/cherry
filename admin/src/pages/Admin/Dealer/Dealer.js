import { API_ADDRESS, API_ORDER_SUMMARY, API_URL, API_USER } from "config";
import {
  Card,
  CardHeader,
  Container,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
import React, { useState } from "react";
import { countRecords, deleteData, deleteUploadFile, findAll } from "api";
import { dayjs } from "utils";
import { notifySuccess, showConfirm } from "redux/actions";
import { NumberFormat } from "components/Shared";
import SimpleHeader from "components/Headers/SimpleHeader";
import { SmartTable } from "components/Shared";
import { get } from "lodash";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Dealer = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();
  const [total, setTotal] = useState(0);
  const [dealers, setDealers] = useState([]);

  const getDealers = async (params) => {
    const total = await countRecords(API_USER);
    const data = await findAll(API_USER, params, dispatch);
    if (total > 0) {
      setDealers(data);
      setTotal(total);
    } else {
      setDealers([]);
      setTotal(0);
    }
  };

  const dealerDetail = (id) => () => {
    history.push("/admin/dealers/" + id);
  };

  const deleteDealer = (id) => () => {
    const message = {
      text: t("CONFIRM_DELETE"),
      onConfirm: async () => {
        const response = await deleteData(API_USER, id);
        if (response) {
          const avatarId = get(response, "avatar.id");
          avatarId && deleteUploadFile(avatarId);

          const idcardImageId = get(response, "idcardImage.id");
          idcardImageId && deleteUploadFile(idcardImageId);

          const slip = get(response, "slip.id");
          slip && deleteUploadFile(slip);

          const orderId = get(response, "orderSummary.id");
          response.orderSummary && deleteData(API_ORDER_SUMMARY, orderId);

          const addresses = get(response, "addresses", []);
          addresses.map(async (address) => {
            await deleteData(API_ADDRESS, address.id);
          });

          const message = {
            text: t("DELETE_SUCCESS"),
          };
          dispatch(notifySuccess(message));
          getDealers();
        }
      },
    };
    dispatch(showConfirm(message));
  };

  return (
    <>
      <SimpleHeader
        name={t("DEALER__LIST")}
        parentName={t("DEALER")}
      ></SimpleHeader>
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">{t("DEALER")}</h3>
              </CardHeader>

              <SmartTable total={total} onReload={getDealers}>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>{t("IMAGE")}</th>
                    <th>{t("FIRSTNAME_LASTNAME")}</th>
                    <th>{t("AREA")}</th>
                    <th>{t("LEVEL")}</th>
                    <th>{t("SUM_PURCHASE")}</th>
                    <th>{t("POINT")}</th>
                    <th data-sort="blocked">{t("STATUS")}</th>
                    <th>{t("REGISTRATION_DATE")}</th>
                    <th>{t("ACTION")}</th>
                  </tr>
                </thead>
                <tbody>
                  {dealers.map((dealer, index) => {
                    let userAvatar = get(
                      dealer,
                      "avatar.formats.thumbnail.url"
                    );
                    if (userAvatar) {
                      userAvatar = `${API_URL}${userAvatar}`;
                    } else {
                      const sex = get(
                        dealer,
                        "mSex.nameEN",
                        "Male"
                      ).toLowerCase();
                      userAvatar = require(`assets/img/icons/common/${sex}.svg`).default;
                    }
                    return (
                      <tr key={index}>
                        <th scope="row">{dealer.id}</th>
                        <td>
                          <div className="avatar-group">
                            <a
                              key={index}
                              className="avatar avatar-sm rounded-circle"
                              onClick={(e) => e.preventDefault()}
                            >
                              <img src={userAvatar} className="thumbnail" />
                            </a>
                          </div>
                        </td>
                        <td>
                          {dealer[`firstname${i18n.language}`]}{" "}
                          {dealer[`lastname${i18n.language}`]}
                        </td>
                        <td>{get(dealer, "addresses[0].province")}</td>
                        <td>{get(dealer, `mLevel.name${i18n.language}`)}</td>
                        <td>
                          <NumberFormat
                            value={get(dealer, "orderSummary.totalPrice", 0)}
                            type="money"
                          />
                        </td>
                        <td>
                          <NumberFormat
                            value={get(dealer, "orderSummary.totalPoint", 0)}
                          />
                        </td>
                        <td>
                          <span className="badge-dot mr-4 badge badge-">
                            <i
                              className={`bg-${
                                dealer.blocked ? "danger" : "success"
                              }`}
                            ></i>
                            <span className="status">
                              {dealer.blocked ? t("NOT_APPROVE") : t("APPROVE")}
                            </span>
                          </span>
                        </td>
                        <th>
                          {dayjs(dealer.created_at).format("D MMMM YYYY H:mm")}
                        </th>
                        <td className="table-actions">
                          <a
                            className="table-action p-1"
                            href="javascript:void(0)"
                            id="edit-dealer"
                            onClick={dealerDetail(dealer.id)}
                          >
                            <i className="fas fa-edit" />
                          </a>
                          <UncontrolledTooltip delay={0} target="edit-dealer">
                            {t("EDIT")}
                          </UncontrolledTooltip>
                          {dealer.blocked && (
                            <>
                              <a
                                className="table-action table-action-delete p-1"
                                href="javascript:void(0)"
                                id="delete-dealer"
                                onClick={deleteDealer(dealer.id)}
                              >
                                <i className="fas fa-trash" />
                              </a>
                              <UncontrolledTooltip
                                delay={0}
                                target="delete-dealer"
                              >
                                {t("DELETE")}
                              </UncontrolledTooltip>
                            </>
                          )}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </SmartTable>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Dealer;
