import {
  Badge,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Row,
} from "reactstrap";
import React from "react";
import SimpleHeader from "components/Headers/SimpleHeader";
import { dayjs } from "utils";
import { useTranslation } from "react-i18next";

const Version = () => {
  const { t } = useTranslation();

  return (
    <>
      <SimpleHeader parentName={t("VERSION")}></SimpleHeader>
      <Container className="mt--6" fluid>
        <Row className="justify-content-center">
          <Col lg="6">
            <Card>
              <CardHeader className="bg-transparent">
                <h3 className="mb-0">{t("VERSION")}</h3>
              </CardHeader>
              <CardBody>
                <div
                  className="timeline timeline-one-side"
                  data-timeline-axis-style="dashed"
                  data-timeline-content="axis"
                >
                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-07-04").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.5.1
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("FIX_BUG")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("REGISTER")}</li>
                          <ul>
                            <li>แก้ไขการกำหนดตำแหน่งเริ่มต้นเมื่อสมัครตัวแทนใหม่</li>
                          </ul>
                          <li>{t("ADDRESS")}</li>
                          <ul>
                            <li>แก้ไขการแสดงข้อผิดพลาด</li>
                            <li>แก้ไขการเลือกที่อยู่</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-success">
                      <i className="ni ni-air-baloon" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-07-03").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="success" pill>
                          {t("VERSION")} 1.5.0
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("PRODUCT")}</li>
                          <ul>
                            <li>ปรับราคาขายส่งตามตำแหน่งตัวแทน</li>
                          </ul>
                          <li>{t("PROFILE")}</li>
                          <ul>
                            <li>เพิ่มตำแหน่งตัวแทนใหม่</li>
                            <li>เพิ่มบัตรตัวแทนใหม่</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("IMPROVEMENT")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>อัปเดตธีม</li>
                          <li>
                            ปรับปรุงประสิทธิภาพการทำงาน และแก้ไขข้อบกพร่อง
                          </li>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-success">
                      <i className="ni ni-air-baloon" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-05-02").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="success" pill>
                          {t("VERSION")} 1.4.0
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("REPORT")}</li>
                          <ul>
                            <li>รายงานการขายสินค้า</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("IMPROVEMENT")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>เปลี่ยนไลบรารี่สำหรับการแสดงวันที่และเวลา</li>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-04-28").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.3.6
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("LOGIN")}</li>
                          <ul>
                            <li>ซ่อน / แสดง รหัสผ่าน</li>
                          </ul>
                          <li>{t("PROFILE")}</li>
                          <ul>
                            <li>เปลี่ยนรหัสผ่าน</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-03-17").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.3.5
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>คัดลอกที่อยู่</li>
                            <li>เพิ่มหมายเหตุ</li>
                          </ul>
                        </ul>
                      </p>

                      <h5 className="mt-3 mb-0">{t("FIX_BUG")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>แก้ไขรูปภาพสินค้าความละเอียดต่ำไม่แสดง</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-03-11").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.3.4
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("FIX_BUG")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ADDRESS")}</li>
                          <ul>
                            <li>แก้ไขการบันทึกที่อยู่ไม่สำเร็จ</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-02-01").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.3.3
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("PROFILE")}</li>
                          <ul>
                            <li>เปลี่ยนภาพโปรไฟล์</li>
                            <li>ดาวน์โหลดบัตรตัวแทน</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-29").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.3.2
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("IMPROVEMENT")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>ปิดการเก็บเงินปลายทาง</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-27").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.3.1
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>เก็บเงินปลายทาง</li>
                            <li>ลบสินค้าในตะกร้า</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("FIX_BUG")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>การแสดงสลิปโอนเงิน</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-success">
                      <i className="ni ni-air-baloon" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-22").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="success" pill>
                          {t("VERSION")} 1.3.0
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("PROFILE")}</li>
                          <ul>
                            <li>แก้ไขข้อมูลส่วนตัว</li>
                            <li>แสดงยอดรวมการสั่งซื้อ</li>
                            <li>บัตรตัวแทน</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("IMPROVEMENT")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>เปลี่ยนข้อความค่าหีบห่อเป็นค่ากล่อง</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-success">
                      <i className="ni ni-air-baloon" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-16").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="success" pill>
                          {t("VERSION")} 1.2.0
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ADDRESS")}</li>
                          <ul>
                            <li>เพิ่มที่อยู่จัดส่งมากกว่า 1 ที่อยู่</li>
                            <li>ตั้งค่าที่อยู่ตั้งต้น</li>
                            <li>เปลี่ยนแปลงที่อยู่จัดส่ง</li>
                            <li>เพิ่มหมู่บ้าน</li>
                          </ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>คำนวณต้นทุนค่าจัดส่ง</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("IMPROVEMENT")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>ปรับจำนวนสั่งซื้อสินค้าตั้งต้น</li>
                            <li>แสดงยอดสั่งซื้อรวมกับค่าจัดส่ง</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-12").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.1.2
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>รับสินค้าเองที่บริษัท</li>
                            <li>สถานะการสั่งซื้อสินค้า</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("IMPROVEMENT")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("REGISTER")}</li>
                          <ul>
                            <li>กำหนดจำนวนตัวอักษรของชื่อบัญชี</li>
                          </ul>
                          <li>{t("PRODUCT")}</li>
                          <ul>
                            <li>เปลี่ยนหน่วยน้ำหนักสินค้า</li>
                            <li>จัดรูปแบบการแสดงตัวเลข</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("FIX_BUG")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("PRODUCT")}</li>
                          <ul>
                            <li>การคำนวณสินค้าคงเหลือ</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-09").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.1.1
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("LOGIN")}</li>
                          <ul>
                            <li>เข้าสู่ระบบด้วยพิมพ์ใหญ่และพิมพ์เล็ก</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("FIX_BUG")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("PRODUCT")}</li>
                          <ul>
                            <li>แก้ไขการดึงสินค้า</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-success">
                      <i className="ni ni-air-baloon" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-07").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="success" pill>
                          {t("VERSION")} 1.1.0
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("REGISTER")}</li>
                          <ul>
                            <li>สมัครตัวแทนจำหน่ายด้วย QR Code</li>
                            <li>คัดลอก URL สำหรับสมัครตัวแทนจำหน่าย</li>
                          </ul>
                        </ul>
                      </p>
                      <h5 className="mt-3 mb-0">{t("IMPROVEMENT")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("DEALER")}</li>
                          <ul>
                            <li>
                              ใช้พื้นที่จัดส่งเดียวกับที่อยู่จัดส่งของตัวแทนจำหน่าย
                            </li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-danger">
                      <i className="ni ni-settings" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-03").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="danger" pill>
                          {t("VERSION")} 1.0.1
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("PROFILE")}</li>
                          <ul>
                            <li>แสดงระดับตำแหน่ง</li>
                          </ul>

                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>แสดงที่อยู่จัดส่ง</li>
                            <li>ปรับราคาขายส่งตามระดับตำแหน่ง</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>

                  <div className="timeline-block">
                    <span className="timeline-step badge-success">
                      <i className="ni ni-air-baloon" />
                    </span>
                    <div className="timeline-content">
                      <small className="text-muted font-weight-bold">
                        {dayjs("2021-01-01").format("D MMMM YYYY")}
                      </small>
                      <div className="mt-3">
                        <Badge color="success" pill>
                          {t("VERSION")} 1.0.0
                        </Badge>
                      </div>
                      <h5 className="mt-3 mb-0">{t("NEW_FEATURE")}</h5>
                      <p className="text-sm mt-1 mb-0">
                        <ul>
                          <li>{t("LOGIN")}</li>
                          <ul>
                            <li>เข้าสู่ระบบด้วยอีเมล และบัญชีผู้ใช้</li>
                            <li>จดจำบัญชีผู้ใช้ และรหัสผ่าน</li>
                          </ul>

                          <li>{t("REGISTER")}</li>
                          <ul>
                            <li>สมัครตัวแทนจำหน่ายด้วยอีเมล และบัญชีผู้ใช้</li>
                            <li>สมัครสมาชิกต่อจากแม่ทีม</li>
                          </ul>

                          <li>{t("PRODUCT")}</li>
                          <ul>
                            <li>จัดการรายการสินค้า</li>
                            <li>เพิ่ม แก้ไข ลบ สินค้า</li>
                            <li>อัปโหลดรูปภาพสินค้า</li>
                            <li>จัดการราคาขายส่ง</li>
                            <li>จัดการสินค้าเข้าระบบ</li>
                            <li>เบิกสินค้าออก</li>
                          </ul>

                          <li>{t("ORDER")}</li>
                          <ul>
                            <li>แค็ตตาล็อกสินค้า</li>
                            <li>รถเข็นสินค้า</li>
                            <li>ชำระสินค้าโดยการโอนเงิน</li>
                            <li>จัดการรายการสั่งซื้อ</li>
                            <li>จัดการสถานะการสั่งซื้อ</li>
                          </ul>

                          <li>{t("DEALER")}</li>
                          <ul>
                            <li>จัดการรายการตัวแทนจำหน่าย</li>
                            <li>แก้ไข ลบ ตัวแทนจำหน่าย</li>
                            <li>อนุมัติตัวแทนจำหน่าย</li>
                          </ul>
                        </ul>
                      </p>
                    </div>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Version;
