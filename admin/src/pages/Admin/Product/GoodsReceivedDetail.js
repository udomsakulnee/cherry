import {
  API_PRODUCT,
  API_STOCK_QUANTITY,
  API_TRANSACTION_GOODS_RECEIVED,
  API_TRANSACTION_GOODS_RECEIVED_LIST,
  KEY_USER,
} from "config";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Form,
  Row,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { createData, deleteData, findAll, findOne, updateData } from "api";
import { getSession, dayjs } from "utils";
import {
  hideSpinner,
  notifyError,
  notifySuccess,
  showAlertWarning,
  showSpinner,
} from "redux/actions";
import { useFieldArray, useForm } from "react-hook-form";
import { useHistory, useParams } from "react-router-dom";
import { FormInput } from "components/Forms";
import SimpleHeader from "components/Headers/SimpleHeader";
import { isEmpty } from "lodash";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";

let productIds = [];

const GoodsReceivedDetail = () => {
  const { t, i18n } = useTranslation();
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const [showId, setShowId] = useState();
  const [products, setProducts] = useState([]);
  const [createdAt, setCreatedAt] = useState();
  const [updatedAt, setUpdatedAt] = useState();
  const [removeList, setRemoveList] = useState([]);
  const user = getSession(KEY_USER);

  const {
    handleSubmit,
    control,
    // errors,
    reset,
    register,
    formState: { errors },
  } = useForm();
  const { fields, append, remove } = useFieldArray({
    control,
    name: "tGoodsReceivedLists",
  });

  const updateQuantity = async (response) => {
    if (response) {
      let responseGoodsReceived = [];
      for (let i = 0; i < response.tGoodsReceivedLists.length; i++) {
        const productId = response.tGoodsReceivedLists[i].product;
        const response2 = await updateData(API_STOCK_QUANTITY, productId);
        if (response2) {
          responseGoodsReceived = [...responseGoodsReceived, response2];
        }
      }

      const message = {
        text: t("CREATE_SUCCESS"),
      };
      dispatch(notifySuccess(message));
      history.push("/admin/goods-received");
    } else {
      const message = {
        text: t("CREATE_FAIL"),
      };
      dispatch(notifyError(message));
    }

    dispatch(hideSpinner());
  };

  const onSubmit = async (data) => {
    dispatch(showSpinner());
    const { tGoodsReceivedLists } = data;
    delete data.productWholesalePrices;

    let tGoodsReceivedListsResponse = [];
    for (let i = 0; i < tGoodsReceivedLists.length; i++) {
      const { id } = tGoodsReceivedLists[i];
      delete tGoodsReceivedLists[i].id;
      if (id === "create") {
        const response = await createData(
          API_TRANSACTION_GOODS_RECEIVED_LIST,
          tGoodsReceivedLists[i]
        );
        if (response) {
          tGoodsReceivedListsResponse = [
            ...tGoodsReceivedListsResponse,
            response,
          ];
          productIds = [...productIds, response.product.id];
        }
      } else {
        const response = await updateData(
          API_TRANSACTION_GOODS_RECEIVED_LIST,
          id,
          tGoodsReceivedLists[i]
        );
        if (response) {
          tGoodsReceivedListsResponse = [
            ...tGoodsReceivedListsResponse,
            response,
          ];
        }
      }
    }

    if (removeList.length > 0) {
      for (let i = 0; i < removeList.length; i++) {
        await deleteData(API_TRANSACTION_GOODS_RECEIVED_LIST, removeList[i]);
      }
      setRemoveList([]);
    }

    if (tGoodsReceivedListsResponse.length === tGoodsReceivedLists.length) {
      data = {
        ...data,
        tGoodsReceivedLists: tGoodsReceivedListsResponse.map((item) => item.id),
        user: user.id,
      };
      if (id === "create") {
        const response = await createData(API_TRANSACTION_GOODS_RECEIVED, data);
        updateQuantity(response);
      } else {
        const response = await updateData(
          API_TRANSACTION_GOODS_RECEIVED,
          id,
          data
        );
        updateQuantity(response);
      }
    } else {
      const message = {
        text: t("UPDATE_FAIL"),
      };
      dispatch(notifyError(message));
    }

    dispatch(hideSpinner());
  };

  const getStockInDetail = async (id) => {
    dispatch(showSpinner());
    let response = await findOne(API_TRANSACTION_GOODS_RECEIVED, id);

    await getMasterData();
    setCreatedAt(response.created_at);
    setUpdatedAt(response.updated_at);

    reset(response);
    const ids = response.tGoodsReceivedLists.map(({ product }) => product);
    productIds = ids;
    dispatch(hideSpinner());
  };

  const getMasterData = () => {
    return new Promise((resolve) => {
      findAll(API_PRODUCT, { _sort: "code" }).then((data) => {
        setProducts(data);
        resolve();
      });
    });
  };

  const handleRemoveStock = (index, id) => () => {
    id !== "create" && setRemoveList([...removeList, id]);
    remove(index);
  };

  useEffect(() => {
    if (showId !== id) {
      setShowId(id);
      if (id === "create") {
        getMasterData();
      } else {
        getStockInDetail(id);
      }
    }

    if (!isEmpty(errors)) {
      dispatch(showAlertWarning({ text: t("WARNING__DATA_INCOMPLETE") }));
    }
  }, [id, errors]);

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <SimpleHeader
        name={
          id === "create" ? t("GOODS__RECEIVED_NEW") : t("GOODS__RECEIVED_EDIT")
        }
        parentName={t("PRODUCT")}
      >
        <Button className="btn-icon btn-3" color="secondary" type="submit">
          <span className="btn-inner--icon">
            <i className="fas fa-save"></i>
          </span>
          <span className="btn-inner--text">{t("SAVE")}</span>
        </Button>
      </SimpleHeader>
      <Container className="mt--6" fluid>
        <Row>
          <Col lg="8">
            <div className="card-wrapper">
              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("GOODS__RECEIVED")}</h3>
                </CardHeader>
                <CardBody>
                  {fields.map(({ id, product, quantity, totalCost }, index) => {
                    return (
                      <div key={`goods-received-${index}`}>
                        {index > 0 && <hr className="mt-3" />}
                        <input
                          type="hidden"
                          name={`tGoodsReceivedLists[${index}].id`}
                          ref={register()}
                          defaultValue={id}
                        />
                        <Row>
                          <Col xs={12} sm={11}>
                            <FormInput
                              label={t("PRODUCT__NAME")}
                              name={`tGoodsReceivedLists[${index}].product`}
                              type="select"
                              control={control}
                              rules={{ required: true }}
                              isSearchable
                              defaultValue={product}
                              option={products.map((item) => ({
                                id: item.id,
                                text: `[${item.code}] ${
                                  item[`name${i18n.language}`]
                                }`,
                              }))}
                            />
                          </Col>
                          <Col xs={12} sm={1} className="text-right">
                            <Button
                              className="btn-icon btn-2"
                              color="danger"
                              type="button"
                              size="sm"
                              onClick={handleRemoveStock(index, id)}
                            >
                              <span className="btn-inner--icon">
                                <i className="fas fa-trash"></i>
                              </span>
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col xs={12} sm={4}>
                            <FormInput
                              label={t("QUANTITY")}
                              name={`tGoodsReceivedLists[${index}].quantity`}
                              type="number"
                              control={control}
                              rules={{ required: true }}
                              defaultValue={quantity}
                            />
                          </Col>
                          <Col xs={12} sm={4}>
                            <FormInput
                              label={`${t("TOTAL_COST")} (${t("BAHT")})`}
                              name={`tGoodsReceivedLists[${index}].totalCost`}
                              type="money"
                              control={control}
                              rules={{ required: true }}
                              defaultValue={totalCost}
                            />
                          </Col>
                        </Row>
                      </div>
                    );
                  })}
                  <Button
                    className="btn-icon btn-3"
                    color="primary"
                    type="button"
                    onClick={() =>
                      append({
                        id: "create",
                      })
                    }
                  >
                    <span className="btn-inner--icon">
                      <i className="fas fa-plus-circle"></i>
                    </span>
                    <span className="btn-inner--text">
                      {t("GOODS__RECEIVED_NEW")}
                    </span>
                  </Button>
                </CardBody>
              </Card>
            </div>
          </Col>

          <Col lg="4">
            <div className="card-wrapper">
              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("INFORMATION")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col sm="4">{t("CREATE_DATE")}</Col>
                    <Col sm="8">
                      <p>
                        {createdAt
                          ? dayjs(createdAt).format("D MMMM YYYY H:mm")
                          : "-"}
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="4">{t("LAST_UPDATE")}</Col>
                    <Col sm="8">
                      <p>
                        {updatedAt
                          ? dayjs(updatedAt).fromNow()
                          : t("MOMENT__FROM_NOW")}
                      </p>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </Container>
    </Form>
  );
};

export default GoodsReceivedDetail;
