import {
  API_TRANSACTION_ORDER,
  API_URL,
  CONST_PAYMENT_METHOD,
  CONST_PAYMENT_STATUS,
} from "config";
import {
  Card,
  CardHeader,
  Container,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NumberFormat } from "components/Shared";
import SimpleHeader from "components/Headers/SimpleHeader";
import { SmartTable } from "components/Shared";
import { dayjs } from "utils";
import { findLimit } from "api";
import { get } from "lodash";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Order = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();
  const user = useSelector((state) => state.user);
  const [total, setTotal] = useState(0);
  const [orders, setOrders] = useState([]);

  const getOrder = async (param) => {
    const userType = get(user, "role.type", "");
    const params = userType === "admin" ? param : { ...param, user: user.id };
    const { data, total = 0 } = await findLimit(
      API_TRANSACTION_ORDER,
      params,
      dispatch
    );
    if (total > 0) {
      setOrders(data);
      setTotal(total);
    } else {
      setOrders([]);
      setTotal(0);
    }
  };

  const orderDetail = (id) => () => {
    history.push("/admin/orders/" + id);
  };

  return (
    <>
      <SimpleHeader
        name={t("ORDER__LIST")}
        parentName={t("PRODUCT")}
      ></SimpleHeader>
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">{t("ORDER__LIST")}</h3>
              </CardHeader>

              <SmartTable total={total} onReload={getOrder}>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>{t("SLIP")}</th>
                    <th>{t("CREATE_DATE")}</th>
                    <th>{t("QUANTITY")}</th>
                    <th>{t("TOTAL_PRICE")}</th>
                    <th>{t("STATUS")}</th>
                    <th>{t("TRACKING_ID")}</th>
                    <th>{t("SHIPPING_TO")}</th>
                    <th>{t("DEALER")}</th>
                    <th>{t("ACTION")}</th>
                  </tr>
                </thead>
                <tbody>
                  {orders.map((order, index) => {
                    const slip = get(order, "slip.formats.thumbnail.url");
                    const trackingUrl = get(
                      order,
                      "mShippingCarrier.trackingUrl"
                    );

                    return (
                      <tr key={index}>
                        <th scope="row">{order.id}</th>
                        <td>
                          {slip && (
                            <div className="avatar-group">
                              <a
                                key={index}
                                className="avatar avatar-sm rounded-circle"
                                onClick={(e) => e.preventDefault()}
                              >
                                <img
                                  src={`${API_URL}${slip}`}
                                  className="thumbnail"
                                />
                              </a>
                            </div>
                          )}
                          {order.paymentMethod === CONST_PAYMENT_METHOD.COD && (
                            <span>{t("CASH_ON_DELIVERY")}</span>
                          )}
                        </td>
                        <th>
                          {dayjs(order.created_at).format("D MMMM YYYY H:mm")}
                        </th>
                        <td>
                          <NumberFormat value={order.totalQuantity} />
                        </td>
                        <td>
                          <NumberFormat
                            value={order.totalPrice + order.shippingFee}
                            type="money"
                          />
                        </td>
                        <td>
                          <span className="badge-dot mr-4 badge badge-">
                            <i
                              className={`bg-${
                                order.status === CONST_PAYMENT_STATUS.SHIPPING
                                  ? "success"
                                  : order.status ===
                                    CONST_PAYMENT_STATUS.INPROGRESS
                                  ? "info"
                                  : "danger"
                              }`}
                            ></i>
                            <span className="status">{t(order.status)}</span>
                          </span>
                        </td>
                        {trackingUrl ? (
                          <td>
                            <a
                              href={`${trackingUrl}${order.trackingId}`}
                              target="_blank"
                              rel="noreferrer"
                            >
                              {order.trackingId}
                            </a>
                          </td>
                        ) : (
                          <td>{order.trackingId}</td>
                        )}
                        <td>
                          {order.isPickup
                            ? t("SELF_PICK_UP")
                            : `${get(order, "address.firstnameTH", "")} ${get(
                                order,
                                "address.lastnameTH",
                                ""
                              )}`}
                        </td>
                        <td>{`${get(
                          order,
                          `user.firstname${i18n.language}`
                        )} ${get(order, `user.lastname${i18n.language}`)}`}</td>
                        <td className="table-actions">
                          <a
                            className="table-action p-1"
                            href="javascript:void(0)"
                            id="edit-product"
                            onClick={orderDetail(order.id)}
                          >
                            <i className="fas fa-edit" />
                          </a>
                          <UncontrolledTooltip delay={0} target="edit-product">
                            {t("EDIT")}
                          </UncontrolledTooltip>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </SmartTable>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Order;
