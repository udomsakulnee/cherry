import {
  API_BANK_ACCOUNT,
  API_ORDER,
  API_ORDER_SUMMARY,
  API_SHIPPING_FEE,
  API_TRANSACTION_CHARGE,
  API_TRANSACTION_ORDER,
  API_URL,
  CONST_PAYMENT_METHOD,
  CONST_PAYMENT_STATUS,
} from "config";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Form,
  Row,
  Table,
  UncontrolledTooltip,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { createData, findAll, findOne, updateData } from "api";
import { get, isEmpty } from "lodash";
import {
  showAlertError,
  showAlertSuccess,
  showAlertWarning,
  showConfirm,
} from "redux/actions";
import { useDispatch, useSelector } from "react-redux";
import Address from "./Address";
import { FormInput } from "components/Forms";
import { NumberFormat } from "components/Shared";
import SimpleHeader from "components/Headers/SimpleHeader";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Cart = () => {
  const location = useLocation();
  const { t, i18n } = useTranslation();
  const history = useHistory();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [cartList, setCartList] = useState([]);
  const [totalQuantity, setTotalQuantity] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalPoint, setTotalPoint] = useState(0);
  const [bankAccount, setBankAccount] = useState([]);
  const [shippingFee, setShippingFee] = useState(0);
  const [isConfirmOrder, setIsConfirmOrder] = useState(0);
  const [isPickup, setIsPickup] = useState(false);
  const [shippingAddress, setShippingAddress] = useState({});
  const [isShowModal, setIsShowModal] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState(
    CONST_PAYMENT_METHOD.TRANSFER
  );

  const getBankAccount = async () => {
    const bank = await findAll(API_BANK_ACCOUNT);
    setBankAccount(bank);
  };

  const getShippingFee = async (price) => {
    const fee = await findAll(API_SHIPPING_FEE, {
      minPrice_lte: price,
      _sort: "minPrice:desc",
    });
    fee && setShippingFee(fee[0].shippingFee);
  };

  const confirmOrder = async () => {
    if (isPickup || !isEmpty(shippingAddress)) {
      const data = {
        user: user.id,
        tOrderLists: cartList,
        totalQuantity,
        totalPrice,
        totalPoint,
        shippingFee: isPickup ? 0 : shippingFee,
        isPickup,
        paymentMethod,
        address: isPickup ? null : shippingAddress.id,
      };
      const response = await createData(API_ORDER, data, dispatch);
      if (response) {
        const status = get(response, "status", "");
        const id = get(response, "id", 0);
        if (
          status === CONST_PAYMENT_STATUS.PENDING ||
          status === CONST_PAYMENT_STATUS.INPROGRESS
        ) {
          setIsConfirmOrder(id);

          if (paymentMethod === CONST_PAYMENT_METHOD.COD) {
            const message = {
              text: t("CART__ORDER_SUCCESS"),
            };
            dispatch(showAlertSuccess(message));
            history.push("/admin/products");
          }
        } else {
          const message = {
            text: t("CART__NOT_ENOUGH_PRODUCT"),
          };
          dispatch(showAlertWarning(message));
        }
      } else {
        const message = {
          text: t("ALERT__DESCRIPTION_DANGER"),
        };
        dispatch(showAlertError(message));
      }
    }
  };

  const onSubmit = async (data) => {
    const response = await updateData(
      API_TRANSACTION_ORDER,
      isConfirmOrder,
      {
        slip: data.slip,
        status: CONST_PAYMENT_STATUS.INPROGRESS,
        remark: data.remark,
      },
      dispatch
    );
    if (response) {
      const response2 = await findOne(API_ORDER_SUMMARY, user.orderSummary.id);
      if (response2) {
        await updateData(API_ORDER_SUMMARY, user.orderSummary.id, {
          totalQuantity: response2.totalQuantity + totalQuantity,
          totalPrice: response2.totalPrice + totalPrice,
          totalPoint: response2.totalPoint + totalPoint,
        });

        await createData(API_TRANSACTION_CHARGE, {
          price: totalQuantity,
        });
      }
      const message = {
        text: t("CART__ORDER_SUCCESS"),
      };
      dispatch(showAlertSuccess(message));
      history.push("/admin/products");
    }
  };

  const setCart = (cart) => {
    const total = cart.reduce((a, b) => ({
      quantity: Number(a.quantity) + Number(b.quantity),
      totalPrice: Number(a.totalPrice) + Number(b.totalPrice),
      totalPoint: Number(a.totalPoint) + Number(b.totalPoint),
    }));
    setTotalQuantity(total.quantity);
    setTotalPrice(total.totalPrice);
    setTotalPoint(total.totalPoint);
    setCartList(cart);
    getShippingFee(total.totalPrice);
  };

  useEffect(() => {
    const cart = get(location, "state");
    if (cart && cart.length > 0) {
      setCart(cart);
      getBankAccount();
    }

    const { addresses = [] } = user;
    if (addresses.length > 0) {
      const filterAddresses = addresses.filter(({ isDefault }) => isDefault);
      filterAddresses.length > 0 && setShippingAddress(filterAddresses[0]);
    }
  }, []);

  const handleChangeAddress = (address) => {
    if (address) {
      setShippingAddress(address);
    }

    setTimeout(() => {
      setIsShowModal(false);
    }, 1000);
  };

  const handleChangeIsPickup = (isPickup) => {
    if (isPickup) {
      setPaymentMethod(CONST_PAYMENT_METHOD.TRANSFER);
    }
    setIsPickup(isPickup);
  };

  const handleRemove = (index) => () => {
    const message = {
      text: t("CONFIRM_DELETE"),
      onConfirm: async () => {
        const filterCartList = cartList.filter((item, i) => i !== index);
        setCart(filterCartList);
      },
    };
    dispatch(showConfirm(message));
  };

  return (
    <>
      <SimpleHeader name={t("CART")} parentName={t("PRODUCT")}></SimpleHeader>
      <Container className="mt--6" fluid>
        <Row className="card-wrapper">
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">{t("ORDER_LIST")}</h3>
              </CardHeader>

              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th>{t("PRODUCT__CODE")}</th>
                    <th>{t("IMAGE")}</th>
                    <th>{t("PRODUCT__NAME")}</th>
                    <th>{t("QUANTITY")}</th>
                    <th>{t("PRICE_PER_PIECE")}</th>
                    <th>{t("TOTAL_PRICE")}</th>
                    <th>{t("POINT")}</th>
                    <th>{t("ACTION")}</th>
                  </tr>
                </thead>
                <tbody className="list">
                  {cartList.map((product, index) => (
                    <tr key={index}>
                      <th scope="row">
                        <NumberFormat value={product.code} type="barcode" />
                      </th>
                      <td>
                        <div className="avatar-group">
                          {product[`image${i18n.language}`] &&
                            product[`image${i18n.language}`].map(
                              ({ id, name, formats }, index) => {
                                const url = get(formats, "thumbnail.url");
                                if (url) {
                                  return (
                                    <>
                                      <a
                                        key={index}
                                        className="avatar avatar-sm rounded-circle"
                                        id={`tooltip${id}`}
                                        onClick={(e) => e.preventDefault()}
                                      >
                                        <img
                                          alt={name}
                                          src={`${API_URL}${url}`}
                                          className="thumbnail"
                                        />
                                      </a>
                                      <UncontrolledTooltip
                                        delay={0}
                                        target={`tooltip${id}`}
                                      >
                                        {name}
                                      </UncontrolledTooltip>
                                    </>
                                  );
                                } else {
                                  return null;
                                }
                              }
                            )}
                        </div>
                      </td>
                      <td>{product[`name${i18n.language}`]}</td>
                      <td>
                        <NumberFormat value={product.quantity} />
                      </td>
                      <td>
                        <NumberFormat value={product.price} type="money" />
                      </td>
                      <td>
                        <NumberFormat value={product.totalPrice} type="money" />
                      </td>
                      <td>
                        <NumberFormat value={product.totalPoint} />
                      </td>
                      <td className="table-actions">
                        {!isConfirmOrder && cartList.length > 1 && (
                          <>
                            <a
                              className="table-action table-action-delete p-1"
                              href="javascript:void(0)"
                              id="delete-product"
                              onClick={handleRemove(index)}
                            >
                              <i className="fas fa-trash" />
                            </a>
                            <UncontrolledTooltip
                              delay={0}
                              target="delete-product"
                            >
                              {t("DELETE")}
                            </UncontrolledTooltip>
                          </>
                        )}
                      </td>
                    </tr>
                  ))}
                  <tr>
                    <th scope="row"></th>
                    <td></td>
                    <td>{t("SHIPPING_FEE")}</td>
                    <td></td>
                    <td></td>
                    <td>
                      <NumberFormat
                        value={isPickup ? 0 : shippingFee}
                        type="money"
                      />
                    </td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
                <tfoot>
                  <td></td>
                  <td></td>
                  <td>{t("TOTAL")}</td>
                  <td>
                    <NumberFormat value={totalQuantity} />
                  </td>
                  <td></td>
                  <td>
                    <NumberFormat
                      value={totalPrice + (isPickup ? 0 : shippingFee)}
                      type="money"
                    />
                  </td>
                  <td>
                    <NumberFormat value={totalPoint} />
                  </td>
                  <td></td>
                </tfoot>
              </Table>
            </Card>
          </div>
        </Row>

        {cartList.length > 0 && (
          <Row className="justify-content-center">
            <Col xs={12} sm={6}>
              <Card>
                <CardHeader>
                  <h5 className="h3 mb-0">{t("PAYMENT")}</h5>
                </CardHeader>

                <CardBody>
                  <div className="h1 text-center mt-3">
                    {t("DEALER")}: {get(user, `firstname${i18n.language}`, "")}{" "}
                    {get(user, `lastname${i18n.language}`, "")}
                  </div>

                  <div className="text-center">
                    <div className="custom-control custom-radio mb-3 d-inline-block mr-4">
                      <input
                        disabled={isConfirmOrder > 0}
                        className="custom-control-input"
                        id="pickupFalse"
                        name="pickup"
                        type="radio"
                        checked={!isPickup && "checked"}
                        onClick={() => handleChangeIsPickup(false)}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="pickupFalse"
                      >
                        {t("DELIVERY")}
                      </label>
                    </div>
                    <div className="custom-control custom-radio mb-3 d-inline-block">
                      <input
                        disabled={isConfirmOrder > 0}
                        className="custom-control-input"
                        id="pickupTrue"
                        name="pickup"
                        type="radio"
                        checked={isPickup && "checked"}
                        onClick={() => handleChangeIsPickup(true)}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="pickupTrue"
                      >
                        {t("PICK_UP_AT_COMPANY")}
                      </label>
                    </div>
                  </div>

                  {!isPickup && (
                    <p className="text-center">
                      {t("SHIPPING_TO")}:{" "}
                      {get(shippingAddress, "firstnameTH", "")}{" "}
                      {get(shippingAddress, "lastnameTH", "")}{" "}
                      <NumberFormat
                        value={shippingAddress.phoneNumber}
                        type="tel"
                      />
                      <br />
                      {`${t("HOUSE_NO")} ${shippingAddress.houseNo}`}{" "}
                      {shippingAddress.village &&
                        `${t("VILLAGE")}${shippingAddress.village}`}{" "}
                      {shippingAddress.building &&
                        `${t("BUILDING")}${shippingAddress.building}`}{" "}
                      {shippingAddress.floor &&
                        `${t("FLOOR")} ${shippingAddress.floor}`}{" "}
                      {shippingAddress.room &&
                        `${t("ROOM")} ${shippingAddress.room}`}{" "}
                      {shippingAddress.villageNo &&
                        `${t("VILLAGE_NO")} ${shippingAddress.villageNo}`}
                      <br />
                      {shippingAddress.lane &&
                        `${t("LANE")}${shippingAddress.lane}`}{" "}
                      {shippingAddress.road &&
                        `${t("ROAD")}${shippingAddress.road}`}{" "}
                      {`${
                        shippingAddress.province === "กรุงเทพมหานคร"
                          ? t("SUB_DISTRICT_TOWN")
                          : t("SUB_DISTRICT_COUNTRY")
                      }${shippingAddress.subDistrict}`}{" "}
                      {`${
                        shippingAddress.province === "กรุงเทพมหานคร"
                          ? t("DISTRICT_TOWN")
                          : t("DISTRICT_COUNTRY")
                      }${shippingAddress.district}`}
                      <br />
                      {`${
                        shippingAddress.province !== "กรุงเทพมหานคร"
                          ? t("PROVINCE")
                          : ""
                      }${shippingAddress.province}`}{" "}
                      {shippingAddress.zipCode}
                      <br />
                      {isConfirmOrder === 0 && (
                        <>
                          <a
                            href="javascript:void(0)"
                            onClick={() => setIsShowModal(true)}
                          >
                            <i className="fa fa-pen"></i>{" "}
                            {t("CHANGE_SHIPPING_ADDRESS")}
                          </a>
                          {isShowModal && (
                            <Address
                              addresses={user.addresses}
                              onChange={handleChangeAddress}
                            />
                          )}
                        </>
                      )}
                    </p>
                  )}
                  {isPickup && (
                    <p className="text-center">
                      {t("TEL")}{" "}
                      <NumberFormat value={user.phoneNumber} type="tel" />
                    </p>
                  )}

                  <h2 className="h2 card-title text-center mb-2">
                    {t("PAYMENT_METHOD")}
                  </h2>
                  <div className="text-center">
                    <div className="custom-control custom-radio mb-3 d-inline-block mr-4">
                      <input
                        disabled={isConfirmOrder > 0}
                        className="custom-control-input"
                        id="paymentTransfer"
                        name="paymentMethod"
                        type="radio"
                        checked={
                          paymentMethod === CONST_PAYMENT_METHOD.TRANSFER &&
                          "checked"
                        }
                        onClick={() =>
                          setPaymentMethod(CONST_PAYMENT_METHOD.TRANSFER)
                        }
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="paymentTransfer"
                      >
                        {t("TRANSFER_MONEY")}
                      </label>
                    </div>
                    {/* <div className="custom-control custom-radio mb-3 d-inline-block">
										<input
											disabled={isConfirmOrder > 0 || isPickup}
											className="custom-control-input"
											id="paymentCOD"
											name="paymentMethod"
											type="radio"
											checked={paymentMethod === CONST_PAYMENT_METHOD.COD && 'checked'}
											onClick={() => setPaymentMethod(CONST_PAYMENT_METHOD.COD)}
										/>
										<label
											className="custom-control-label"
											htmlFor="paymentCOD"
										>
											{t('CASH_ON_DELIVERY')}
										</label>
									</div> */}
                  </div>
                  {isConfirmOrder > 0 &&
                    bankAccount.map((bank, index) => (
                      <div className="pt-4 text-center" key={index}>
                        <h2 className="h2 card-title text-primary">
                          {t("ACCOUNT_NUMBER")}:{" "}
                          <NumberFormat
                            value={bank.accountNumber}
                            type="account"
                          />
                        </h2>
                        <h5 className="h3 title">
                          <span className="d-block mb-1">
                            {t("ACCOUNT_NAME")}:{" "}
                            {get(bank, `accountName${i18n.language}`)}
                          </span>
                          <small className="h4 font-weight-light text-muted">
                            {t("BANK")}: {get(bank, `bankName${i18n.language}`)}
                          </small>
                          <br />
                          <small className="h4 font-weight-light text-muted">
                            {t("BRANCH")}:{" "}
                            {get(bank, `branchName${i18n.language}`)}{" "}
                            {t("ACCOUNT_TYPE")}:{" "}
                            {get(bank, `accountType${i18n.language}`)}
                          </small>
                        </h5>
                      </div>
                    ))}

                  <div className="text-center">
                    <h1 className="h1 card-title mt-5 text-danger">
                      {t("AMOUNT")}{" "}
                      <NumberFormat
                        value={totalPrice + (isPickup ? 0 : shippingFee)}
                      />{" "}
                      {t("BAHT")}
                    </h1>
                    {isConfirmOrder === 0 && (
                      <Button
                        className="btn-icon btn-3"
                        color="primary"
                        type="button"
                        onClick={confirmOrder}
                      >
                        <span className="btn-inner--icon">
                          <i className="fas fa-shopping-cart"></i>
                        </span>
                        <span className="btn-inner--text">
                          {t("CONFIRM_ORDER")}
                        </span>
                      </Button>
                    )}
                  </div>
                  {isConfirmOrder > 0 && (
                    <Form onSubmit={handleSubmit(onSubmit)}>
                      <FormInput
                        label={t("SLIP")}
                        name="slip"
                        type="file"
                        maxFiles={1}
                        control={control}
                        rules={{ required: true }}
                      />
                      <FormInput
                        label={t("REMARK")}
                        name="remark"
                        type="textarea"
                        rows={3}
                        control={control}
                      />
                      <div className="text-center">
                        <Button color="primary" type="submit">
                          {t("ORDER")}
                        </Button>
                      </div>
                    </Form>
                  )}
                </CardBody>
              </Card>
            </Col>
          </Row>
        )}
      </Container>
    </>
  );
};

export default Cart;
