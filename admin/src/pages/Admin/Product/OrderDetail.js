import {
  API_BANK_ACCOUNT,
  API_ORDER_SUMMARY,
  API_PRODUCT,
  API_STOCK_QUANTITY,
  API_TRANSACTION_CHARGE,
  API_TRANSACTION_ORDER,
  API_TRANSACTION_ORDER_LIST,
  API_URL,
  CONST_PAYMENT_METHOD,
  CONST_PAYMENT_STATUS,
} from "config";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Form,
  Row,
  Table,
  UncontrolledTooltip,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { createData, deleteData, findAll, findOne, updateData } from "api";
import {
  hideSpinner,
  notifySuccess,
  showAlertSuccess,
  showConfirm,
  showSpinner,
} from "redux/actions";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Address from "./Address";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { FormInput } from "components/Forms";
import { NumberFormat } from "components/Shared";
import SimpleHeader from "components/Headers/SimpleHeader";
import { get } from "lodash";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";

const OrderDetail = () => {
  const { t, i18n } = useTranslation();
  const { id } = useParams();
  const history = useHistory();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [products, setProducts] = useState([]);
  const [orderList, setOrderList] = useState();
  const [bankAccount, setBankAccount] = useState([]);
  const [isShowModal, setIsShowModal] = useState(false);
  const [copiedText, setCopiedText] = useState("");

  const getOrder = async () => {
    dispatch(showSpinner());
    const response = await findAll(API_PRODUCT);
    if (response) setProducts(response);

    const response2 = await findOne(API_TRANSACTION_ORDER, id);
    if (response2) setOrderList(response2);
    dispatch(hideSpinner());
  };

  const getBankAccount = async () => {
    const bank = await findAll(API_BANK_ACCOUNT);
    setBankAccount(bank);
  };

  const deleteOrder = async () => {
    if (get(user, "role.type", "") === "admin") {
      const message = {
        text: t("CONFIRM_DELETE"),
        onConfirm: async () => {
          const response = await deleteData(API_TRANSACTION_ORDER, id);
          if (response) {
            for (let i = 0; i < response.tOrderLists.length; i++) {
              const orderId = get(response.tOrderLists[i], "id");
              const productStock = get(response.tOrderLists[i], "product");
              const response2 = await deleteData(
                API_TRANSACTION_ORDER_LIST,
                orderId
              );
              response2 && (await updateData(API_STOCK_QUANTITY, productStock));
            }

            const message = {
              text: t("DELETE_SUCCESS"),
            };
            dispatch(notifySuccess(message));
            history.push("/admin/orders");
          }
        },
      };
      dispatch(showConfirm(message));
    }
  };

  const onSubmitAdmin = async (data) => {
    const {
      shippingCarrier = "",
      trackingId = "",
      shippingCost = 0,
      packageCost = 0,
    } = data;
    const response = await updateData(
      API_TRANSACTION_ORDER,
      id,
      {
        shippingCarrier,
        trackingId,
        shippingCost,
        packageCost,
        status: CONST_PAYMENT_STATUS.SHIPPING,
      },
      dispatch
    );
    if (response) {
      const message = {
        text: t("UPDATE_SUCCESS"),
      };
      dispatch(showAlertSuccess(message));
      history.push("/admin/orders");
    }
  };

  const onSubmitDealer = async (data) => {
    const response = await updateData(
      API_TRANSACTION_ORDER,
      id,
      {
        slip: data.slip,
        status: CONST_PAYMENT_STATUS.INPROGRESS,
        remark: data.remark,
      },
      dispatch
    );
    if (response) {
      const response2 = await findOne(API_ORDER_SUMMARY, user.orderSummary.id);
      if (response2) {
        await updateData(API_ORDER_SUMMARY, user.orderSummary.id, {
          totalQuantity: response2.totalQuantity + orderList.totalQuantity,
          totalPrice: response2.totalPrice + orderList.totalPrice,
          totalPoint: response2.totalPoint + orderList.totalPoint,
        });

        await createData(API_TRANSACTION_CHARGE, {
          price: orderList.totalQuantity,
        });
      }
      const message = {
        text: t("CART__ORDER_SUCCESS"),
      };
      dispatch(showAlertSuccess(message));
      history.push("/admin/orders");
    }
  };

  const handleChangeAddress = async (address) => {
    if (address) {
      const response = await updateData(API_TRANSACTION_ORDER, id, {
        address: address.id,
      });
      setOrderList(response);
    }

    setTimeout(() => {
      setIsShowModal(false);
    }, 1000);
  };

  useEffect(() => {
    getOrder();
    getBankAccount();
  }, []);

  const renderAddress = (user, address, isPickup = false) => {
    if (isPickup) {
      return (
        <>
          <p className="text-center">{t("PICK_UP_AT_COMPANY")}</p>
          <p className="text-center">
            {t("TEL")}{" "}
            <NumberFormat value={get(user, "phoneNumber", "")} type="tel" />
          </p>
        </>
      );
    } else if (address) {
      return (
        <p className="text-center">
          {t("SHIPPING_TO")}: {get(address, "firstnameTH", "")}{" "}
          {get(address, "lastnameTH", "")}{" "}
          <NumberFormat value={get(address, "phoneNumber", "")} type="tel" />
          <br />
          {`${t("HOUSE_NO")} ${address.houseNo}`}{" "}
          {address.village && `${t("VILLAGE")}${address.village}`}{" "}
          {address.building && `${t("BUILDING")}${address.building}`}{" "}
          {address.floor && `${t("FLOOR")} ${address.floor}`}{" "}
          {address.room && `${t("ROOM")} ${address.room}`}{" "}
          {address.villageNo && `${t("VILLAGE_NO")} ${address.villageNo}`}
          <br />
          {address.lane && `${t("LANE")}${address.lane}`}{" "}
          {address.road && `${t("ROAD")}${address.road}`}{" "}
          {`${
            address.province === "กรุงเทพมหานคร"
              ? t("SUB_DISTRICT_TOWN")
              : t("SUB_DISTRICT_COUNTRY")
          }${address.subDistrict}`}{" "}
          {`${
            address.province === "กรุงเทพมหานคร"
              ? t("DISTRICT_TOWN")
              : t("DISTRICT_COUNTRY")
          }${address.district}`}
          <br />
          {`${address.province !== "กรุงเทพมหานคร" ? t("PROVINCE") : ""}${
            address.province
          }`}{" "}
          {address.zipCode}
        </p>
      );
    }

    return;
  };

  const renderAdmin = () => {
    const slip = get(orderList, "slip.url");
    const firstnameTH = get(orderList, "address.firstnameTH", "");
    const lastnameTH = get(orderList, "address.lastnameTH", "");
    const tel = get(orderList, "address.phoneNumber", "");
    const houseNo = get(orderList, "address.houseNo", "");
    const village = get(orderList, "address.village");
    const building = get(orderList, "address.building");
    const floor = get(orderList, "address.floor");
    const room = get(orderList, "address.room");
    const villageNo = get(orderList, "address.villageNo");
    const lane = get(orderList, "address.lane", "");
    const road = get(orderList, "address.road", "");
    const subDistrict = get(orderList, "address.subDistrict", "");
    const district = get(orderList, "address.district", "");
    const province = get(orderList, "address.province", "");
    const zipCode = get(orderList, "address.zipCode", "");
    const address =
      `${firstnameTH} ${lastnameTH} ${t("TEL")}.${tel} ${houseNo}${
        village ? ` ${t("VILLAGE")}${village}` : ""
      }${building ? ` ${t("BUILDING")}${building}` : ""}${
        floor ? ` ${t("FLOOR")} ${floor}` : ""
      }${room ? ` ${t("ROOM")} ${room}` : ""}${
        villageNo ? ` ${t("VILLAGE_NO")} ${villageNo}` : ""
      }` +
      `${lane ? ` ${t("LANE")}${lane}` : ""}${
        road ? ` ${t("ROAD")}${road}` : ""
      } ${`${
        province === "กรุงเทพมหานคร"
          ? t("SUB_DISTRICT_TOWN")
          : t("SUB_DISTRICT_COUNTRY")
      }${subDistrict}`} ${`${
        province === "กรุงเทพมหานคร"
          ? t("DISTRICT_TOWN")
          : t("DISTRICT_COUNTRY")
      }${district}`} ` +
      `${`${
        province !== "กรุงเทพมหานคร" ? t("PROVINCE") : ""
      }${province}`} ${zipCode}`;

    return (
      <Row className="justify-content-center">
        <Col xs={12} sm={6}>
          <Card>
            <CardHeader>
              <h5 className="h3 mb-0">{t("STATUS")}</h5>
            </CardHeader>

            <CardBody>
              <div className="h1 text-center mt-3">
                {t("DEALER")}:{" "}
                {get(orderList, `user.firstname${i18n.language}`, "")}{" "}
                {get(orderList, `user.lastname${i18n.language}`, "")}
              </div>
              {orderList.status !== CONST_PAYMENT_STATUS.PENDING &&
                renderAddress(
                  orderList.user,
                  orderList.address,
                  orderList.isPickup
                )}
              <div className="text-center">
                <CopyToClipboard
                  text={address}
                  onCopy={() => setCopiedText(address)}
                >
                  <Button id="copy-address" type="button">
                    <div>
                      <i className="ni ni-single-copy-04" />
                      <span>{t("COPY_ADDRESS")}</span>
                    </div>
                  </Button>
                </CopyToClipboard>
                <UncontrolledTooltip
                  delay={0}
                  trigger="hover focus"
                  target="copy-address"
                >
                  {copiedText === address
                    ? t("COPIED")
                    : t("COPY_TO_CLIPBOARD")}
                </UncontrolledTooltip>
              </div>

              <div className="pt-4 text-center">
                {orderList.remark && <p>หมายเหตุ: {orderList.remark}</p>}
                <h2 className="h2 card-title text-primary">
                  {t(orderList.status)}
                </h2>
                {orderList.status === CONST_PAYMENT_STATUS.SHIPPING &&
                  !orderList.isPickup && (
                    <h5 className="h3 title">
                      <span className="d-block mb-1">
                        {t("SHIPPING_BY")}: {orderList.shippingCarrier}
                      </span>
                      <small className="h4 font-weight-light text-muted">
                        {t("TRACKING_ID")}: {orderList.trackingId}
                      </small>
                      <br />
                      <small className="h4 font-weight-light text-muted">
                        {t("SHIPPING_COST")} {orderList.shippingCost}{" "}
                        {t("BAHT")}
                      </small>
                      &nbsp;
                      <small className="h4 font-weight-light text-muted">
                        {t("PACKAGE_COST")} {orderList.packageCost} {t("BAHT")}
                      </small>
                    </h5>
                  )}
              </div>

              {slip && (
                <img src={`${API_URL}${slip}`} className="card-img-top" />
              )}
              {orderList.paymentMethod === CONST_PAYMENT_METHOD.COD && (
                <>
                  <h2 className="h2 card-title text-danger text-center">
                    {t("CASH_ON_DELIVERY")}
                  </h2>
                  <h1 className="h1 card-title text-danger text-center">
                    {t("AMOUNT")}{" "}
                    <NumberFormat
                      value={
                        orderList.totalPrice +
                        (orderList.isPickup ? 0 : orderList.shippingFee)
                      }
                    />{" "}
                    {t("BAHT")}
                  </h1>
                </>
              )}

              {orderList.status === CONST_PAYMENT_STATUS.INPROGRESS && (
                <Form onSubmit={handleSubmit(onSubmitAdmin)} className="mt-4">
                  {!orderList.isPickup && (
                    <>
                      <Row>
                        <Col xs={12} sm={6}>
                          <FormInput
                            label={t("SHIPPING_BY")}
                            name="shippingCarrier"
                            control={control}
                          />
                        </Col>
                        <Col xs={12} sm={6}>
                          <FormInput
                            label={t("TRACKING_ID")}
                            name="trackingId"
                            control={control}
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={6}>
                          <FormInput
                            label={t("SHIPPING_COST")}
                            name="shippingCost"
                            control={control}
                            type="money"
                            rules={{ required: true }}
                          />
                        </Col>
                        <Col xs={12} sm={6}>
                          <FormInput
                            label={t("PACKAGE_COST")}
                            name="packageCost"
                            control={control}
                            type="money"
                            rules={{ required: true }}
                          />
                        </Col>
                      </Row>
                    </>
                  )}
                  <div className="text-center">
                    <Button color="primary" type="submit">
                      {t("SHIPPING")}
                    </Button>
                  </div>
                </Form>
              )}

              {orderList.status === CONST_PAYMENT_STATUS.PENDING && (
                <div className="text-center">
                  <Button
                    className="btn-icon btn-3"
                    color="danger"
                    type="button"
                    onClick={deleteOrder}
                  >
                    <span className="btn-inner--icon">
                      <i className="fas fa-trash-alt"></i>
                    </span>
                    <span className="btn-inner--text">{t("DELETE")}</span>
                  </Button>
                </div>
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  };

  const renderDealer = () => {
    const slip = get(orderList, "slip.url");

    return (
      <>
        {orderList.status !== CONST_PAYMENT_STATUS.PENDING && (
          <Row className="justify-content-center">
            <Col xs={12} sm={6}>
              <Card>
                <CardHeader>
                  <h5 className="h3 mb-0">{t("STATUS")}</h5>
                </CardHeader>

                <CardBody>
                  <div className="h1 text-center mt-3">
                    {t("DEALER")}:{" "}
                    {get(orderList, `user.firstname${i18n.language}`, "")}{" "}
                    {get(orderList, `user.lastname${i18n.language}`, "")}
                  </div>
                  {renderAddress(
                    orderList.user,
                    orderList.address,
                    orderList.isPickup
                  )}
                  {!orderList.isPickup &&
                    orderList.status === CONST_PAYMENT_STATUS.INPROGRESS && (
                      <div className="text-center">
                        <a
                          href="javascript:void(0)"
                          onClick={() => setIsShowModal(true)}
                        >
                          <i className="fa fa-pen"></i>{" "}
                          {t("CHANGE_SHIPPING_ADDRESS")}
                        </a>
                        {isShowModal && (
                          <Address
                            addresses={user.addresses}
                            onChange={handleChangeAddress}
                          />
                        )}
                      </div>
                    )}

                  <div className="pt-4 text-center">
                    {orderList.remark && <p>หมายเหตุ: {orderList.remark}</p>}
                    <h2 className="h2 card-title text-primary">
                      {t(orderList.status)}
                    </h2>
                    {orderList.status === CONST_PAYMENT_STATUS.SHIPPING && (
                      <h5 className="h3 title">
                        <span className="d-block mb-1">
                          {t("SHIPPING_BY")}: {orderList.shippingCarrier}
                        </span>
                        <small className="h4 font-weight-light text-muted">
                          {t("TRACKING_ID")}: {orderList.trackingId}
                        </small>
                      </h5>
                    )}
                  </div>

                  {slip && (
                    <img src={`${API_URL}${slip}`} className="card-img-top" />
                  )}
                </CardBody>
              </Card>
            </Col>
          </Row>
        )}

        {orderList.status === CONST_PAYMENT_STATUS.PENDING && (
          <Row className="justify-content-center">
            <Col xs={12} sm={6}>
              <Card>
                <CardHeader>
                  <h5 className="h3 mb-0">{t("PAYMENT")}</h5>
                </CardHeader>

                <CardBody>
                  <div className="h1 text-center mt-3">
                    {t("DEALER")}:{" "}
                    {get(orderList, `user.firstname${i18n.language}`, "")}{" "}
                    {get(orderList, `user.lastname${i18n.language}`, "")}
                  </div>
                  {renderAddress(
                    orderList.user,
                    orderList.address,
                    orderList.isPickup
                  )}
                  {!orderList.isPickup && (
                    <div className="text-center">
                      <a
                        href="javascript:void(0)"
                        onClick={() => setIsShowModal(true)}
                      >
                        <i className="fa fa-pen"></i>{" "}
                        {t("CHANGE_SHIPPING_ADDRESS")}
                      </a>
                      {isShowModal && (
                        <Address
                          addresses={user.addresses}
                          onChange={handleChangeAddress}
                        />
                      )}
                    </div>
                  )}

                  <h2 className="h2 card-title text-center mt-5">
                    {t("PAYMENT_METHOD")} {t("TRANSFER_MONEY")}
                  </h2>
                  {bankAccount.map((bank, index) => (
                    <div className="pt-4 text-center" key={index}>
                      <h2 className="h2 card-title text-primary">
                        {t("ACCOUNT_NUMBER")}:{" "}
                        <NumberFormat
                          value={bank.accountNumber}
                          type="account"
                        />
                      </h2>
                      <h5 className="h3 title">
                        <span className="d-block mb-1">
                          {t("ACCOUNT_NAME")}:{" "}
                          {get(bank, `accountName${i18n.language}`)}
                        </span>
                        <small className="h4 font-weight-light text-muted">
                          {t("BANK")}: {get(bank, `bankName${i18n.language}`)}
                        </small>
                        <br />
                        <small className="h4 font-weight-light text-muted">
                          {t("BRANCH")}:{" "}
                          {get(bank, `branchName${i18n.language}`)}{" "}
                          {t("ACCOUNT_TYPE")}:{" "}
                          {get(bank, `accountType${i18n.language}`)}
                        </small>
                      </h5>
                    </div>
                  ))}

                  <div className="text-center">
                    <h2 className="h2 card-title mt-5">
                      {t("AMOUNT")}{" "}
                      <NumberFormat
                        value={
                          orderList &&
                          orderList.totalPrice + orderList.shippingFee
                        }
                      />{" "}
                      {t("BAHT")}
                    </h2>
                  </div>
                  <Form onSubmit={handleSubmit(onSubmitDealer)}>
                    <FormInput
                      label={t("SLIP")}
                      name="slip"
                      type="file"
                      maxFiles={1}
                      control={control}
                      rules={{ required: true }}
                    />
                    <FormInput
                      label={t("REMARK")}
                      name="remark"
                      type="textarea"
                      rows={3}
                      control={control}
                    />
                    <div className="text-center">
                      <Button color="primary" type="submit">
                        {t("ORDER")}
                      </Button>
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        )}
      </>
    );
  };

  return (
    <>
      <SimpleHeader
        name={t("ORDER__LIST")}
        parentName={t("PRODUCT")}
      ></SimpleHeader>
      <Container className="mt--6" fluid>
        <Row className="card-wrapper">
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">{t("ORDER_LIST")}</h3>
              </CardHeader>

              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th>{t("PRODUCT__CODE")}</th>
                    <th>{t("IMAGE")}</th>
                    <th>{t("PRODUCT__NAME")}</th>
                    <th>{t("QUANTITY")}</th>
                    <th>{t("PRICE_PER_PIECE")}</th>
                    <th>{t("TOTAL_PRICE")}</th>
                    <th>{t("POINT")}</th>
                  </tr>
                </thead>
                <tbody className="list">
                  {orderList &&
                    orderList.tOrderLists.map((order, index) => {
                      const product = products.filter(
                        (item) => item.id === order.product
                      );
                      return (
                        <tr key={index}>
                          <th scope="row">
                            <NumberFormat
                              value={product.length > 0 && product[0].code}
                              type="barcode"
                            />
                          </th>
                          <td>
                            <div className="avatar-group">
                              {product.length > 0 &&
                                product[0][`image${i18n.language}`] &&
                                product[0][`image${i18n.language}`].map(
                                  ({ id, name, formats }, index) => {
                                    const url = get(formats, "thumbnail.url");
                                    if (url) {
                                      return (
                                        <>
                                          <a
                                            key={index}
                                            className="avatar avatar-sm rounded-circle"
                                            id={`tooltip${id}`}
                                            onClick={(e) => e.preventDefault()}
                                          >
                                            <img
                                              alt={name}
                                              src={`${API_URL}${url}`}
                                              className="thumbnail"
                                            />
                                          </a>
                                          <UncontrolledTooltip
                                            delay={0}
                                            target={`tooltip${id}`}
                                          >
                                            {name}
                                          </UncontrolledTooltip>
                                        </>
                                      );
                                    } else {
                                      return null;
                                    }
                                  }
                                )}
                            </div>
                          </td>
                          <td>
                            {product.length > 0 &&
                              product[0][`name${i18n.language}`]}
                          </td>
                          <td>
                            <NumberFormat value={order.quantity} />
                          </td>
                          <td>
                            <NumberFormat
                              value={order.totalPrice / order.quantity}
                              type="money"
                            />
                          </td>
                          <td>
                            <NumberFormat
                              value={order.totalPrice}
                              type="money"
                            />
                          </td>
                          <td>
                            <NumberFormat value={order.totalPoint} />
                          </td>
                        </tr>
                      );
                    })}
                  <tr>
                    <th scope="row"></th>
                    <td></td>
                    <td>{t("SHIPPING_FEE")}</td>
                    <td></td>
                    <td></td>
                    <td>
                      {orderList && (
                        <NumberFormat
                          value={orderList.shippingFee}
                          type="money"
                        />
                      )}
                    </td>
                    <td></td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td></td>
                    <td></td>
                    <td>{t("TOTAL")}</td>
                    <td>
                      {orderList && (
                        <NumberFormat value={orderList.totalQuantity} />
                      )}
                    </td>
                    <td></td>
                    <td>
                      {orderList && (
                        <NumberFormat
                          value={orderList.totalPrice + orderList.shippingFee}
                          type="money"
                        />
                      )}
                    </td>
                    <td>
                      {orderList && (
                        <NumberFormat value={orderList.totalPoint} />
                      )}
                    </td>
                  </tr>
                </tfoot>
              </Table>
            </Card>
          </div>
        </Row>

        {orderList &&
          (get(user, "role.type", "") === "admin"
            ? renderAdmin()
            : renderDealer())}
      </Container>
    </>
  );
};

export default OrderDetail;
