import { API_PRODUCT, API_URL } from "config";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Row,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { get, sortBy } from "lodash";
import { notify, showAlertWarning } from "redux/actions";
import { useDispatch, useSelector } from "react-redux";
import { NumberFormat } from "components/Shared";
import NumberFormatInput from "react-number-format";
import SimpleHeader from "components/Headers/SimpleHeader";
import { findAll } from "api";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Catalog = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const history = useHistory();
  const [products, setProducts] = useState([]);
  const [cartList, setCartList] = useState([]);
  const [amounts, setAmounts] = useState({});
  const [definePrice, setDefinePrice] = useState({});

  const cart = () => {
    history.push("/admin/cart", cartList);
  };

  const addToCart = (productId, amount, min, max, productWholesalePrices) => {
    const selectedProduct = products.filter(
      (product) => String(product.id) === productId
    );
    if (amount && Number(amount) > 0) {
      if (Number(amount) < min) {
        const message = {
          text: t("ORDER__MIN_PURCHASE", { quantity: min }),
        };
        dispatch(showAlertWarning(message));
        return;
      }

      if (Number(amount) > max) {
        const message = {
          text: t("ORDER__MAX_PURCHASE", { quantity: max }),
        };
        dispatch(showAlertWarning(message));
        return;
      }

      const { code, imageEN, imageTH, nameEN, nameTH, point } =
        selectedProduct[0];
      const wholesale = productWholesalePrices.filter(
        (item) => item.quantity <= amount
      );
      if (wholesale.length > 0) {
        const item = wholesale.slice(-1).pop();
        const price = item.price > 0 ? item.price : definePrice[code];
        const data = {
          product: productId,
          code,
          imageEN,
          imageTH,
          nameEN,
          nameTH,
          quantity: amount,
          price,
          totalPrice: price * amount,
          totalPoint: point * amount,
        };

        const message = {
          text: t("ADD_TO_CART_SUCCESS"),
        };
        dispatch(notify(message));
        setCartList([...cartList, data]);
      } else {
        const message = {
          text: t("INPUT_PRODUCT_QUANTITY"),
        };
        dispatch(showAlertWarning(message));
      }
    } else {
      const message = {
        text: t("INPUT_PRODUCT_QUANTITY"),
      };
      dispatch(showAlertWarning(message));
    }
  };

  const getProducts = async () => {
    const data = await findAll(API_PRODUCT, null, dispatch);
    let defaultAmount = {};
    let defaultDefinePrice = {};
    data.map((product) => {
      defaultAmount = { ...defaultAmount, [product.code]: 0 };
      defaultDefinePrice = {
        ...defaultDefinePrice,
        [product.code]: product.retailPrice,
      };
    });
    setAmounts(defaultAmount);
    setProducts(data);
    setDefinePrice(defaultDefinePrice);
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <>
      <SimpleHeader name={t("PRODUCT__LIST")} parentName={t("PRODUCT")}>
        <Button
          className="btn-icon btn-3"
          color="secondary"
          type="button"
          onClick={cart}
          disabled={cartList.length === 0}
        >
          <span className="btn-inner--icon">
            <i className="fas fa-shopping-cart"></i>
          </span>
          <span className="btn-inner--text">
            {t("CART")} ({cartList.length})
          </span>
        </Button>
      </SimpleHeader>
      <Container className="mt--6" fluid>
        <Row className="card-wrapper">
          {products.map((product, index) => {
            const productId = String(get(product, "id", ""));
            const imageUrl = get(product, `image${i18n.language}[0].url`);
            const quantity = get(product, "stock.quantity", 0);
            const allPrices = get(product, "productWholesalePrices", []);
            const filteredPrices = allPrices.filter(
              (price) =>
                quantity >= price.quantity && user.mLevel.id === price.mLevel
            );
            const productWholesalePrices = sortBy(filteredPrices, "quantity");

            return (
              <Col lg="4" key={index}>
                <Card className="card-pricing border-0 text-center mb-4">
                  <CardHeader className="bg-transparent">
                    <h4 className="text-uppercase ls-1 text-primary py-3 mb-0">
                      {product[`name${i18n.language}`]}
                    </h4>
                  </CardHeader>
                  <CardBody>
                    {imageUrl && (
                      <img
                        className="img-center img-fluid shadow shadow-lg--hover"
                        src={`${API_URL}${imageUrl}`}
                        style={{ width: "200px" }}
                      />
                    )}
                    <div className="display-2 mt-3">
                      <NumberFormat
                        className="font-weight-bold"
                        value={product.retailPrice}
                        type="money"
                      />
                    </div>
                    {productWholesalePrices.length > 0 ? (
                      <span className="text-success">
                        {t("IN_STOCK")} <NumberFormat value={quantity} />{" "}
                        {t("PIECE")}
                      </span>
                    ) : (
                      <span className="text-danger">{t("OUT_OF_STOCK")}</span>
                    )}
                    <br />
                    <br />
                    {productWholesalePrices.length > 0 && (
                      <>
                        {productWholesalePrices[0].price > 0 ? (
                          <div>
                            <table className="table table-responsive">
                              <thead className="table-light">
                                <tr>
                                  <th>{t("QUANTITY")}</th>
                                  <th>{t("PRICE_PER_PIECE")}</th>
                                  <th>{t("COST")}</th>
                                  <th>{t("PROFIT")}</th>
                                </tr>
                              </thead>
                              <tbody>
                                {productWholesalePrices.map((item, key) => (
                                  <tr key={key}>
                                    <th>
                                      <NumberFormat value={item.quantity} />
                                    </th>
                                    <th>
                                      <NumberFormat
                                        value={item.price}
                                        type="money"
                                      />
                                    </th>
                                    <th>
                                      <NumberFormat
                                        value={item.quantity * item.price}
                                        type="money"
                                      />
                                    </th>
                                    <th>
                                      <NumberFormat
                                        value={
                                          item.quantity * product.retailPrice -
                                          item.quantity * item.price
                                        }
                                        type="money"
                                      />
                                    </th>
                                  </tr>
                                ))}
                              </tbody>
                            </table>
                          </div>
                        ) : (
                          <>
                            <label>{t("PRICE_PER_PIECE")}</label>
                            <NumberFormatInput
                              prefix="฿ "
                              className="form-control text-center"
                              thousandSeparator={true}
                              allowNegative={false}
                              decimalScale={0}
                              value={definePrice[product.code]}
                              onValueChange={(e) =>
                                setDefinePrice({
                                  ...definePrice,
                                  [product.code]: e.floatValue,
                                })
                              }
                            />
                            <br />
                          </>
                        )}
                        <label>{t("QUANTITY")}</label>
                        <NumberFormatInput
                          className="form-control text-center"
                          defaultValue={0}
                          thousandSeparator={true}
                          allowNegative={false}
                          decimalScale={0}
                          onValueChange={(e) =>
                            setAmounts({
                              ...amounts,
                              [product.code]: e.floatValue,
                            })
                          }
                        />
                        <Button
                          className="mb-3 mt-3"
                          color="primary"
                          type="button"
                          onClick={() =>
                            addToCart(
                              productId,
                              amounts[product.code],
                              productWholesalePrices[0].quantity,
                              quantity,
                              productWholesalePrices
                            )
                          }
                        >
                          {t("ADD_TO_CART")}
                        </Button>
                      </>
                    )}
                  </CardBody>
                </Card>
              </Col>
            );
          })}
        </Row>
      </Container>
    </>
  );
};

export default Catalog;
