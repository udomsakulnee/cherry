import {
  API_STOCK_QUANTITY,
  API_TRANSACTION_GOODS_RECEIVED,
  API_TRANSACTION_GOODS_RECEIVED_LIST,
} from "config";
import {
  Button,
  Card,
  CardHeader,
  Container,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
import React, { useState } from "react";
import { deleteData, findLimit, updateData } from "api";
import { notifySuccess, showConfirm } from "redux/actions";
import { NumberFormat } from "components/Shared";
import SimpleHeader from "components/Headers/SimpleHeader";
import { SmartTable } from "components/Shared";
import { dayjs } from "utils";
import { get } from "lodash";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const GoodsReceived = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();
  const [total, setTotal] = useState(0);
  const [goodsReceived, setGoodsReceived] = useState([]);

  const getGoodsReceived = async (params) => {
    const { data, total = 0 } = await findLimit(
      API_TRANSACTION_GOODS_RECEIVED,
      params,
      dispatch
    );
    if (total > 0) {
      setGoodsReceived(data);
      setTotal(total);
    } else {
      setGoodsReceived([]);
      setTotal(0);
    }
  };

  const goodsReceivedDetail = (id) => () => {
    history.push("/admin/goods-received/" + id);
  };

  const deleteGoodsReceived = (id) => () => {
    const message = {
      text: t("CONFIRM_DELETE"),
      onConfirm: async () => {
        const response = await deleteData(API_TRANSACTION_GOODS_RECEIVED, id);
        if (response) {
          for (let i = 0; i < response.tGoodsReceivedLists.length; i++) {
            const productId = get(response.tGoodsReceivedLists[i], "id");
            const productStock = get(
              response.tGoodsReceivedLists[i],
              "product"
            );
            const response2 = await deleteData(
              API_TRANSACTION_GOODS_RECEIVED_LIST,
              productId
            );
            response2 && (await updateData(API_STOCK_QUANTITY, productStock));
          }

          const message = {
            text: t("DELETE_SUCCESS"),
          };
          dispatch(notifySuccess(message));
          getGoodsReceived();
        }
      },
    };
    dispatch(showConfirm(message));
  };

  return (
    <>
      <SimpleHeader name={t("GOODS__RECEIVED")} parentName={t("PRODUCT")}>
        <Button
          className="btn-icon btn-3"
          color="secondary"
          type="button"
          onClick={goodsReceivedDetail("create")}
        >
          <span className="btn-inner--icon">
            <i className="fas fa-plus-circle"></i>
          </span>
          <span className="btn-inner--text">{t("GOODS__RECEIVED_NEW")}</span>
        </Button>
      </SimpleHeader>
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">{t("GOODS__RECEIVED")}</h3>
              </CardHeader>

              <SmartTable total={total} onReload={getGoodsReceived}>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>{t("CREATE_DATE")}</th>
                    <th>{t("TOTAL_COST")}</th>
                    <th>{t("QUANTITY")}</th>
                    <th>{t("BY")}</th>
                    <th>{t("ACTION")}</th>
                  </tr>
                </thead>
                <tbody>
                  {goodsReceived.map((item, index) => {
                    const itemList = get(item, "tGoodsReceivedLists", []);
                    const stock = itemList.reduce((a, b) => ({
                      quantity: a.quantity + b.quantity,
                      totalCost: a.totalCost + b.totalCost,
                    }));
                    return (
                      <tr key={index}>
                        <th scope="row">{item.id}</th>
                        <th>
                          {dayjs(item.created_at).format("D MMMM YYYY H:mm")}
                        </th>
                        <td>
                          <NumberFormat value={stock.totalCost} type="money" />
                        </td>
                        <td>
                          <NumberFormat value={stock.quantity} />
                        </td>
                        <td>{`${get(
                          item,
                          `user.firstname${i18n.language}`
                        )} ${get(item, `user.lastname${i18n.language}`)}`}</td>
                        <td className="table-actions">
                          <a
                            className="table-action p-1"
                            href="javascript:void(0)"
                            id="edit-product"
                            onClick={goodsReceivedDetail(item.id)}
                          >
                            <i className="fas fa-edit" />
                          </a>
                          <UncontrolledTooltip delay={0} target="edit-product">
                            {t("EDIT")}
                          </UncontrolledTooltip>
                          <a
                            className="table-action table-action-delete p-1"
                            href="javascript:void(0)"
                            id="delete-product"
                            onClick={deleteGoodsReceived(item.id)}
                          >
                            <i className="fas fa-trash" />
                          </a>
                          <UncontrolledTooltip
                            delay={0}
                            target="delete-product"
                          >
                            {t("DELETE")}
                          </UncontrolledTooltip>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </SmartTable>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default GoodsReceived;
