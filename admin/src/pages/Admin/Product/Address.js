import { API_ADDRESS, API_USER_ME } from "config";
import {
  Button,
  Col,
  Form,
  ListGroup,
  ListGroupItem,
  Modal,
  Row,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { createData, findAll, updateData } from "api";
import {
  hideSpinner,
  notifyError,
  notifySuccess,
  setUser,
  showSpinner,
} from "redux/actions";
import { useDispatch, useSelector } from "react-redux";
import { FormInput } from "components/Forms";
import { NumberFormat } from "components/Shared";
import PropTypes from "prop-types";
import { noop } from "lodash";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";

const Address = ({ addresses, onChange }) => {
  const [isShow, setIsShow] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [isDefault, setIsDefault] = useState(false);
  const [id, setId] = useState();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const {
    handleSubmit,
    control,
    setValue,
    register,
    reset,
    formState: { errors },
  } = useForm();

  const handleChange = (data) => {
    setIsShow(false);
    onChange(data);
  };

  const handleEditAddress = (data) => {
    reset(data);
    setId(data.id);
    setIsDefault(data.isDefault);
    setIsEdit(true);
  };

  useEffect(() => {
    setIsShow(true);
  }, []);

  const handleChangeAddress = (address) => {
    setValue("subDistrict", address.subDistrict);
    setValue("district", address.district);
    setValue("province", address.province);
    setValue("zipCode", address.zipCode);
  };

  const onSubmit = async (data) => {
    dispatch(showSpinner());
    const request = {
      ...data,
      zipCode: String(data.zipCode),
      user: user.id,
    };
    let response;
    if (id) {
      response = await updateData(API_ADDRESS, id, request);
    } else {
      response = await createData(API_ADDRESS, request);
    }

    if (request.isDefault) {
      for (let i = 0; i < addresses.length; i++) {
        if (addresses[i].id !== id) {
          await updateData(API_ADDRESS, addresses[i].id, { isDefault: false });
        }
      }
    }
    if (response) {
      const user = await findAll(API_USER_ME);
      user && dispatch(setUser(user));
      handleChange(response);

      const message = {
        text: t("UPDATE_SUCCESS"),
      };
      dispatch(notifySuccess(message));
      dispatch(hideSpinner());
    } else {
      const message = {
        text: t("CREATE_FAIL"),
      };
      dispatch(notifyError(message));
      dispatch(hideSpinner());
    }
  };

  if (isEdit) {
    return (
      <Modal
        size="lg"
        className="modal-dialog-centered"
        isOpen={isShow}
        toggle={() => handleChange()}
      >
        <Form onSubmit={handleSubmit(onSubmit)}>
          <div className="modal-header">
            <h6 className="modal-title" id="modal-address-default">
              {t("CHANGE_SHIPPING_ADDRESS")}
            </h6>
            <button
              aria-label="Close"
              className="close"
              data-dismiss="modal"
              type="button"
              onClick={() => handleChange()}
            >
              <span aria-hidden={true}>×</span>
            </button>
          </div>
          <div className="modal-body">
            <Row>
              <Col xs={12} sm={4}>
                <FormInput
                  label={t("FIRSTNAME")}
                  name="firstnameTH"
                  control={control}
                />
              </Col>
              <Col xs={12} sm={4}>
                <FormInput
                  label={t("LASTNAME")}
                  name="lastnameTH"
                  control={control}
                />
              </Col>
              <Col xs={12} sm={4}>
                <FormInput
                  label={t("PHONE_NUMBER")}
                  name="phoneNumber"
                  type="tel"
                  control={control}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={4}>
                <FormInput
                  label={t("HOUSE_NO")}
                  name="houseNo"
                  control={control}
                  rules={{ required: true }}
                />
              </Col>
              <Col xs={12} sm={4}>
                <FormInput
                  label={t("VILLAGE")}
                  name="village"
                  control={control}
                />
              </Col>
              <Col xs={12} sm={4}>
                <FormInput
                  label={t("BUILDING")}
                  name="building"
                  control={control}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={4}>
                <FormInput label={t("FLOOR")} name="floor" control={control} />
              </Col>
              <Col xs={12} sm={4}>
                <FormInput label={t("ROOM")} name="room" control={control} />
              </Col>
              <Col xs={12} sm={4}>
                <FormInput
                  label={t("VILLAGE_NO")}
                  name="villageNo"
                  control={control}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={6}>
                <FormInput label={t("LANE")} name="lane" control={control} />
              </Col>
              <Col xs={12} sm={6}>
                <FormInput label={t("ROAD")} name="road" control={control} />
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={6}>
                <FormInput
                  label={t("SUB_DISTRICT")}
                  name="subDistrict"
                  type="address"
                  control={control}
                  rules={{ required: true }}
                  onChangeAddress={handleChangeAddress}
                />
              </Col>
              <Col xs={12} sm={6}>
                <FormInput
                  label={t("DISTRICT")}
                  name="district"
                  type="address"
                  control={control}
                  rules={{ required: true }}
                  onChangeAddress={handleChangeAddress}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={6}>
                <FormInput
                  label={t("PROVINCE")}
                  name="province"
                  type="address"
                  control={control}
                  rules={{ required: true }}
                  onChangeAddress={handleChangeAddress}
                />
              </Col>
              <Col xs={12} sm={6}>
                <FormInput
                  label={t("ZIP_CODE")}
                  name="zipCode"
                  type="address"
                  control={control}
                  rules={{ required: true }}
                  onChangeAddress={handleChangeAddress}
                />
              </Col>
            </Row>
            <Row>
              <Col xs="12">
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id="customCheckRegister"
                    type="checkbox"
                    name="isDefault"
                    ref={register()}
                    disabled={isDefault}
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="customCheckRegister"
                  >
                    <span className="text-muted">
                      {t("SET_DEFAULT_ADDTESS")}
                    </span>
                  </label>
                </div>
              </Col>
            </Row>
          </div>
          <div className="modal-footer">
            <Button color="success" type="submit">
              {t("SAVE")}
            </Button>
            <Button
              className="ml-auto"
              color="link"
              data-dismiss="modal"
              type="button"
              onClick={() => handleChange()}
            >
              {t("CLOSE")}
            </Button>
          </div>
        </Form>
      </Modal>
    );
  } else {
    return (
      <Modal
        size="lg"
        className="modal-dialog-centered"
        isOpen={isShow}
        toggle={() => handleChange()}
      >
        <div className="modal-header">
          <h6 className="modal-title" id="modal-address-default">
            {t("CHANGE_SHIPPING_ADDRESS")}
          </h6>
          <button
            aria-label="Close"
            className="close"
            data-dismiss="modal"
            type="button"
            onClick={() => handleChange()}
          >
            <span aria-hidden={true}>×</span>
          </button>
        </div>
        <div className="modal-body">
          <ListGroup flush>
            {addresses.map((item, index) => (
              <ListGroupItem
                key={index}
                className="list-group-item-action flex-column align-items-start py-4 px-4"
                href="javascript:void(0)"
                tag="a"
              >
                <div className="d-flex w-100 justify-content-between">
                  <div>
                    <div
                      className="d-flex w-100 align-items-center"
                      onClick={() => handleChange(item)}
                    >
                      <h3 className="mb-1">
                        {item.firstnameTH} {item.lastnameTH} |{" "}
                        <NumberFormat value={item.phoneNumber} type="tel" />
                      </h3>
                    </div>
                  </div>
                  {item.isDefault && (
                    <small className="text-warning">{t("DEFAULT")}</small>
                  )}
                </div>
                <div className="d-flex w-100 justify-content-between">
                  <div
                    className="d-flex w-100 align-items-center"
                    onClick={() => handleChange(item)}
                  >
                    <p className="text-sm mb-0">
                      {`${t("HOUSE_NO")} ${item.houseNo}`}{" "}
                      {item.village && `${t("VILLAGE")}${item.village}`}{" "}
                      {item.building && `${t("BUILDING")}${item.building}`}{" "}
                      {item.floor && `${t("FLOOR")} ${item.floor}`}{" "}
                      {item.room && `${t("ROOM")} ${item.room}`}{" "}
                      {item.villageNo && `${t("VILLAGE_NO")} ${item.villageNo}`}
                      <br />
                      {item.lane && `${t("LANE")}${item.lane}`}{" "}
                      {item.road && `${t("ROAD")}${item.road}`}{" "}
                      {`${
                        item.province === "กรุงเทพมหานคร"
                          ? t("SUB_DISTRICT_TOWN")
                          : t("SUB_DISTRICT_COUNTRY")
                      }${item.subDistrict}`}{" "}
                      {`${
                        item.province === "กรุงเทพมหานคร"
                          ? t("DISTRICT_TOWN")
                          : t("DISTRICT_COUNTRY")
                      }${item.district}`}
                      <br />
                      {`${
                        item.province !== "กรุงเทพมหานคร" ? t("PROVINCE") : ""
                      }${item.province}`}{" "}
                      {item.zipCode}
                    </p>
                  </div>
                  <a
                    className="text-primary text-right mt-4"
                    style={{ width: "70px" }}
                    onClick={() => handleEditAddress(item)}
                  >
                    <i className="fas fa-edit"></i> {t("EDIT")}
                  </a>
                </div>
              </ListGroupItem>
            ))}
          </ListGroup>
        </div>
        <div className="modal-footer">
          <Button color="primary" type="button" onClick={() => setIsEdit(true)}>
            {t("ADD_NEW_ADDRESS")}
          </Button>
          <Button
            className="ml-auto"
            color="link"
            data-dismiss="modal"
            type="button"
            onClick={() => handleChange()}
          >
            {t("CLOSE")}
          </Button>
        </div>
      </Modal>
    );
  }
};

Address.defaultProps = {
  addresses: [],
  onChange: noop,
};

Address.propTypes = {
  addresses: PropTypes.object,
  onChange: PropTypes.func,
};

export default Address;
