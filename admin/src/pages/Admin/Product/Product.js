import { API_PRODUCT, API_URL } from "config";
import {
  Button,
  Card,
  CardHeader,
  Container,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
import { NumberFormat, SmartTable } from "components/Shared";
import React, { useState } from "react";
import { findLimit } from "api";
import SimpleHeader from "components/Headers/SimpleHeader";
import { get } from "lodash";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Product = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();
  const [total, setTotal] = useState(0);
  const [products, setProducts] = useState([]);

  const getProducts = async (params) => {
    const { data, total = 0 } = await findLimit(API_PRODUCT, params, dispatch);
    if (total > 0) {
      setProducts(data);
      setTotal(total);
    } else {
      setProducts([]);
      setTotal(0);
    }
  };

  const productDetail = (id) => () => {
    history.push("/admin/products/" + id);
  };

  // const deleteProduct = (id) => () => {
  // 	const message = {
  // 		text: t('CONFIRM_DELETE'),
  // 		onConfirm: async () => {
  // 			const response = await deleteData(API_PRODUCT, id);
  // 			if (response) {
  // 				const stockId = get(response, 'stock.id');
  // 				stockId && deleteData(API_STOCK, stockId);

  // 				const productWholesalePrices = get(response, 'productWholesalePrices', []);
  // 				productWholesalePrices.map(({id}) => {
  // 					id && deleteData(API_PRODUCT_WHOLESALE_PRICE, id);
  // 					return id;
  // 				});

  // 				const imageTH = get(response, 'imageTH', []);
  // 				imageTH.map(({id}) => {
  // 					id && deleteUploadFile(id);
  // 					return id;
  // 				});

  // 				const imageEN = get(response, 'imageEN', []);
  // 				imageEN.map(({id}) => {
  // 					id && deleteUploadFile(id);
  // 					return id;
  // 				});

  // 				const message = {
  // 					text: t('DELETE_SUCCESS'),
  // 				};
  // 				dispatch(notifySuccess(message));
  // 				getProducts();
  // 			}
  // 		},
  // 	};
  // 	dispatch(showConfirm(message));
  // };

  return (
    <>
      <SimpleHeader name={t("PRODUCT__LIST")} parentName={t("PRODUCT")}>
        <Button
          className="btn-icon btn-3"
          color="secondary"
          type="button"
          onClick={productDetail("create")}
        >
          <span className="btn-inner--icon">
            <i className="fas fa-plus-circle"></i>
          </span>
          <span className="btn-inner--text">{t("PRODUCT__NEW")}</span>
        </Button>
      </SimpleHeader>
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">{t("PRODUCT")}</h3>
              </CardHeader>

              <SmartTable total={total} onReload={getProducts}>
                <thead>
                  <tr>
                    <th data-sort="code">{t("PRODUCT__CODE")}</th>
                    <th>{t("IMAGE")}</th>
                    <th data-sort={`name${i18n.language}`}>
                      {t("PRODUCT__NAME")}
                    </th>
                    <th data-sort="mProductUnit">{t("CATEGORY")}</th>
                    <th data-sort="retailPrice">{t("RETAIL_PRICE")}</th>
                    <th>{t("AVERAGE_COST")}</th>
                    <th>{t("INVENTORY")}</th>
                    <th>{t("ACTION")}</th>
                  </tr>
                </thead>
                <tbody>
                  {products.map((product, index) => (
                    <tr key={index}>
                      <th scope="row">
                        <NumberFormat value={product.code} type="barcode" />
                      </th>
                      <td>
                        <div className="avatar-group">
                          {product[`image${i18n.language}`] &&
                            product[`image${i18n.language}`].map(
                              ({ id, name, formats }, index) => {
                                const url = get(formats, "thumbnail.url");
                                if (url) {
                                  return (
                                    <>
                                      <a
                                        key={index}
                                        className="avatar avatar-sm rounded-circle"
                                        id={`tooltip${id}`}
                                        onClick={(e) => e.preventDefault()}
                                      >
                                        <img
                                          alt={name}
                                          src={`${API_URL}${url}`}
                                          className="thumbnail"
                                        />
                                      </a>
                                      <UncontrolledTooltip
                                        delay={0}
                                        target={`tooltip${id}`}
                                      >
                                        {name}
                                      </UncontrolledTooltip>
                                    </>
                                  );
                                } else {
                                  return null;
                                }
                              }
                            )}
                        </div>
                      </td>
                      <td>{product[`name${i18n.language}`]}</td>
                      <td>
                        {product.mBrandCategory &&
                          product.mBrandCategory[`name${i18n.language}`]}
                      </td>
                      <td>
                        <NumberFormat
                          value={product.retailPrice}
                          type="money"
                        />
                      </td>
                      <td>
                        <NumberFormat
                          value={get(product, "stock.averageCost", 0)}
                          type="money"
                        />
                      </td>
                      <td>
                        <NumberFormat
                          value={get(product, "stock.quantity", 0)}
                        />
                      </td>
                      <td className="table-actions">
                        <a
                          className="table-action p-1"
                          href="javascript:void(0)"
                          id="edit-product"
                          onClick={productDetail(product.id)}
                        >
                          <i className="fas fa-edit" />
                        </a>
                        <UncontrolledTooltip delay={0} target="edit-product">
                          {t("EDIT")}
                        </UncontrolledTooltip>
                        {/* <a
												className="table-action table-action-delete p-1"
												href="javascript:void(0)"
												id="delete-product"
												onClick={deleteProduct(product.id)}
												>
												<i className="fas fa-trash" />
												</a>
												<UncontrolledTooltip delay={0} target="delete-product">{t('DELETE')}</UncontrolledTooltip> */}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </SmartTable>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Product;
