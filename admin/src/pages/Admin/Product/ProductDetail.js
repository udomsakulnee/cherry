import {
  API_MASTER_BRAND,
  API_MASTER_BRAND_CATEGORY,
  API_MASTER_BRAND_SUB_CATEGORY,
  API_MASTER_LEVEL,
  API_MASTER_PRODUCT_LABEL,
  API_MASTER_PRODUCT_TYPE,
  API_MASTER_PRODUCT_UNIT,
  API_PRODUCT,
  API_PRODUCT_WHOLESALE_PRICE,
  API_STOCK,
} from "config";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Form,
  Row,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { createData, deleteData, findAll, findOne, updateData } from "api";
import { get, isEmpty, orderBy } from "lodash";
import {
  hideSpinner,
  notifyError,
  notifySuccess,
  showAlertWarning,
  showSpinner,
} from "redux/actions";
import { useFieldArray, useForm } from "react-hook-form";
import { FormInput } from "components/Forms";
import NumberFormat from "react-number-format";
import SimpleHeader from "components/Headers/SimpleHeader";
import { dayjs } from "utils";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";

const ProductDetail = () => {
  const { t, i18n } = useTranslation();
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const [showId, setShowId] = useState();
  const [productTypes, setProductTypes] = useState([]);
  const [brands, setBrands] = useState([]);
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [units, setUnits] = useState([]);
  const [labels, setLabels] = useState([]);
  const [levels, setLevels] = useState([]);
  const [createdAt, setCreatedAt] = useState();
  const [updatedAt, setUpdatedAt] = useState();
  const [removePrices, setRemovePrices] = useState([]);

  const {
    handleSubmit,
    control,
    reset,
    getValues,
    register,
    formState: { errors },
  } = useForm();
  const { fields, append, remove } = useFieldArray({
    control,
    name: "productWholesalePrices",
  });

  const onSubmit = async (data) => {
    dispatch(showSpinner());
    const { productWholesalePrices } = data;
    delete data.productWholesalePrices;

    let productWholesalePricesResponse = [];
    for (let i = 0; i < productWholesalePrices.length; i++) {
      const { id } = productWholesalePrices[i];
      delete data.id;
      if (id === "create") {
        const response = await createData(
          API_PRODUCT_WHOLESALE_PRICE,
          productWholesalePrices[i]
        );
        if (response) {
          productWholesalePricesResponse = [
            ...productWholesalePricesResponse,
            response,
          ];
        }
      } else {
        const response = await updateData(
          API_PRODUCT_WHOLESALE_PRICE,
          id,
          productWholesalePrices[i]
        );
        if (response) {
          productWholesalePricesResponse = [
            ...productWholesalePricesResponse,
            response,
          ];
        }
      }
    }

    if (
      productWholesalePricesResponse.length === productWholesalePrices.length
    ) {
      data = {
        ...data,
        productWholesalePrices: productWholesalePricesResponse.map(
          (item) => item.id
        ),
      };
      if (id === "create") {
        const response = await createData(API_PRODUCT, data, dispatch);
        if (response) {
          createData(API_STOCK, { product: response.id });
          const message = {
            text: t("CREATE_SUCCESS"),
          };
          dispatch(notifySuccess(message));
          history.push("/admin/products");
        } else {
          const message = {
            text: t("CREATE_FAIL"),
          };
          dispatch(notifyError(message));
        }
      } else {
        const response = await updateData(API_PRODUCT, id, data, dispatch);
        if (response) {
          const message = {
            text: t("UPDATE_SUCCESS"),
          };

          if (removePrices.length > 0) {
            for (let i = 0; i < removePrices.length; i++) {
              deleteData(API_PRODUCT_WHOLESALE_PRICE, removePrices[i]);
            }
            setRemovePrices([]);
          }

          dispatch(notifySuccess(message));
          history.push("/admin/products");
        } else {
          const message = {
            text: t("UPDATE_FAIL"),
          };
          dispatch(notifyError(message));
        }
      }
    } else {
      const message = {
        text: t("UPDATE_FAIL"),
      };
      dispatch(notifyError(message));
    }

    dispatch(hideSpinner());
  };

  const extractData = (data) => ({
    ...data,
    mProductType: get(data, "mProductType.id"),
    mBrand: get(data, "mBrand.id"),
    mBrandCategory: get(data, "mBrandCategory.id"),
    mBrandSubCategory: get(data, "mBrandSubCategory.id"),
    mProductUnit: get(data, "mProductUnit.id"),
    mProductLabels: get(data, "mProductLabels", []).map((item) => item.id),
    productWholesalePrices: orderBy(get(data, "productWholesalePrices", []), [
      "quantity",
    ]),
  });

  const getProductDetail = async (id) => {
    dispatch(showSpinner());
    let response = await findOne(API_PRODUCT, id);
    const data = extractData(response);

    await getMasterData(data);
    setCreatedAt(data.created_at);
    setUpdatedAt(data.updated_at);

    const { created_at, updated_at, published_at, stock, ...dataForEdit } =
      data;
    reset(dataForEdit);
    dispatch(hideSpinner());
  };

  const getMasterData = ({ mBrandCategory, mBrandSubCategory } = {}) => {
    return new Promise((resolve) => {
      let wait = 5;

      findAll(API_MASTER_PRODUCT_TYPE).then((data) => {
        setProductTypes(data);
        wait--;
        !wait && resolve();
      });

      findAll(API_MASTER_BRAND).then((data) => {
        setBrands(data);
        wait--;
        !wait && resolve();
      });

      if (mBrandCategory) {
        wait++;
        findAll(API_MASTER_BRAND_CATEGORY).then((data) => {
          setCategories(data);
          wait--;
          !wait && resolve();
        });
      }

      if (mBrandSubCategory) {
        wait++;
        findAll(API_MASTER_BRAND_SUB_CATEGORY).then((data) => {
          setSubCategories(data);
          wait--;
          !wait && resolve();
        });
      }

      findAll(API_MASTER_PRODUCT_UNIT).then((data) => {
        setUnits(data);
        wait--;
        !wait && resolve();
      });

      findAll(API_MASTER_PRODUCT_LABEL).then((data) => {
        setLabels(data);
        wait--;
        !wait && resolve();
      });

      findAll(API_MASTER_LEVEL, { _sort: "level" }).then((data) => {
        setLevels(data);
        wait--;
        !wait && resolve();
      });
    });
  };

  useEffect(() => {
    if (showId !== id) {
      setShowId(id);
      if (id === "create") {
        getMasterData();
      } else {
        getProductDetail(id);
      }
    }

    if (!isEmpty(errors)) {
      dispatch(showAlertWarning({ text: t("WARNING__DATA_INCOMPLETE") }));
    }
  }, [id, errors]);

  const handleChangeBrand = (mBrand) => {
    mBrand &&
      findAll(API_MASTER_BRAND_CATEGORY, { mBrand }).then((data) => {
        setCategories(data);
      });
  };

  const handleChangeCategory = (mBrandCategory) => {
    mBrandCategory &&
      findAll(API_MASTER_BRAND_SUB_CATEGORY, { mBrandCategory }).then(
        (data) => {
          setSubCategories(data);
        }
      );
  };

  const handleRemovePrice = (index, id) => () => {
    id !== "create" && setRemovePrices([...removePrices, id]);
    remove(index);
  };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <SimpleHeader
        name={id === "create" ? t("PRODUCT__NEW") : t("PRODUCT__EDIT")}
        parentName={t("PRODUCT")}
      >
        <Button className="btn-icon btn-3" color="secondary" type="submit">
          <span className="btn-inner--icon">
            <i className="fas fa-save"></i>
          </span>
          <span className="btn-inner--text">{t("SAVE")}</span>
        </Button>
      </SimpleHeader>
      <Container className="mt--6" fluid>
        <Row>
          <Col lg="8">
            <div className="card-wrapper">
              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("PRODUCT")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("PRODUCT__NAME")} (${t("EN")}) `}
                        name="nameEN"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("PRODUCT__NAME")} (${t("TH")}) `}
                        name="nameTH"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("PRODUCT__CODE")}
                        name="code"
                        type="barcode"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={3}>
                      <FormInput
                        label={`${t("WEIGHT")} (${t("G")})`}
                        name="weight"
                        type="number"
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("REGISTRATION_NUMBER")}
                        name="registrationNumber"
                        type="fda"
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={3}>
                      <FormInput
                        label={t("POINT")}
                        name="point"
                        type="number"
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={3}>
                      <FormInput
                        label={t("QUANTITY_WARNING")}
                        name="quantityWarning"
                        type="number"
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={3}>
                      <FormInput
                        label={`${t("ON_OFF")} ${t("QUANTITY_WARNING")}`}
                        name="quantityWarningActive"
                        type="switch"
                        control={control}
                      />
                    </Col>
                  </Row>
                </CardBody>
              </Card>

              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("PRODUCT__PRICE")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs={12} sm={4}>
                      <FormInput
                        label={`${t("RETAIL_PRICE")} (${t("BAHT")})`}
                        name="retailPrice"
                        type="money"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>

                  {fields.map(({ id, mLevel, quantity, price }, index) => {
                    const cost = quantity * price || 0;
                    const profit =
                      getValues("retailPrice") * quantity - cost || 0;
                    return (
                      <div key={`prices-${id}`}>
                        <hr className="mt-3" />
                        <input
                          type="hidden"
                          name={`productWholesalePrices[${index}].id`}
                          ref={register()}
                          defaultValue={id}
                        />
                        <Row>
                          <Col xs={12} sm={3}>
                            <FormInput
                              label={t("LEVEL")}
                              name={`productWholesalePrices[${index}].mLevel`}
                              type="select"
                              control={control}
                              rules={{ required: true }}
                              defaultValue={mLevel}
                              option={levels.map((item) => ({
                                id: item.id,
                                text: item[`name${i18n.language}`],
                              }))}
                            />
                          </Col>
                          <Col xs={12} sm={9} className="text-right">
                            <Button
                              className="btn-icon btn-2"
                              color="danger"
                              type="button"
                              size="sm"
                              onClick={handleRemovePrice(index, id)}
                            >
                              <span className="btn-inner--icon">
                                <i className="fas fa-trash"></i>
                              </span>
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col xs={12} sm={3}>
                            <FormInput
                              label={`${t("QUANTITY")} (${t("PIECE")})`}
                              name={`productWholesalePrices[${index}].quantity`}
                              type="number"
                              control={control}
                              defaultValue={quantity}
                              rules={{ required: true }}
                            />
                          </Col>
                          <Col xs={12} sm={3}>
                            <FormInput
                              label={`${t("WHOLESALE_PRICE")} (${t("BAHT")})`}
                              name={`productWholesalePrices[${index}].price`}
                              type="money"
                              control={control}
                              defaultValue={price}
                              rules={{ required: true }}
                            />
                          </Col>
                          <Col xs={12} sm={3}>
                            <label
                              className="form-control-label"
                              htmlFor="cost"
                            >
                              {t("COST")}
                            </label>
                            <NumberFormat
                              className="form-control form-control-flush"
                              value={cost}
                              thousandSeparator={true}
                              prefix={"฿ "}
                            />
                          </Col>
                          <Col xs={12} sm={3}>
                            <label
                              className="form-control-label"
                              htmlFor="profit"
                            >
                              {t("PROFIT")}
                            </label>
                            <NumberFormat
                              className="form-control form-control-flush"
                              value={profit}
                              thousandSeparator={true}
                              prefix={"฿ "}
                            />
                          </Col>
                        </Row>
                      </div>
                    );
                  })}
                  <Button
                    className="btn-icon btn-3"
                    color="primary"
                    type="button"
                    onClick={() =>
                      append({
                        id: "create",
                      })
                    }
                  >
                    <span className="btn-inner--icon">
                      <i className="fas fa-plus-circle"></i>
                    </span>
                    <span className="btn-inner--text">
                      {t("ADD_WHOLESALE_PRICE")}
                    </span>
                  </Button>
                </CardBody>
              </Card>

              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("PRODUCT__DETAIL")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs={12}>
                      <FormInput
                        label={`${t("PRODUCT__DETAIL")} (${t("EN")}) `}
                        name="detailEN"
                        type="editor"
                        control={control}
                      />
                    </Col>
                    <Col xs={12}>
                      <FormInput
                        label={`${t("PRODUCT__DETAIL")} (${t("TH")}) `}
                        name="detailTH"
                        type="editor"
                        control={control}
                      />
                    </Col>
                  </Row>
                </CardBody>
              </Card>

              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("IMAGE")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("IMAGE")} (${t("EN")}) `}
                        name="imageEN"
                        type="file"
                        maxFiles={10}
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("IMAGE")} (${t("TH")}) `}
                        name="imageTH"
                        type="file"
                        maxFiles={10}
                        control={control}
                      />
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </div>
          </Col>

          <Col lg="4">
            <div className="card-wrapper">
              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("INFORMATION")}</h3>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col sm="4">{t("CREATE_DATE")}</Col>
                    <Col sm="8">
                      <p>
                        {createdAt
                          ? dayjs(createdAt).format("D MMMM YYYY H:mm")
                          : "-"}
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="4">{t("LAST_UPDATE")}</Col>
                    <Col sm="8">
                      <p>
                        {updatedAt
                          ? dayjs(updatedAt).fromNow()
                          : t("MOMENT__FROM_NOW")}
                      </p>
                    </Col>
                  </Row>
                </CardBody>
              </Card>

              <Card>
                <CardHeader>
                  <h3 className="mb-0">{t("CHOOSE_ITEM")}</h3>
                </CardHeader>
                <CardBody>
                  <FormInput
                    label={t("PRODUCT__TYPE")}
                    name="mProductType"
                    type="select"
                    control={control}
                    rules={{ required: true }}
                    option={productTypes.map((item) => ({
                      id: item.id,
                      text: item[`name${i18n.language}`],
                    }))}
                  />
                  <FormInput
                    label={t("BRAND")}
                    name="mBrand"
                    type="select"
                    control={control}
                    rules={{ required: true }}
                    option={brands.map((item) => ({
                      id: item.id,
                      text: item[`name${i18n.language}`],
                    }))}
                    onChange={handleChangeBrand}
                  />
                  <FormInput
                    label={t("CATEGORY")}
                    name="mBrandCategory"
                    type="select"
                    control={control}
                    rules={{ required: true }}
                    disabled={!getValues("mBrand")}
                    option={categories.map((item) => ({
                      id: item.id,
                      text: item[`name${i18n.language}`],
                    }))}
                    onChange={handleChangeCategory}
                  />
                  <FormInput
                    label={t("SUB_CATEGORY")}
                    name="mBrandSubCategory"
                    type="select"
                    control={control}
                    disabled={!getValues("mBrandCategory")}
                    option={subCategories.map((item) => ({
                      id: item.id,
                      text: item[`name${i18n.language}`],
                    }))}
                  />
                  <FormInput
                    label={t("PRODUCT__UNIT")}
                    name="mProductUnit"
                    type="select"
                    control={control}
                    rules={{ required: true }}
                    option={units.map((item) => ({
                      id: item.id,
                      text: item[`name${i18n.language}`],
                    }))}
                  />
                  <FormInput
                    label={t("LABEL")}
                    name="mProductLabels"
                    type="select"
                    control={control}
                    multiple
                    option={labels.map((item) => ({
                      id: item.id,
                      text: item[`name${i18n.language}`],
                    }))}
                  />
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </Container>
    </Form>
  );
};

export default ProductDetail;
