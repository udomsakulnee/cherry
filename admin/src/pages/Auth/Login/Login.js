import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import {
  browserName,
  deviceType,
  engineName,
  engineVersion,
  fullBrowserVersion,
  mobileModel,
  mobileVendor,
  osName,
  osVersion,
} from "react-device-detect";
import { clearSession, getStorage, setStorage } from "utils";
import {
  clearUser,
  hideSpinner,
  setUser,
  showAlertWarning,
  showSpinner,
} from "redux/actions";
import { createData, loginAPI } from "api";
import { API_LOG_LOGIN } from "config";
import { APP_VERSION } from "config";
import AuthHeader from "components/Headers/AuthHeader";
import { KEY_REMEMBER } from "config";
import classnames from "classnames";
import { isEmpty } from "lodash";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import InputPassword from "components/InputPassword/InputPassword";

const Login = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const dispatch = useDispatch();
  const [focusedUsername, setFocusedUsername] = useState(false);
  const [focusedPassword, setFocusedPassword] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [rememberMe, setRememberMe] = useState(false);

  useEffect(() => {
    clearSession();
    dispatch(clearUser());

    const remember = getStorage(KEY_REMEMBER);
    if (remember && remember.username && remember.password) {
      setUsername(remember.username);
      setPassword(remember.password);
      setRememberMe(true);
    }
  }, []);

  const handleLogin = async () => {
    try {
      dispatch(showSpinner());
      const user = await loginAPI(username.toLowerCase(), password);
      if (!isEmpty(user)) {
        rememberMe
          ? setStorage(KEY_REMEMBER, {
              username: username.toLowerCase(),
              password,
            })
          : setStorage(KEY_REMEMBER, { username: "", password: "" });
        dispatch(setUser(user));

        const logData = {
          user: user.id,
          osName,
          osVersion,
          browserName,
          fullBrowserVersion,
          engineName,
          engineVersion,
          mobileVendor,
          mobileModel,
          deviceType,
          appVersion: APP_VERSION,
        };
        await createData(API_LOG_LOGIN, logData);
        dispatch(hideSpinner());
        history.push("/admin");
      }
    } catch (err) {
      if (err) {
        const message = `LOGIN__${err
          .replace(".", "")
          .replaceAll(" ", "_")
          .toUpperCase()}`;
        dispatch(showAlertWarning({ text: t(message) }));
      }
      dispatch(hideSpinner());
    }
  };

  return (
    <>
      <AuthHeader title={`${t("WELCOME")}!`} lead={t("LOGIN__CAPTION")} />
      <Container className="mt--8 pb-5">
        <Row className="justify-content-center">
          <Col lg="5" md="7">
            <Card className="bg-secondary border-0 mb-0">
              {/* <CardHeader className="bg-transparent pb-5">
								<div className="text-muted text-center mt-2 mb-3">
								<small>{t('SIGNIN_WITH')}</small>
								</div>
								<div className="btn-wrapper text-center">
								<Button
									className="btn-neutral btn-icon"
									color="default"
									onClick={e => e.preventDefault()}
								>
									<span className="btn-inner--icon mr-1">
									<img
										alt="..."
										src={require('assets/img/icons/common/facebook.svg')}
									/>
									</span>
									<span className="btn-inner--text">{t('FACEBOOK')}</span>
								</Button>
								<Button
									className="btn-neutral btn-icon"
									color="default"
									onClick={e => e.preventDefault()}
								>
									<span className="btn-inner--icon mr-1">
									<img
										alt="..."
										src={require('assets/img/icons/common/google.svg')}
									/>
									</span>
									<span className="btn-inner--text">{t('GOOGLE')}</span>
								</Button>
								</div>
							</CardHeader> */}
              <CardBody className="px-lg-5 py-lg-5">
                <div className="text-center mb-4">
                  <img
                    alt="logo"
                    src={require("assets/img/brand/logo.svg").default}
                    className="brand-logo"
                  />
                </div>
                <div className="text-center text-muted mb-4">
                  {/* <small>{t('LOGIN__OR_WITH_ACCOUNT')}</small> */}
                  <small>{t("SIGNIN_WITH")}</small>
                </div>
                <Form role="form">
                  <FormGroup
                    className={classnames("mb-3", {
                      focused: focusedUsername,
                    })}
                  >
                    <InputGroup className="input-group-merge input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-email-83" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder={t("LOGIN__EMAIL_OR_USERNAME")}
                        type="text"
                        onFocus={() => setFocusedUsername(true)}
                        onBlur={() => setFocusedUsername(false)}
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup
                    className={classnames({
                      focused: focusedPassword,
                    })}
                  >
                    <InputGroup className="input-group-merge input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-lock-circle-open" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <InputPassword
                        className="pr-5"
                        placeholder={t("PASSWORD")}
                        onFocus={() => setFocusedPassword(true)}
                        onBlur={() => setFocusedPassword(false)}
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </InputGroup>
                  </FormGroup>
                  <div
                    className="custom-control custom-control-alternative custom-checkbox"
                    onClick={() => setRememberMe(!rememberMe)}
                  >
                    <input
                      className="custom-control-input"
                      id="customCheckLogin"
                      type="checkbox"
                      checked={rememberMe && "checked"}
                    />
                    <label
                      className="custom-control-label"
                      htmlFor=" customCheckLogin"
                    >
                      <span className="text-muted">{t("REMEMBER_ME")}</span>
                    </label>
                  </div>
                  <div className="text-center">
                    <Button
                      className="my-4"
                      color="info"
                      type="button"
                      onClick={handleLogin}
                    >
                      {t("SIGNIN")}
                    </Button>
                  </div>
                </Form>
              </CardBody>
            </Card>
            <Row className="mt-3">
              {/* <Col className="text-right" xs="12">
								<a
								className="text-light"
								onClick={e => e.preventDefault()}
								>
								<small>{t('FORGOT_PASSWORD')} ?</small>
								</a>
							</Col> */}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Login;
