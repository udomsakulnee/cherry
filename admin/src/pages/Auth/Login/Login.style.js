import styled from "styled-components";

export const IconEye = styled.i`
  position: absolute;
  right: 16px;
  font-size: 20px;
  color: #adb5bd;
  line-height: 46px;
  z-index: 3;
  cursor: pointer;
  &:hover {
    color: #999;
  }
`;
