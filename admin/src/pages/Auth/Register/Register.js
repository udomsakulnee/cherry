import { API_ADDRESS, API_MASTER_SEX, API_ORDER_SUMMARY, API_MASTER_LEVEL } from "config";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import { Controller, useForm } from "react-hook-form";
import React, { useEffect, useState } from "react";
import { createData, findAll, getUserAPI, registerAPI } from "api";
import { get, isEmpty } from "lodash";
import {
  hideSpinner,
  showAlertSuccess,
  showAlertWarning,
  showSpinner,
} from "redux/actions";
import AuthHeader from "components/Headers/AuthHeader";
import { FormInput } from "components/Forms";
import { clearSession, checkPasswordStrength, passwordPattern } from "utils";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import InputPassword from "components/InputPassword/InputPassword";
import PasswordPattern from "components/ChangePasswordCard/PasswordPattern";

const Register = () => {
  const { t, i18n } = useTranslation();
  const { id } = useParams();
  const history = useHistory();
  const {
    handleSubmit,
    control,
    formState: { errors },
    setValue,
    reset,
  } = useForm();
  const dispatch = useDispatch();
  const [parent, setParent] = useState({
    id: null,
    nameEN: "-",
    nameTH: "-",
  });
  const [sexes, setSexes] = useState([]);
  const [passwordStrength, setPasswordStrength] = useState("NOT_FOUND");
  const [level, setLevel] = useState({});

  const getMasterData = () => {
    findAll(API_MASTER_SEX).then((data) => {
      setSexes(data);
    });

    findAll(API_MASTER_LEVEL, {level: 1}).then((data) => {
      if (data.length > 0) {
        setLevel(data[0]);
      }
    });
  };

  const onSubmit = async (data) => {
    const { password, confirmPassword } = data;
    if (password === confirmPassword) {
      dispatch(showSpinner());
      data = {
        ...data,
        username: data.username.toLowerCase(),
        email: data.email.toLowerCase(),
        mLevel: get(level, "id", 1),
      };
      const parentId = get(parent, "id");
      if (parentId) {
        data = { ...data, parent: parentId };
      }
      try {
        const {
          avatar,
          consent,
          email,
          firstnameEN,
          firstnameTH,
          idcard,
          idcardImage,
          lastnameEN,
          lastnameTH,
          mLevel,
          mSex,
          parent,
          password,
          phoneNumber,
          slip,
          username,
          houseNo,
          building,
          village,
          floor,
          room,
          villageNo,
          lane,
          road,
          subDistrict,
          district,
          province,
          zipCode,
        } = data;
        const userInfo = {
          avatar,
          consent,
          email,
          firstnameEN,
          firstnameTH,
          idcard,
          idcardImage,
          lastnameEN,
          lastnameTH,
          mLevel,
          mSex,
          parent,
          password,
          phoneNumber,
          slip,
          username,
        };
        const { user } = await registerAPI(userInfo);
        if (user) {
          const address = {
            firstnameTH,
            lastnameTH,
            phoneNumber,
            houseNo,
            building,
            village,
            floor,
            room,
            villageNo,
            lane,
            road,
            subDistrict,
            district,
            province,
            zipCode: String(zipCode),
          };
          const response = await createData(API_ADDRESS, {
            ...address,
            user: user.id,
            isDefault: true,
          });
          const orderSummary = {
            user: user.id,
            totalQuantity: 0,
            totalPrice: 0,
            totalPoint: 0,
          };
          const response2 = await createData(API_ORDER_SUMMARY, orderSummary);
          if (response && response2) {
            dispatch(showAlertSuccess({ text: t("REGISTER__SUCCESS") }));
            reset();
            history.push("/auth");
          }
        }
        dispatch(hideSpinner());
      } catch (err) {
        if (err) {
          const message = `REGISTER__${err
            .replace(".", "")
            .replaceAll(" ", "_")
            .toUpperCase()}`;
          dispatch(showAlertWarning({ text: t(message) }));
        }
        dispatch(hideSpinner());
      }
    } else {
      dispatch(showAlertWarning({ text: t("REGISTER__PASSWORD_NOT_MATCH") }));
    }
  };

  const handleChangeAddress = (address) => {
    setValue("subDistrict", address.subDistrict);
    setValue("district", address.district);
    setValue("province", address.province);
    setValue("zipCode", address.zipCode);
  };

  const getParent = async (id) => {
    try {
      const parent = await getUserAPI(id);
      setParent(parent);
    } catch (err) {
      history.push("/auth");
    }
  };

  useEffect(() => {
    clearSession();
    getMasterData();
    if (id) {
      getParent(id);
    } else {
      history.push("/auth");
    }
  }, []);

  useEffect(() => {
    if (!isEmpty(errors)) {
      if (get(errors, "password.type") === "pattern") {
        dispatch(
          showAlertWarning({
            text: t("REGISTER__PASSWORD_PATTERN_IS_INCORRECT"),
          })
        );
      } else {
        dispatch(showAlertWarning({ text: t("WARNING__DATA_INCOMPLETE") }));
      }
    }
  }, [errors]);

  return (
    <>
      <AuthHeader title={t("REGISTER")} lead={t("LOGIN__CAPTION")} />
      <Container className="mt--8 pb-5">
        <Row className="justify-content-center">
          <Col lg="6" md="8">
            <Card className="bg-secondary border-0">
              <CardBody className="px-lg-5 py-lg-5">
                <div className="text-center mb-4">
                  <img
                    alt="dermatid"
                    src={require("assets/img/brand/logo.svg").default}
                    className="brand-logo"
                  />
                </div>
                <div className="text-center text-muted mb-4">
                  <small>{t("SIGNUP_WITH")}</small>
                </div>
                <Form onSubmit={handleSubmit(onSubmit)}>
                  <FormGroup>
                    <InputGroup
                      className={`input-group-merge input-group-alternative mb-3 border ${
                        !isEmpty(errors.email) && "border-danger"
                      }`}
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-email-83" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Controller
                        name="email"
                        control={control}
                        rules={{ required: true }}
                        render={({field}) => (
                          <Input {...field} placeholder={t("EMAIL")} type="email" />
                        )}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup
                      className={`input-group-merge input-group-alternative mb-3 border ${
                        !isEmpty(errors.username) && "border-danger"
                      }`}
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-single-02" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Controller
                        name="username"
                        control={control}
                        rules={{
                          required: true,
                          minLength: 5,
                        }}
                        render={({field})  => (
                          <Input
                            {...field}
                            placeholder={`${t("USERNAME")} (${t(
                              "MIN_LENGTH_CHARACTERS",
                              { length: 5 }
                            )})`}
                            type="text"
                          />
                        )}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup
                      className={`input-group-merge input-group-alternative mb-3 border ${
                        !isEmpty(errors.password) && "border-danger"
                      }`}
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-lock-circle-open" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Controller
                        name="password"
                        control={control}
                        rules={{
                          required: true,
                          pattern: passwordPattern,
                        }}
                        render={({field}) => (
                          <InputPassword {...field} placeholder={t("PASSWORD")} onInput={checkPasswordStrength(setPasswordStrength)} />
                        )}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup
                      className={`input-group-merge input-group-alternative mb-3 border ${
                        !isEmpty(errors.confirmPassword) && "border-danger"
                      }`}
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-lock-circle-open" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Controller
                        name="confirmPassword"
                        control={control}
                        rules={{ required: true }}
                        render={({field}) => (
                          <InputPassword {...field} placeholder={t("CONFIRM_PASSWORD")} />
                        )}
                      />
                    </InputGroup>
                  </FormGroup>
                  <PasswordPattern passwordStrength={passwordStrength} />
                  <hr />
                  <div className="text-center text-muted mb-4">
                    <small>{t("PROFILE")}</small>
                  </div>
                  <Row>
                    <Col xs={12} sm={12}>
                      <p className="text-center">
                        {t("PARENT")}:{" "}
                        {get(parent, `name${i18n.language}`, "-")}
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={12}>
                      <FormInput
                        label={t("PROFILE_PICTURE")}
                        name="avatar"
                        type="file"
                        maxFiles={1}
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("FIRSTNAME")} (${t("EN")})`}
                        name="firstnameEN"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("LASTNAME")} (${t("EN")})`}
                        name="lastnameEN"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("FIRSTNAME")} (${t("TH")})`}
                        name="firstnameTH"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={`${t("LASTNAME")} (${t("TH")})`}
                        name="lastnameTH"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("SEX")}
                        name="mSex"
                        type="select"
                        control={control}
                        rules={{ required: true }}
                        option={sexes.map((item) => ({
                          id: item.id,
                          text: item[`name${i18n.language}`],
                        }))}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("PHONE_NUMBER")}
                        name="phoneNumber"
                        type="tel"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("IDCARD")}
                        name="idcard"
                        type="idcard"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={12}>
                      <FormInput
                        label={t("IDCARD_IMAGE")}
                        name="idcardImage"
                        type="file"
                        maxFiles={1}
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={12}>
                      <FormInput
                        label={t("SLIP")}
                        name="slip"
                        type="file"
                        maxFiles={1}
                        control={control}
                      />
                    </Col>
                  </Row>
                  <hr />
                  <div className="text-center text-muted mb-4">
                    <small>{t("ADDRESS")}</small>
                  </div>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("HOUSE_NO")}
                        name="houseNo"
                        control={control}
                        rules={{ required: true }}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("VILLAGE")}
                        name="village"
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("BUILDING")}
                        name="building"
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={4}>
                      <FormInput
                        label={t("FLOOR")}
                        name="floor"
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={4}>
                      <FormInput
                        label={t("ROOM")}
                        name="room"
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={4}>
                      <FormInput
                        label={t("VILLAGE_NO")}
                        name="villageNo"
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("LANE")}
                        name="lane"
                        control={control}
                      />
                    </Col>
                    <Col xs={12} sm={6}>
                      <FormInput
                        label={t("ROAD")}
                        name="road"
                        control={control}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={12}>
                      <FormInput
                        label={t("SUB_DISTRICT")}
                        name="subDistrict"
                        type="address"
                        control={control}
                        rules={{ required: true }}
                        onChangeAddress={handleChangeAddress}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={12}>
                      <FormInput
                        label={t("DISTRICT")}
                        name="district"
                        type="address"
                        control={control}
                        rules={{ required: true }}
                        onChangeAddress={handleChangeAddress}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={12}>
                      <FormInput
                        label={t("PROVINCE")}
                        name="province"
                        type="address"
                        control={control}
                        rules={{ required: true }}
                        onChangeAddress={handleChangeAddress}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={12}>
                      <FormInput
                        label={t("ZIP_CODE")}
                        name="zipCode"
                        type="address"
                        control={control}
                        rules={{ required: true }}
                        onChangeAddress={handleChangeAddress}
                      />
                    </Col>
                  </Row>
                  <Row className="my-4">
                    <Col xs="12">
                      <Controller
                        control={control}
                        rules={{ required: true }}
                        render={({field: {value, onChange}}) => (
                          <div className="custom-control custom-control-alternative custom-checkbox">
                            <input
                              value={value}
                              onChange={onChange}
                              className="custom-control-input"
                              id="customCheckRegister"
                              type="checkbox"
                              name="consent"
                            />
                            <label
                              className={`custom-control-label border ${
                                !isEmpty(errors.consent) && "border-danger"
                              }`}
                              htmlFor="customCheckRegister"
                            >
                              <span className="text-muted">
                                {t("AGREE_WITH")}{" "}
                                <a href="/privacy-policy" target="_blank">
                                  {t("PRIVACY_POLICY")}
                                </a>
                              </span>
                            </label>
                          </div>
                        )}
                      />
                    </Col>
                  </Row>
                  <div className="text-center">
                    <Button className="mt-4" color="info" type="submit">
                      {t("APPLY")}
                    </Button>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Register;
