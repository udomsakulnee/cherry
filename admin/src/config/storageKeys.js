export const KEY_TOKEN = "token";
export const KEY_USER = "user";
export const KEY_LOCALE = "locale";
export const KEY_LIMIT = "limit";
export const KEY_AUTH = "auth";
export const KEY_REMEMBER = "remember";
