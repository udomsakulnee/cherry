export const ENV = process.env.REACT_APP_ENV || "development";
export const API_URL = process.env.REACT_APP_API_URL || "http://localhost:1337";
export const APP_VERSION = process.env.REACT_APP_VERSION || "";
