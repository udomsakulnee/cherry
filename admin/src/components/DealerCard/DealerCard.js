import React, { useEffect, useRef } from "react";
import { API_URL } from "config";
import { Button } from "reactstrap";
import MemberCard2 from "assets/img/membercard/member-card-2.png";
import MemberCard3 from "assets/img/membercard/member-card-3.png";
import MemberCard4 from "assets/img/membercard/member-card-4.png";
import MemberCard5 from "assets/img/membercard/member-card-5.png";
import MemberCard6 from "assets/img/membercard/member-card-6.png";
import MemberCard7 from "assets/img/membercard/member-card-7.png";
import MemberCard8 from "assets/img/membercard/member-card-8.png";
import MemberCard9 from "assets/img/membercard/member-card-9.png";
import MemberCard10 from "assets/img/membercard/member-card-10.png";
import MemberCard11 from "assets/img/membercard/member-card-11.png";
import { get } from "lodash";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";

const DealerCard = () => {
  const memberCard = [
    null,
    MemberCard2,
    MemberCard3,
    MemberCard4,
    MemberCard5,
    MemberCard6,
    MemberCard7,
    MemberCard8,
    MemberCard9,
    MemberCard10,
    MemberCard11,
    null,
  ];
  const { t } = useTranslation();
  const user = useSelector((state) => state.user);
  const { avatar, mSex } = user;
  let userAvatar = get(avatar, "url");
  if (userAvatar) {
    userAvatar = `${API_URL}${userAvatar}`;
  } else {
    const sex = get(mSex, "nameEN", "Male").toLowerCase();
    userAvatar = require(`assets/img/icons/common/${sex}.svg`);
  }

  const area = get(user, "addresses[0].province", "");
  const level = get(user, "mLevel.level");
  let positionX1 = 89;
  let positionY1 = 375;
  let positionX2 = 530;
  let positionY2 = 443;
  let positionX3 = 512;
  let positionY3 = 503;
  let fontSize3 = 20;
  let widthImg = 310;
  let heightImg = 360;
  let positionXImg = 411;
  let positionYImg = 28;

  const dealerCard = memberCard[level - 1];
  let textColor = "#c3962a";

  if (level < 10) {
    positionX1 = 425;
    positionX2 = 148;
    positionY1 = 241;
    positionY2 = 577;
    positionX3 = 130;
    positionY3 = 643;
    fontSize3 = 25;
    widthImg = 312;
    heightImg = 379;
    positionXImg = 36;
    positionYImg = 141;

    if (level === 2) {
      positionY3 = 640;
    }
    if (level < 4) {
      textColor = "#666666";
    } else if (level > 6) {
      textColor = "#6978b7";
    }
  }

  const canvasRef = useRef(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const draw = (ctx) => {
    const img = new Image();
    const img2 = new Image();
    img.src = dealerCard;
    img2.crossOrigin = "Anonymous";
    img2.src = userAvatar;
    let count = 2;
    const drawImage = () => {
      if (count === 0) {
        ctx.drawImage(img, 0, 0, 752, 752);
        ctx.font = "25px Open Sans, sans-serif";
        ctx.fillStyle = textColor;
        ctx.fillText(user.facebook, positionX1, positionY1);
        ctx.fillText(user.line, positionX1, positionY1 + 63);
        ctx.fillText(user.instagram, positionX1, positionY1 + 126);
        ctx.fillText(
          `${user.phoneNumber.slice(0, 3)} ${user.phoneNumber.slice(
            3,
            6
          )} ${user.phoneNumber.slice(6, 10)}`,
          positionX1,
          positionY1 + 189
        );
        ctx.fillText(area, positionX1, positionY1 + 252);
        ctx.font = "30px Open Sans, sans-serif";
        ctx.fillStyle = "#ffffff";
        ctx.fillText(
          `DM${String(user.id).padStart(4, "0")}`,
          positionX2,
          positionY2
        );
        ctx.font = `${fontSize3}px Open Sans, sans-serif`;
        ctx.fillText(
          `${user.firstnameEN} ${user.lastnameEN}`,
          positionX3,
          positionY3
        );

        const imgWidth = img2.width;
        const imgHeight = img2.height;
        const imgAspect = imgHeight / imgWidth;
        let canvasWidth = widthImg;
        let canvasHeight = heightImg;
        const canvasAspect = canvasHeight / canvasWidth;

        let x1 = 0; // top left X position
        let y1 = 0; // top left Y position
        let x2 = 0; // bottom right X position
        let y2 = 0; // bottom right Y position

        if (imgWidth < canvasWidth && imgHeight < canvasHeight) {
          x1 = (canvasWidth - imgWidth) / 2;
          y1 = (canvasHeight - imgHeight) / 2;
          x2 = imgWidth + x1;
          y2 = imgHeight + y1;
        } else {
          if (canvasAspect > imgAspect) {
            y1 = canvasHeight;
            canvasHeight = canvasWidth * imgAspect;
            y1 = (y1 - canvasHeight) / 2;
          } else {
            x1 = canvasWidth;
            canvasWidth = canvasHeight / imgAspect;
            x1 = (x1 - canvasWidth) / 2;
          }
          x2 = positionXImg + x1;
          y2 = positionYImg + y1;
        }

        ctx.imageSmoothingEnabled = true;
        ctx.drawImage(
          img2,
          x1,
          y1,
          imgWidth,
          imgHeight,
          x2,
          y2,
          canvasWidth,
          canvasHeight
        );
      }
    };
    img.onload = () => {
      count--;
      drawImage();
    };
    img2.onload = () => {
      count--;
      drawImage();
    };
  };

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");
    draw(context);
  }, [draw]);

  const downloadCard = () => {
    const canvas = canvasRef.current;
    const url = canvas.toDataURL("image/png");
    const link = document.createElement("a");
    link.download = `DM${String(user.id).padStart(4, "0")} ${
      user.firstnameEN
    } ${user.lastnameEN}.png`;
    link.href = url;
    link.click();
  };

  return (
    <div className="text-center">
      <svg
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        width="100%"
        height="100%"
        viewBox="0 0 752 752"
      >
        <image href={dealerCard} width="752" height="752" />
        <g>
          <text x={positionX1} y={positionY1} fontSize="25" fill={textColor}>
            {user.facebook}
          </text>
          <text
            x={positionX1}
            y={positionY1 + 63}
            fontSize="25"
            fill={textColor}
          >
            {user.line}
          </text>
          <text
            x={positionX1}
            y={positionY1 + 126}
            fontSize="25"
            fill={textColor}
          >
            {user.instagram}
          </text>
          <text
            x={positionX1}
            y={positionY1 + 189}
            fontSize="25"
            fill={textColor}
          >
            {user.phoneNumber.slice(0, 3)} {user.phoneNumber.slice(3, 6)}{" "}
            {user.phoneNumber.slice(6, 10)}
          </text>
          <text
            x={positionX1}
            y={positionY1 + 252}
            fontSize="25"
            fill={textColor}
          >
            {area}
          </text>
          <text x={positionX2} y={positionY2} fontSize="30" fill="#ffffff">
            DM{String(user.id).padStart(4, "0")}
          </text>
          <text
            x={positionX3}
            y={positionY3}
            fontSize={fontSize3}
            fill="#ffffff"
          >
            {user.firstnameEN} {user.lastnameEN}
          </text>
          <image
            href={userAvatar}
            height={heightImg}
            width={widthImg}
            x={positionXImg}
            y={positionYImg}
          />
        </g>
      </svg>
      <canvas
        ref={canvasRef}
        width="752"
        height="752"
        style={{ display: "none" }}
      />

      <Button
        className="btn-icon btn-3 mt-3"
        color="success"
        type="button"
        onClick={downloadCard}
      >
        <span className="btn-inner--icon">
          <i className="ni ni-cloud-download-95" />
        </span>
        <span className="btn-inner--text">{t("DOWNLOAD")}</span>
      </Button>
    </div>
  );
};

DealerCard.defaultProps = {};

DealerCard.propTypes = {};

export default DealerCard;
