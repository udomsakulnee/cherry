/*!

=========================================================
* Argon Dashboard PRO React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";

// reactstrap components
import { NavItem, NavLink, Nav, Container, Row, Col } from "reactstrap";
import { APP_VERSION } from "config";
import { useTranslation } from "react-i18next";

function AuthFooter() {
  const { t } = useTranslation();

  return (
    <>
      <footer className="py-5" id="footer-main">
        <Container>
          <Row className="align-items-center justify-content-xl-between">
            <Col xl="6">
              <div className="copyright text-center text-xl-left text-muted">
                © {new Date().getFullYear()}{" "}
                <a
                  className="font-weight-bold ml-1"
                  href="https://www.facebook.com/DERMAtidThailand.CEO"
                  target="_blank"
                  rel="noreferrer"
                >
                  Dermatid Thailand
                </a>
              </div>
            </Col>
            <Col lg="6">
              <Nav className="nav-footer justify-content-center justify-content-lg-end">
                <NavItem>
                  <NavLink
                    href="https://www.facebook.com/udomsakulnee"
                    target="_blank"
                  >
                    {t("DEVELOPED_BY")} <span>{t("WISUT_UDOMSAKULNEE")}</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/privacy-policy" target="_blank">
                    {t("PRIVACY_POLICY")}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink>
                    {t("VERSION")} {APP_VERSION}
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default AuthFooter;
