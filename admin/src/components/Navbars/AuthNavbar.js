/*!

=========================================================
* Argon Dashboard PRO React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// react library for routing
import { Link } from "react-router-dom";
// reactstrap components
import {
  UncontrolledCollapse,
  NavbarBrand,
  Navbar,
  NavItem,
  Nav,
  Container,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { LOCALE_ENGLISH, LOCALE_THAI } from "i18n";
import { dayjs, setStorage } from "utils";
import { KEY_LOCALE } from "config";
import { useTranslation } from "react-i18next";

function AdminNavbar() {
  const { i18n, t } = useTranslation();

  const changeLanguage = (locale) => () => {
    i18n.changeLanguage(locale);
    dayjs.locale(locale.toLowerCase());
    setStorage(KEY_LOCALE, locale);
  };

  return (
    <>
      <Navbar
        className="navbar-horizontal navbar-main navbar-dark navbar-transparent"
        expand="lg"
        id="navbar-main"
      >
        <Container>
          <NavbarBrand to="/" tag={Link}>
            <img
              alt="..."
              src={require("assets/img/brand/logo.svg").default}
              className="brand-logo brand-logo-sm"
            />
          </NavbarBrand>
          <button
            aria-controls="navbar-collapse"
            aria-expanded={false}
            aria-label="Toggle navigation"
            className="navbar-toggler"
            data-target="#navbar-collapse"
            data-toggle="collapse"
            id="navbar-collapse"
            type="button"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <UncontrolledCollapse
            className="navbar-custom-collapse"
            navbar
            toggler="#navbar-collapse"
          >
            <div className="navbar-collapse-header">
              <Row>
                <Col className="collapse-brand" xs="6">
                  <Link to="/admin/dashboard">
                    <img
                      alt="..."
                      src={require("assets/img/brand/blue.png").default}
                    />
                  </Link>
                </Col>
                <Col className="collapse-close" xs="6">
                  <button
                    aria-controls="navbar-collapse"
                    aria-expanded={false}
                    aria-label="Toggle navigation"
                    className="navbar-toggler"
                    data-target="#navbar-collapse"
                    data-toggle="collapse"
                    id="navbar-collapse"
                    type="button"
                  >
                    <span />
                    <span />
                  </button>
                </Col>
              </Row>
            </div>
            <hr className="d-lg-none" />
            <Nav className="align-items-lg-center ml-lg-auto" navbar>
              <NavItem className="d-none d-lg-block ml-lg-4">
                <UncontrolledDropdown>
                  <DropdownToggle
                    caret
                    color="secondary"
                    data-testid="currentLocale"
                  >
                    <img
                      className="locale flag"
                      alt={i18n.language}
                      src={
                        require(`assets/img/icons/flags/${i18n.language}.png`)
                          .default
                      }
                    />
                    &nbsp;&nbsp;
                    {t(i18n.language)}
                  </DropdownToggle>
                  <DropdownMenu>
                    <li>
                      <DropdownItem
                        onClick={changeLanguage(LOCALE_ENGLISH)}
                        data-testid="localeEN"
                      >
                        <img
                          className="locale flag"
                          alt={LOCALE_ENGLISH}
                          src={require("assets/img/icons/flags/EN.png").default}
                        />
                        {t(LOCALE_ENGLISH)}
                      </DropdownItem>
                    </li>
                    <li>
                      <DropdownItem
                        onClick={changeLanguage(LOCALE_THAI)}
                        data-testid="localeTH"
                      >
                        <img
                          className="locale flag"
                          alt={LOCALE_THAI}
                          src={require("assets/img/icons/flags/TH.png").default}
                        />
                        {t(LOCALE_THAI)}
                      </DropdownItem>
                    </li>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </NavItem>
            </Nav>
          </UncontrolledCollapse>
        </Container>
      </Navbar>
    </>
  );
}

export default AdminNavbar;
