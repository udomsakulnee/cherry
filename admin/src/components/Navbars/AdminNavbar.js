/*!

=========================================================
* Argon Dashboard PRO React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState } from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// reactstrap components
import {
  Collapse,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Media,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from "reactstrap";
import { LOCALE_ENGLISH, LOCALE_THAI } from "i18n";
import { dayjs, hash, setStorage } from "utils";
import { get, isEmpty } from "lodash";
import DealerCardModal from "components/Modal/DealerCardModal";
import RegisterModal from "components/Modal/RegisterModal";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { API_URL, KEY_LOCALE } from "config";

function AdminNavbar({ theme, sidenavOpen, toggleSidenav }) {
  const { i18n, t } = useTranslation();
  const history = useHistory();
  const user = useSelector((state) => state.user);
  const { username, avatar, mSex, mLevel } = user;
  let userProfile = username;
  let userAvatar = get(avatar, "formats.thumbnail.url");
  if (userAvatar) {
    userAvatar = `${API_URL}${userAvatar}`;
  } else {
    const sex = get(mSex, "nameEN", "Male").toLowerCase();
    userAvatar = require(`assets/img/icons/common/${sex}.svg`);
  }
  if (
    !isEmpty(user[`firstname${i18n.language}`]) &&
    !isEmpty(user[`lastname${i18n.language}`])
  ) {
    userProfile = `${user[`firstname${i18n.language}`]} ${
      user[`lastname${i18n.language}`]
    }`;
  }
  userProfile = `${userProfile} (${user.mLevel[`name${i18n.language}`]})`;

  const changeLanguage = (locale) => () => {
    i18n.changeLanguage(locale);
    dayjs.locale(locale.toLowerCase());
    setStorage(KEY_LOCALE, locale);
  };

  // function that on mobile devices makes the search open
  const openSearch = () => {
    document.body.classList.add("g-navbar-search-showing");
    setTimeout(function () {
      document.body.classList.remove("g-navbar-search-showing");
      document.body.classList.add("g-navbar-search-show");
    }, 150);
    setTimeout(function () {
      document.body.classList.add("g-navbar-search-shown");
    }, 300);
  };
  // function that on mobile devices makes the search close
  const closeSearch = () => {
    document.body.classList.remove("g-navbar-search-shown");
    setTimeout(function () {
      document.body.classList.remove("g-navbar-search-show");
      document.body.classList.add("g-navbar-search-hiding");
    }, 150);
    setTimeout(function () {
      document.body.classList.remove("g-navbar-search-hiding");
      document.body.classList.add("g-navbar-search-hidden");
    }, 300);
    setTimeout(function () {
      document.body.classList.remove("g-navbar-search-hidden");
    }, 500);
  };

  const [isOpenRegisterModal, setIsOpenRegisterModal] = useState(false);
  const linkRegister = `${window.location.origin}/auth/register/${hash(
    user.username
  )}`;
  const openRegisterModal = (isOpen) => {
    setIsOpenRegisterModal(isOpen);
  };

  const [isOpenCardModal, setIsOpenCardModal] = useState(false);
  const openCardModal = (isOpen) => {
    setIsOpenCardModal(isOpen);
  };

  return (
    <>
      <Navbar
        className={classnames(
          "navbar-top navbar-expand border-bottom",
          { "navbar-dark bg-default": theme === "dark" },
          { "navbar-light bg-default": theme === "light" }
        )}
      >
        <Container fluid>
          <Collapse navbar isOpen={true}>
            <Form
              className={classnames(
                "navbar-search form-inline mr-sm-3",
                { "navbar-search-light": theme === "dark" },
                { "navbar-search-dark": theme === "light" }
              )}
            >
              <FormGroup className="mb-0">
                <InputGroup className="input-group-alternative input-group-merge">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fas fa-search" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder={t("SEARCH")} type="text" />
                </InputGroup>
              </FormGroup>
              <button
                aria-label="Close"
                className="close"
                type="button"
                onClick={closeSearch}
              >
                <span aria-hidden={true}>×</span>
              </button>
            </Form>

            <Nav className="align-items-center ml-md-auto" navbar>
              <NavItem className="d-xl-none">
                <div
                  className={classnames(
                    "pr-3 sidenav-toggler",
                    { active: sidenavOpen },
                    { "sidenav-toggler-dark": theme === "dark" }
                  )}
                  onClick={toggleSidenav}
                >
                  <div className="sidenav-toggler-inner">
                    <i className="sidenav-toggler-line" />
                    <i className="sidenav-toggler-line" />
                    <i className="sidenav-toggler-line" />
                  </div>
                </div>
              </NavItem>
              {mLevel.level > 1 && (
                <>
                  <NavItem className="d-sm-none">
                    <NavLink onClick={openSearch}>
                      <i className="ni ni-zoom-split-in" />
                    </NavLink>
                  </NavItem>
                  <UncontrolledDropdown nav>
                    <DropdownToggle className="nav-link" color="" tag="a">
                      <i className="ni ni-ungroup" />
                    </DropdownToggle>
                    <DropdownMenu
                      className="dropdown-menu-lg dropdown-menu-dark bg-gradient-default opacity-8"
                      right
                    >
                      <Row className="shortcuts px-4">
                        {mLevel.level < 12 && (
                          <Col
                            className="shortcut-item"
                            onClick={() => openCardModal(true)}
                            xs="4"
                            tag="a"
                          >
                            <span className="shortcut-media avatar rounded-circle bg-gradient-green">
                              <i className="far fa-id-card" />
                            </span>
                            <small>{t("DEALER_CARD")}</small>
                          </Col>
                        )}
                        <Col
                          className="shortcut-item"
                          onClick={() => openRegisterModal(true)}
                          xs="4"
                          tag="a"
                        >
                          <span className="shortcut-media avatar rounded-circle bg-gradient-red">
                            <i className="fas fa-qrcode" />
                          </span>
                          <small>{t("DEALER_REGISTER")}</small>
                        </Col>
                      </Row>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </>
              )}
              <UncontrolledDropdown nav>
                <DropdownToggle className="nav-link pr-3" color="" tag="a">
                  <span>
                    <img
                      className="locale flag"
                      alt={i18n.language}
                      src={
                        require(`assets/img/icons/flags/${i18n.language}.png`)
                          .default
                      }
                    />
                  </span>
                  &nbsp;&nbsp;
                  <span className="mb-0 text-sm font-weight-bold">
                    {t(i18n.language)}
                  </span>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={changeLanguage(LOCALE_ENGLISH)}>
                    <img
                      className="locale flag"
                      alt={i18n.language}
                      src={require("assets/img/icons/flags/EN.png").default}
                    />
                    <span>{t(LOCALE_ENGLISH)}</span>
                  </DropdownItem>
                  <DropdownItem onClick={changeLanguage(LOCALE_THAI)}>
                    <img
                      className="locale flag"
                      alt={i18n.language}
                      src={require("assets/img/icons/flags/TH.png").default}
                    />
                    <span>{t(LOCALE_THAI)}</span>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
            <Nav className="align-items-center ml-auto ml-md-0" navbar>
              <UncontrolledDropdown nav>
                <DropdownToggle className="nav-link pr-0" color="" tag="a">
                  <Media className="align-items-center">
                    <span className="avatar avatar-sm rounded-circle">
                      <img
                        alt={userProfile}
                        src={userAvatar}
                        style={{
                          objectFit: "cover",
                          width: "100%",
                          height: "100%",
                        }}
                      />
                    </span>
                    <Media className="ml-2 d-none d-lg-block">
                      <span className="mb-0 text-sm font-weight-bold">
                        {userProfile}
                      </span>
                    </Media>
                  </Media>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem className="noti-title" header tag="div">
                    <h6 className="text-overflow m-0">{t("WELCOME")}!</h6>
                  </DropdownItem>
                  <DropdownItem
                    onClick={() => {
                      history.push("/admin/profile");
                    }}
                  >
                    <i className="ni ni-single-02" />
                    <span>{t("MY_PROFILE")}</span>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem onClick={() => history.push("/auth/login")}>
                    <i className="ni ni-user-run" />
                    <span>{t("LOGOUT")}</span>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
      {isOpenRegisterModal && (
        <RegisterModal
          isOpenModal={isOpenRegisterModal}
          onClose={openRegisterModal}
          linkRegister={linkRegister}
          userProfile={userProfile}
        />
      )}
      {isOpenCardModal && (
        <DealerCardModal
          isOpenModal={isOpenCardModal}
          onClose={openCardModal}
        />
      )}
    </>
  );
}

AdminNavbar.defaultProps = {
  toggleSidenav: () => {},
  sidenavOpen: false,
  theme: "dark",
};
AdminNavbar.propTypes = {
  toggleSidenav: PropTypes.func,
  sidenavOpen: PropTypes.bool,
  theme: PropTypes.oneOf(["dark", "light"]),
};

export default AdminNavbar;
