import React, { useState } from "react";
import { Input } from "reactstrap";
import { IconEye } from "./InputPassword.style";

const InputPassword = (props) => {
  const [isShowPassword, setIsShowPassword] = useState(false);
  const toggleShowPassword = () => {
    setIsShowPassword(!isShowPassword);
  };
  return (
    <>
      <Input {...props} type={isShowPassword ? "text" : "password"} />
      <IconEye
        className={`fas fa-eye${isShowPassword ? "" : "-slash"}`}
        onClick={toggleShowPassword}
      ></IconEye>
    </>
  );
};

export default InputPassword;
