import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";

const PasswordPattern = ({ passwordStrength }) => {
  const { t } = useTranslation();
  return (
    <>
      <div className="text-muted font-italic mb-3">
        <small>
          {t("REGISTER__PASSWORD_STRENGTH")}{" "}
          <span
            className={`${
              passwordStrength === "NOT_FOUND"
                ? ""
                : passwordStrength === "WEAK"
                ? "text-danger"
                : passwordStrength === "MEDIUM"
                ? "text-warning"
                : "text-success"
            } font-weight-700`}
          >
            {t(passwordStrength)}
          </span>
        </small>
      </div>
      <div className="text-muted mb-4">
        <small>
          <ul>
            <li>{t("REGISTER__PASSWORD_PATTERN1")}</li>
            <li>{t("REGISTER__PASSWORD_PATTERN2")}</li>
            <li>{t("REGISTER__PASSWORD_PATTERN3")}</li>
            <li>{t("REGISTER__PASSWORD_PATTERN4")}</li>
          </ul>
        </small>
      </div>
    </>
  );
};

PasswordPattern.defaultProps = {
  passwordStrength: "",
};

PasswordPattern.propTypes = {
  passwordStrength: PropTypes.string,
};

export default PasswordPattern;
