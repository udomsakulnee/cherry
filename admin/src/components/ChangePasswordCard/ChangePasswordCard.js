import { Button, Card, CardBody, CardHeader, Col, Form, Row } from "reactstrap";
import React, { useState } from "react";
import { API_PASSWORD_ME } from "config";
import { FormInput } from "components/Forms";
import { updateData } from "api";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { checkPasswordStrength, passwordPattern } from "utils";
import { showAlertWarning, showAlertSuccess } from "redux/actions";
import PasswordPattern from "components/ChangePasswordCard/PasswordPattern";

const ChangePasswordCard = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm();
  const history = useHistory();
  const [isEditPassword, setIsEditPassword] = useState(false);
  const [passwordStrength, setPasswordStrength] = useState("NOT_FOUND");

  const handleSavePassword = async (data) => {
    const { oldPassword, newPassword, confirmNewPassword } = data;
    if (newPassword !== confirmNewPassword) {
      dispatch(showAlertWarning({ text: t("REGISTER__PASSWORD_NOT_MATCH") }));
      return;
    }
    const params = {
      oldPassword,
      newPassword,
    };
    const response = await updateData(API_PASSWORD_ME, "", params, dispatch);
    if (response) {
      dispatch(showAlertSuccess({ text: t("PASSWORD_CHANGE_SUCCESS") }));
      history.push("/auth/login");
    } else {
      dispatch(showAlertWarning({ text: t("PASSWORD_INVALID") }));
    }
  };
  return (
    <>
      <Card>
        <CardHeader>
          <div className="row align-items-center">
            <div className="col-8">
              <h3 className="mb-0">{t("CHANGE_PASSWORD")}</h3>
            </div>
            {!isEditPassword && (
              <div className="col-4 text-right">
                <a
                  href="javascript:void(0)"
                  onClick={() => setIsEditPassword(!isEditPassword)}
                >
                  <i className="fas fa-pen" /> {t("EDIT")}
                </a>
              </div>
            )}
          </div>
        </CardHeader>
        <CardBody>
          <Form onSubmit={handleSubmit(handleSavePassword)}>
            <Row>
              <Col xs={12}>
                <FormInput
                  label={`${t("OLD_PASSWORD")}`}
                  name="oldPassword"
                  control={control}
                  displayType={!isEditPassword ? "text" : "input"}
                  type="password"
                  rules={{ required: true }}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <FormInput
                  label={`${t("NEW_PASSWORD")}`}
                  name="newPassword"
                  control={control}
                  displayType={!isEditPassword ? "text" : "input"}
                  type="password"
                  rules={{
                    required: true,
                    pattern: passwordPattern,
                  }}
                  onInput={checkPasswordStrength(setPasswordStrength)}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <FormInput
                  label={`${t("CONFIRM_NEW_PASSWORD")}`}
                  name="confirmNewPassword"
                  displayType={!isEditPassword ? "text" : "input"}
                  control={control}
                  type="password"
                  rules={{ required: true }}
                />
              </Col>
            </Row>
            <PasswordPattern passwordStrength={passwordStrength} />
            <Row>
              <Col xs={12} className="text-right">
                {isEditPassword && (
                  <>
                    <Button
                      className="btn-icon btn-3"
                      color="success"
                      type="submit"
                    >
                      <span className="btn-inner--icon">
                        <i className="fas fa-save"></i>
                      </span>
                      <span className="btn-inner--text">{t("SAVE")}</span>
                    </Button>
                    <Button
                      className="btn-icon btn-3"
                      color="secondary"
                      type="button"
                      onClick={() => setIsEditPassword(!isEditPassword)}
                    >
                      <span className="btn-inner--icon">
                        <i className="fa fa-ban"></i>
                      </span>
                      <span className="btn-inner--text">{t("CANCEL")}</span>
                    </Button>
                  </>
                )}
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    </>
  );
};
export default ChangePasswordCard;
