import { useDispatch, useSelector } from "react-redux";
import React from "react";
import ReactBSAlert from "react-bootstrap-sweetalert";
import { hideAlert } from "redux/actions";
import { useTranslation } from "react-i18next";

export const SweetAlert = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    isShow,
    title,
    text,
    type,
    confirmText,
    cancelText,
    onConfirm,
    onCancel,
  } = useSelector((state) => state.alert);

  const handleConfirm = () => {
    dispatch(hideAlert());
    onConfirm && onConfirm();
  };

  const handleCancel = () => {
    dispatch(hideAlert());
    onCancel && onCancel();
  };

  return (
    isShow && (
      <ReactBSAlert
        type={type === "confirm" ? "custom" : type}
        style={{ display: "block" }}
        title={title ? title : t(`ALERT__TITLE_${type.toUpperCase()}`)}
        onConfirm={handleConfirm}
        onCancel={handleCancel}
        confirmBtnBsStyle={type === "confirm" ? "danger" : type}
        confirmBtnText={
          confirmText ? confirmText : type === "confirm" ? t("YES") : t("OK")
        }
        showConfirm={true}
        cancelBtnBsStyle="secondary"
        cancelBtnText={cancelText ? cancelText : t("NO")}
        showCancel={type === "confirm"}
        customIcon={
          type === "confirm" && (
            <div
              className="swal2-icon swal2-question swal2-animate-question-icon"
              style={{ display: "flex" }}
            >
              <span className="swal2-icon-text">?</span>
            </div>
          )
        }
      >
        {text}
      </ReactBSAlert>
    )
  );
};

export default SweetAlert;
