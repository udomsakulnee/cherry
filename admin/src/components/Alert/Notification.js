import React, { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import NotificationAlert from "react-notification-alert";
import { hideNotify } from "redux/actions";
import { useTranslation } from "react-i18next";

export const Notification = () => {
  const { t } = useTranslation();
  const notification = useRef();
  const dispatch = useDispatch();
  const { title, text, type, isShow } = useSelector(
    (state) => state.notification
  );

  if (isShow) {
    let options = {
      place: "tr",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {title ? title : t(`ALERT__TITLE_${type.toUpperCase()}`)}
          </span>
          <span data-notify="message">{text}</span>
        </div>
      ),
      type,
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notification.current.notificationAlert(options);
    dispatch(hideNotify());
  }

  return (
    <div className="rna-wrapper">
      <NotificationAlert ref={notification} />
    </div>
  );
};

export default Notification;
