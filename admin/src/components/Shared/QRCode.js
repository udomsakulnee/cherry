import PropTypes from "prop-types";
import QRCodeLib from "qrcode.react";
import React from "react";

export const QRCode = ({ value, imageSettings, size }) => {
  return <QRCodeLib {...{ value, size, imageSettings }} />;
};

QRCode.defaultProps = {
  size: 256,
  imageSettings: {
    src: require("assets/img/brand/logo.jpg"),
    height: 50,
    width: 50,
  },
};

QRCode.propTypes = {
  value: PropTypes.string,
  size: PropTypes.number,
  imageSettings: PropTypes.object,
};

export default QRCode;
