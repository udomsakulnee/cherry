import Format from "react-number-format";
import PropTypes from "prop-types";
import React from "react";

export const NumberFormat = ({ className, value, type }) => {
  let format = null;
  switch (type) {
    case "idcard":
      format = "# #### ##### ## #";
      break;
    case "tel":
      format = "## #### ####";
      break;
    case "barcode":
      format = "# ###### ######";
      break;
    case "fda":
      format = "## # ##### # ####";
      break;
    case "account":
      format = "### # ##### #";
      break;
  }

  return (
    <Format
      className={className}
      value={value}
      displayType="text"
      thousandSeparator={type === "number" || type === "money"}
      prefix={type === "money" ? "฿ " : ""}
      decimalScale={type === "money" ? 2 : 0}
      format={format}
    />
  );
};

NumberFormat.defaultProps = {
  className: "",
  value: 0,
  type: "number",
};

NumberFormat.propTypes = {
  className: PropTypes.string,
  value: PropTypes.number,
  type: PropTypes.string,
};

export default NumberFormat;
