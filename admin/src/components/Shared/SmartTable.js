import {
  CardFooter,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  UncontrolledDropdown,
  Button,
} from "reactstrap";
import React, { useState, useEffect } from "react";
import { get, noop } from "lodash";
import { getStorage, setStorage } from "utils";
import { KEY_LIMIT } from "config";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { Datetime } from "components/Forms";
import { dayjs } from "utils";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

export const SmartTable = ({
  total,
  children,
  onReload,
  isClientSide,
  report,
}) => {
  const pageLimit = [10, 25, 50, 100];
  const { t } = useTranslation();
  const [start, setStart] = useState(0);
  const defaultLimit = getStorage(KEY_LIMIT);
  const [limit, setLimit] = useState(defaultLimit || 10);
  const [sortKey, setSortKey] = useState("id");
  const [sortType, setSortType] = useState("desc");
  const [date, setDate] = useState();

  useEffect(() => {
    if (isClientSide) {
      changeDate(dayjs());
    } else {
      reload();
    }
  }, []);

  const reload = (params = {}) => {
    let newParams = {
      _start: start,
      _limit: limit,
      _sort: `${sortKey}:${sortType}`,
      ...params,
    };
    !isClientSide && onReload(newParams);
  };

  const changePageLimit = (newLimit) => () => {
    setStart(0);
    setLimit(newLimit);
    setStorage(KEY_LIMIT, newLimit);
    reload({ _start: 0, _limit: newLimit });
  };

  const changePageNumber = (newStart) => () => {
    setStart(newStart);
    reload({ _start: newStart });
  };

  const changeSortBy = (newKey) => () => {
    if (newKey) {
      if (sortKey !== newKey) {
        setSortKey(newKey);
        setSortType("asc");
        reload({ _sort: `${newKey}:asc` });
      } else {
        if (sortType === "asc") {
          setSortType("desc");
          reload({ _sort: `${newKey}:desc` });
        } else {
          setSortKey("id");
          setSortType("desc");
          reload({ _sort: "id:desc" });
        }
      }
    }
  };

  const changeDate = (d) => {
    const created_at_gte = `${dayjs(d).year()}-${dayjs(d).format("MM-01")}`;
    const created_at_lte = `${dayjs(d).year()}-${dayjs(d).format("MM")}-${dayjs(
      d
    )
      .endOf("month")
      .format("DD")}`;
    setDate(d);
    onReload({ created_at_gte, created_at_lte });
  };

  const generatePageLink = (i) => {
    const active = start / limit + 1 === i;
    return (
      <PaginationItem key={i} className={active && "active"}>
        <PaginationLink onClick={!active && changePageNumber((i - 1) * limit)}>
          {i}
        </PaginationLink>
      </PaginationItem>
    );
  };

  const generatePageNumbers = () => {
    let elements = [];
    const totalPage = Math.ceil(total / limit);
    if (totalPage < 8) {
      for (let i = 1; i <= totalPage; i += 1) {
        elements.push(generatePageLink(i));
      }
    } else {
      const current = start / limit + 1;
      elements.push(generatePageLink(1));
      if (current > 4) {
        elements.push(<span key={1}>...</span>);
      } else {
        elements.push(generatePageLink(2));
        elements.push(generatePageLink(3));
        elements.push(generatePageLink(4));
        elements.push(generatePageLink(5));
        elements.push(generatePageLink(6));
      }

      if (current > 4 && totalPage - current > 3) {
        elements.push(generatePageLink(current - 2));
        elements.push(generatePageLink(current - 1));
        elements.push(generatePageLink(current));
        elements.push(generatePageLink(current + 1));
        elements.push(generatePageLink(current + 2));
      }

      if (totalPage - current < 4) {
        elements.push(generatePageLink(totalPage - 5));
        elements.push(generatePageLink(totalPage - 4));
        elements.push(generatePageLink(totalPage - 3));
        elements.push(generatePageLink(totalPage - 2));
        elements.push(generatePageLink(totalPage - 1));
      } else {
        elements.push(<span key={totalPage - 1}>...</span>);
      }
      elements.push(generatePageLink(totalPage));
    }
    return elements;
  };

  const header = get(children[0], "props.children.props.children");
  const body = get(children[1], "props.children");
  const foot = get(children[2], "props.children");

  return (
    <>
      {isClientSide && (
        <div className="p-3 mb-2 bg-success">
          <div className="float-left">
            <Datetime value={date} onChange={changeDate} />
          </div>
          {report && (
            <ExcelFile
              element={
                <Button
                  className="btn-icon btn-3 float-right"
                  color="secondary"
                  type="button"
                  disabled={total == 0}
                >
                  <span className="btn-inner--icon">
                    <i className="fas fa-file-export"></i>
                  </span>
                  <span className="btn-inner--text">{t("EXPORT_FILE")}</span>
                </Button>
              }
            >
              <ExcelSheet dataSet={report} name="Organization" />
            </ExcelFile>
          )}
        </div>
      )}

      <Table
        className="align-items-center table-flush"
        responsive
        hover
        striped
      >
        <thead className="thead-light">
          <tr>
            {header.map((item, key) => {
              const dataSort = get(item, "props.data-sort");
              const children = get(item, "props.children");
              return (
                <th
                  key={key}
                  className={dataSort && "sort"}
                  data-sort={dataSort}
                  scope="col"
                  onClick={changeSortBy(dataSort)}
                >
                  {children}
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody className="list">
          {total > 0 ? (
            isClientSide ? (
              body.filter((item, key) => key >= start && key < start + limit)
            ) : (
              body
            )
          ) : (
            <tr>
              <td rowSpan={header.length}>{t("SELECT__NO_RESULTS")}</td>
            </tr>
          )}
        </tbody>
        {foot && total > 0 && (
          <thead className="thead-light">
            <tr>
              {header.map((item, key) => {
                const dataSort = get(item, "props.data-sort");
                const children = get(item, "props.children");
                return (
                  <th
                    key={key}
                    className={dataSort && "sort"}
                    data-sort={dataSort}
                    scope="col"
                    onClick={changeSortBy(dataSort)}
                  >
                    {children}
                  </th>
                );
              })}
            </tr>
            {foot}
          </thead>
        )}
      </Table>
      <CardFooter className="py-4">
        <UncontrolledDropdown group className="float-left btn-group-sm">
          <p className="mb-0 mt-1 mr-2 text-sm">{t("SHOW")}</p>
          <DropdownToggle caret color="secondary" className="mt-1">
            {limit}
          </DropdownToggle>
          <DropdownMenu>
            {pageLimit.map((item) => (
              <DropdownItem key={item} onClick={changePageLimit(item)}>
                {item}
              </DropdownItem>
            ))}
          </DropdownMenu>
          <p className="mb-0 mt-1 ml-2 text-sm">
            {t("SHOWING", {
              start: total > 0 ? start + 1 : 0,
              end: start + limit < total ? start + limit : total,
              total,
            })}
          </p>
        </UncontrolledDropdown>

        <Pagination
          className="pagination justify-content-end mb-0"
          listClassName="justify-content-end mb-0"
        >
          <PaginationItem className={start === 0 && "disabled"}>
            <PaginationLink
              onClick={changePageNumber(start - limit)}
              tabIndex="-1"
            >
              <i className="fas fa-angle-left" />
              <span className="sr-only">Previous</span>
            </PaginationLink>
          </PaginationItem>
          {generatePageNumbers()}
          <PaginationItem className={start + limit >= total && "disabled"}>
            <PaginationLink onClick={changePageNumber(start + limit)}>
              <i className="fas fa-angle-right" />
              <span className="sr-only">Next</span>
            </PaginationLink>
          </PaginationItem>
        </Pagination>
      </CardFooter>
    </>
  );
};

SmartTable.defaultProps = {
  total: 0,
  children: [],
  onReload: noop,
  isClientSide: false,
  report: null,
};

SmartTable.propTypes = {
  total: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
  onReload: PropTypes.func,
  isClientSide: PropTypes.bool,
  report: PropTypes.array,
};

export default SmartTable;
