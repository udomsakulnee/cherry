import React, { useEffect, useState } from "react";
import DealerCard from "components/DealerCard/DealerCard";
import { Modal } from "reactstrap";
import PropTypes from "prop-types";
import { noop } from "lodash";
import { useTranslation } from "react-i18next";

const DealerCardModal = ({ isOpenModal, onClose }) => {
  const { t } = useTranslation();
  const [isOpen, setIsOpen] = useState(false);
  const toggleModal = () => {
    setIsOpen(!isOpen);
    onClose(!isOpen);
  };

  useEffect(() => {
    setIsOpen(isOpenModal);
  }, [isOpenModal]);

  return (
    <Modal
      className="modal-dialog-centered"
      size="lg"
      isOpen={isOpen}
      toggle={toggleModal}
    >
      <div className="modal-header">
        <h5 className="modal-title">{t("DEALER_CARD")}</h5>
        <button
          aria-label="Close"
          className="close"
          data-dismiss="modal"
          type="button"
          onClick={toggleModal}
        >
          <span aria-hidden={true}>×</span>
        </button>
      </div>
      <div className="modal-body pt-0">
        <DealerCard />
      </div>
    </Modal>
  );
};

DealerCardModal.defaultProps = {
  isOpenModal: false,
  onClose: noop,
};

DealerCardModal.propTypes = {
  isOpenModal: PropTypes.bool,
  onClose: PropTypes.func,
};

export default DealerCardModal;
