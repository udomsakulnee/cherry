import { Button, Col, Modal, UncontrolledTooltip } from "reactstrap";
import React, { useEffect, useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import PropTypes from "prop-types";
import { QRCode } from "components/Shared";
import { useTranslation } from "react-i18next";

const RegisterModal = ({ isOpenModal, onClose, linkRegister, userProfile }) => {
  const { t } = useTranslation();
  const [isOpen, setIsOpen] = useState(false);
  const [copiedText, setCopiedText] = useState("");
  const toggleModal = () => {
    setIsOpen(!isOpen);
    onClose(!isOpen);
  };

  useEffect(() => {
    setIsOpen(isOpenModal);
  }, [isOpenModal]);

  return (
    <Modal
      className="modal-dialog-centered"
      size="md"
      isOpen={isOpen}
      toggle={toggleModal}
    >
      <div className="modal-header">
        <button
          aria-label="Close"
          className="close"
          data-dismiss="modal"
          type="button"
          onClick={toggleModal}
        >
          <span aria-hidden={true}>×</span>
        </button>
      </div>
      <div className="modal-body p-0">
        <div className="text-muted text-center mt-2 mb-2">
          <img
            alt="dermatid"
            src={require("assets/img/brand/logo.svg").default}
            style={{ width: "140px" }}
          />
        </div>
        <div className="text-muted text-center mt-4 mb-4">
          <span>{t("REGISTER")}</span>
          <br />
          <small>{`${t("PARENT")}: ${userProfile}`}</small>
        </div>
        <div className="text-center text-muted m-4">
          <QRCode value={linkRegister} />
        </div>
        <Col md="12">
          <CopyToClipboard
            text={linkRegister}
            onCopy={() => setCopiedText(linkRegister)}
          >
            <Button
              className="btn-icon-clipboard"
              id="btn_tooltip_clipboard"
              type="button"
            >
              <div>
                <i className="ni ni-single-copy-04" />
                <span>{linkRegister}</span>
              </div>
            </Button>
          </CopyToClipboard>
          <UncontrolledTooltip
            delay={0}
            trigger="hover focus"
            target="btn_tooltip_clipboard"
          >
            {copiedText === linkRegister ? t("COPIED") : t("COPY_TO_CLIPBOARD")}
          </UncontrolledTooltip>
        </Col>
        <div className="text-center mt-4 mb-4">
          <Button color="danger" type="button" onClick={toggleModal}>
            {t("CLOSE")}
          </Button>
        </div>
      </div>
    </Modal>
  );
};

RegisterModal.defaultProps = {
  isOpenModal: false,
  onClose: () => {},
  userProfile: "-",
};

RegisterModal.propTypes = {
  isOpenModal: PropTypes.bool,
  onClose: PropTypes.func,
  linkRegister: PropTypes.string,
  userProfile: PropTypes.string,
};

export default RegisterModal;
