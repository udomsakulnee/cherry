import { Button, Modal } from "reactstrap";
import React, { useEffect, useState } from "react";
import FileUpload from "components/Forms/FileUpload";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

const UploadModal = ({ isOpenModal, onClose, onChange, value }) => {
  const { t } = useTranslation();
  const [isOpen, setIsOpen] = useState(false);

  const toggleModal = () => {
    setIsOpen(!isOpen);
    onClose(!isOpen);
  };

  const handleOnChange = (value) => {
    onChange(value);
  };

  useEffect(() => {
    setIsOpen(isOpenModal);
  }, [isOpenModal]);

  return (
    <Modal
      className="modal-dialog-centered"
      size="md"
      isOpen={isOpen}
      toggle={toggleModal}
    >
      <div className="modal-header">
        <button
          aria-label="Close"
          className="close"
          data-dismiss="modal"
          type="button"
          onClick={toggleModal}
        >
          <span aria-hidden={true}>×</span>
        </button>
      </div>
      <div className="modal-body p-0">
        <div className="text-muted m-4">
          <FileUpload onChange={handleOnChange} value={value} />
        </div>
        <div className="text-center mt-4 mb-4">
          <Button color="danger" type="button" onClick={toggleModal}>
            {t("CLOSE")}
          </Button>
        </div>
      </div>
    </Modal>
  );
};

UploadModal.defaultProps = {
  isOpenModal: false,
  onClose: () => {},
  userProfile: "-",
  onChange: () => {},
  value: {},
};

UploadModal.propTypes = {
  isOpenModal: PropTypes.bool,
  onClose: PropTypes.func,
  linkRegister: PropTypes.string,
  userProfile: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.object,
};

export default UploadModal;
