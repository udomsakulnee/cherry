import React, { useCallback, useState } from "react";
import { debounce, noop } from "lodash";
import { Input } from "reactstrap";
import NumberFormat from "react-number-format";
import PropTypes from "prop-types";
import styled from "styled-components";

const masterAddress = require("../../assets/json/address.json");
export const Address = ({ name, value, onChange, onChangeAddress }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [addressList, setAddressList] = useState([]);

  const searchAddress = useCallback(
    debounce((key) => {
      if (key.length > 2) {
        const result = masterAddress.filter(
          (address) =>
            String(address[name]).indexOf(key) > -1 ||
            String(address[`${name}EN`])
              .replaceAll(" ", "")
              .toLowerCase()
              .indexOf(key.replaceAll(" ", "").toLowerCase()) > -1
        );
        setAddressList(result);
      } else {
        setAddressList([]);
      }
    }, 100),
    []
  );

  const selectedAddress = (address) => () => {
    onChange(address[name]);
    onChangeAddress(address);
    setIsOpen(false);
    setAddressList([]);
  };

  return (
    <Container onFocus={() => setIsOpen(true)}>
      {name === "zipCode" ? (
        <NumberFormat
          className="form-control"
          value={value}
          format="#####"
          onValueChange={(e) => onChange(e.value)}
          onKeyUp={(e) => searchAddress(e.target.value)}
          onClick={(e) => searchAddress(e.target.value)}
        />
      ) : (
        <Input
          type="text"
          value={value}
          onChange={onChange}
          onInput={(e) => searchAddress(e.target.value)}
          onClick={(e) => searchAddress(e.target.value)}
          tabindex="1"
        />
      )}
      {addressList.length > 0 && (
        <AutoComplete
          style={{ display: isOpen ? "block" : "none" }}
          tabindex="1"
        >
          <UL>
            {addressList.map((address, index) => (
              <LI key={index} onClick={selectedAddress(address)}>
                <Text>{`${address.subDistrict}, ${address.district}, ${address.province}, ${address.zipCode}`}</Text>
                <TextEN>{`${address.subDistrictEN}, ${address.districtEN}, ${address.provinceEN}, ${address.zipCode}`}</TextEN>
              </LI>
            ))}
          </UL>
        </AutoComplete>
      )}
    </Container>
  );
};

Address.defaultProps = {
  name: "",
  value: "",
  onChange: noop,
  onChangeAddress: noop,
  invalid: false,
};

Address.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onChangeAddress: PropTypes.func,
  invalid: PropTypes.bool,
};

export default Address;

const Container = styled.div`
  position: relative;
`;

const AutoComplete = styled.div`
  width: 100%;
  background-color: red;
  padding: 0.5rem 0;
  background-color: #fff;
  border: 1px solid #dee2e6;
  border-radius: 0.4375rem;
  border-top: none;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  position: absolute;
  z-index: 1;
  display: none;
  max-height: 217px;
  overflow-y: auto;
`;

const UL = styled.ul`
  width: 100%;
  backgroud-color: red;
  list-style: none;
  margin: 0;
  padding: 0;
  border-top: none;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
`;

const LI = styled.li`
  cursor: pointer;
  padding: 0.5rem 0.75rem;
  background-color: #fff;
  color: #212529;
  font-size: 0.875rem;
`;

const Text = styled.p`
  margin-bottom: 0px;
  color: #212529;
  font-size: 0.875rem;
`;

const TextEN = styled.p`
  margin-bottom: 0px;
  color: #8898aa;
  font-size: 0.8125rem;
`;
