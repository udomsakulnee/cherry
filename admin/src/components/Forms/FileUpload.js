import {
  Badge,
  Button,
  Col,
  ListGroup,
  ListGroupItem,
  Modal,
  Row,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { deleteUploadFile, uploadFile } from "api";
import { get, noop } from "lodash";
import {
  notifySuccess,
  showAlertError,
  showAlertWarning,
  showConfirm,
} from "redux/actions";
import { API_URL } from "config";
import { Colors } from "variables/styles";
import PropTypes from "prop-types";
import { isEmpty } from "lodash";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { useDropzone } from "react-dropzone";
import { useTranslation } from "react-i18next";

const getColor = (props) => {
  if (props.isDragAccept) {
    return "#2dce89";
  }
  if (props.isDragReject) {
    return "#f5365c";
  }
  if (props.isDragActive) {
    return "#11cdef";
  }
  return "#dee2e6";
};

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 4rem 1rem;
  border-width: 1px;
  border-radius: 0.375rem;
  border-color: ${(props) => getColor(props)};
  border-style: dashed;
  background-color: #fff;
  color: #8898aa;
  outline: none;
  transition: all 0.15s ease;
  cursor: pointer;
  &:hover {
    border-color: #8898aa;
  }
`;

export const FileUpload = ({
  multiple,
  accept,
  maxFiles,
  maxSize,
  minSize,
  value,
  onChange,
  invalid,
}) => {
  const { t } = useTranslation();
  const [files, setFiles] = useState([]);
  const [toggleModal, setToggleModal] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    if (files !== value && !isEmpty(value)) {
      const count = get(value, "length", 0);
      count === 0 ? setFiles([value]) : setFiles(value);
    }
  }, [value]);

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    multiple,
    accept,
    maxFiles,
    maxSize: maxSize * 1024 * 1024,
    minSize: minSize * 1024 * 1024,
    onDropAccepted: async (newFiles) => {
      if (files.length < maxFiles) {
        const newFilesFilter = newFiles.filter((file) => {
          const findIndex = files.findIndex(
            (item) => item.name === file.name && item.size && file.size
          );
          if (findIndex > -1) {
            const message = {
              text: t("DROPZONE__ERROR_DUPLICATE_FILE"),
            };
            dispatch(showAlertWarning(message));
          }
          return findIndex === -1;
        });

        let successFile = files;
        newFilesFilter.map(async (file) => {
          const response = await uploadFile(file);
          if (response.length > 0) {
            const fileData = response.reduce((item) => item);
            successFile = [...successFile, fileData];
            changeFiles(successFile);

            const message = {
              text: t("DROPZONE__SUCCESS_UPLOAD_FILE"),
            };
            dispatch(notifySuccess(message));
          } else {
            const message = {
              text: t("DROPZONE__ERROR_FILE_UPLOAD"),
            };
            dispatch(showAlertError(message));
          }
        });
      } else {
        const message = {
          text: t("DROPZONE__ERROR_MAX_FILES_EXCEEDED", { maxFiles }),
        };
        dispatch(showAlertWarning(message));
      }
    },
    onDropRejected: (newFiles) => {
      const code = get(newFiles[0], "errors[0].code");
      let message;
      switch (code) {
        case "file-too-large":
          message = {
            text: t("DROPZONE__ERROR_FILE_TOO_LARGE", { maxSize }),
          };
          break;

        case "file-too-small":
          message = {
            text: t("DROPZONE__ERROR_FILE_TOO_SMALL", { minSize }),
          };
          break;

        case "too-many-files":
          message = {
            text: t("DROPZONE__ERROR_TOO_MANY_FILES"),
          };
          break;

        case "file-invalid-type":
          message = {
            text: t("DROPZONE__ERROR_FILE_INVALID_TYPE", { accept }),
          };
          break;

        default:
          message = {
            text: t("DROPZONE__ERROR_DEFAULT"),
          };
      }

      dispatch(showAlertWarning(message));
    },
  });

  const deleteFile = (id) => () => {
    const message = {
      text: t("DROPZONE__CONFIRM_DELETE"),
      onConfirm: async () => {
        const newFiles = files.filter((item) => item.id !== id);
        const response = await deleteUploadFile(id);
        if (response) {
          changeFiles(newFiles);
          const message = {
            text: t("DROPZONE__SUCCESS_DELETE_FILE"),
          };
          dispatch(notifySuccess(message));
        } else {
          const message = {
            text: t("DROPZONE__ERROR_DELETE_FILE"),
          };
          dispatch(showAlertError(message));
        }
      },
    };
    dispatch(showConfirm(message));
  };

  const changeFiles = (files) => {
    setFiles(files);
    onChange(files);
  };

  const filesItem = files.map((file, key) => {
    const thumbnailUrl = get(file, "formats.thumbnail.url");
    const imageUrl = get(file, "url");
    const widthHeight =
      file.width && file.height ? `— ${file.width}x${file.height}` : "";
    return (
      <ListGroupItem key={key} className=" px-0">
        <Row className="align-items-center">
          <Col className="col-auto">
            <div className="avatar avatar-xl d-flex overflow-hidden">
              <img
                className="avatar-img rounded"
                src={`${API_URL}${thumbnailUrl}`}
                onClick={() => {
                  setToggleModal(true);
                  setPreviewImage(`${API_URL}${imageUrl}`);
                }}
              />
            </div>
          </Col>
          <div className="col ml--3 overflow-hidden">
            <div>
              <h4 className="mb-1 text-truncate">
                <Badge color="primary" pill className="mr-2">
                  {widthHeight ? t("IMAGE") : t("FILE")}
                </Badge>
                {file.name}
              </h4>
            </div>
            <p className="small text-muted mb-0">{`${file.ext
              .replace(".", "")
              .toUpperCase()} ${widthHeight} — ${Math.round(file.size)}KB`}</p>
          </div>
          <Col className="col-auto">
            <Button
              size="sm"
              color="danger"
              type="button"
              onClick={deleteFile(file.id)}
            >
              <i className="fas fa-trash" />
            </Button>
          </Col>
        </Row>
      </ListGroupItem>
    );
  });

  const borderStyle = invalid
    ? { border: `1px solid ${Colors.warning}`, borderRadius: "0.25rem" }
    : null;

  return (
    <div style={borderStyle}>
      <Container
        {...getRootProps({ isDragActive, isDragAccept, isDragReject })}
      >
        <input {...getInputProps()} />
        <div>
          <i className="fas fa-cloud-upload-alt fa-2x"></i>{" "}
          {t("DROPZONE__DEFAULT_MESSAGE")}
        </div>
      </Container>
      <ListGroup className="dz-preview dz-preview-multiple list-group-lg" flush>
        {filesItem}
      </ListGroup>

      <Modal
        className="modal-dialog-centered modal-lg"
        isOpen={toggleModal}
        toggle={() => setToggleModal(false)}
      >
        <div className="modal-body">
          <img
            className="img-fluid"
            style={{ width: "100%" }}
            src={previewImage}
          />
        </div>
        <div className="modal-footer pt-0">
          <Button
            color="secondary"
            data-dismiss="modal"
            type="button"
            onClick={() => setToggleModal(false)}
          >
            {t("CLOSE")}
          </Button>
        </div>
      </Modal>
    </div>
  );
};

FileUpload.defaultProps = {
  multiple: true,
  accept: "image/*",
  maxFiles: 1,
  maxSize: 3,
  minSize: 0,
  value: [],
  onChange: noop,
  invalid: false,
};

FileUpload.propTypes = {
  multiple: PropTypes.bool,
  accept: PropTypes.string,
  maxFiles: PropTypes.number,
  maxSize: PropTypes.number,
  minSize: PropTypes.number,
  value: PropTypes.array,
  onChange: PropTypes.func,
  invalid: PropTypes.bool,
};

export default FileUpload;
