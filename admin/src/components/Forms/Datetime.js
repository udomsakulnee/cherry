import {
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import { LOCALE_ENGLISH } from "i18n";
import PropTypes from "prop-types";
import React from "react";
import ReactDatetime from "react-datetime";
import { dayjs } from "utils";
import { useTranslation } from "react-i18next";

export const Datetime = ({ value, onChange }) => {
  const { t, i18n } = useTranslation();

  const renderMonth = (props, month) => {
    return <td {...props}>{t(`MONTH_SHORT_${month}`)}</td>;
  };

  const renderYear = (props, year) => {
    return (
      <td {...props}>{i18n.language === LOCALE_ENGLISH ? year : year + 543}</td>
    );
  };

  const renderInput = (props) => {
    const [month, year] = props.value.split(" ");
    return (
      <input
        {...props}
        value={`${t(month.toUpperCase())} ${
          i18n.language === LOCALE_ENGLISH ? year : Number(year) + 543
        }`}
      />
    );
  };

  return (
    <FormGroup className="mb-0">
      <InputGroup className="input-group-alternative">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="ni ni-calendar-grid-58" />
          </InputGroupText>
        </InputGroupAddon>
        <ReactDatetime
          dateFormat="MMMM YYYY"
          renderMonth={renderMonth}
          renderYear={renderYear}
          renderInput={renderInput}
          timeFormat={false}
          value={value}
          onChange={onChange}
        />
      </InputGroup>
    </FormGroup>
  );
};

Datetime.defaultProps = {
  value: dayjs(),
  onChange: () => {},
};

Datetime.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func,
};

export default Datetime;
