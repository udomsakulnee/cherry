import { Colors } from "variables/styles";
import PropTypes from "prop-types";
import React from "react";
import ReactQuill from "react-quill";
import { noop } from "lodash";

export const TextEditor = ({ value, onChange, invalid }) => {
  const toolbarOptions = [
    [{ header: 1 }, { header: 2 }],
    [
      "bold",
      "italic",
      "underline",
      "strike",
      { script: "sub" },
      { script: "super" },
    ],
    [
      { align: "" },
      { align: "center" },
      { align: "right" },
      { align: "justify" },
    ],
    [
      { list: "bullet" },
      { list: "ordered" },
      { indent: "-1" },
      { indent: "+1" },
    ],
    ["link", "image"],
  ];

  const borderStyle = invalid
    ? { border: `1px solid ${Colors.warning}`, borderRadius: "0.25rem" }
    : null;

  return (
    <ReactQuill
      style={borderStyle}
      value={value}
      onChange={onChange}
      theme="snow"
      modules={{ toolbar: toolbarOptions }}
    />
  );
};

TextEditor.defaultProps = {
  value: "",
  onChange: noop,
  invalid: false,
};

TextEditor.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  invalid: PropTypes.bool,
};

export default TextEditor;
