import { FormGroup, Input } from "reactstrap";
import { get, isEmpty, noop } from "lodash";
import { Address } from "./Address";
import { Colors } from "variables/styles";
import { Controller } from "react-hook-form";
import FileUpload from "./FileUpload";
import NumberFormat from "react-number-format";
import PropTypes from "prop-types";
import React, { useState } from "react";
import Select2 from "react-select2-wrapper";
import TextEditor from "./TextEditor";
import styled from "styled-components";
import { useTranslation } from "react-i18next";

const Select = styled(Select2)`
  display: none;
  & + .select2-container .select2-selection {
    ${(props) => props.invalid && "border-color: " + Colors.warning};
  }
`;

export const FormInput = (props) => {
  const { t } = useTranslation();
  const {
    label,
    name,
    type,
    control,
    option,
    multiple,
    disabled,
    isSearchable,
    className,
    displayType,
    readonly,
    rules,
    onInput,
    onChangeAddress,
  } = props;
  const isRequired = get(rules, "required", false);
  const isInvalid = get(control, `formStateRef.current.errors.${name}`, false);
  const errorMessage = get(
    control,
    `formStateRef.current.errors.${name}.message`
  );
  let format = "";

  const renderInput = () => {
    switch (type) {
      case "text":
      case "textarea":
      case "email":
        return (
          <Controller
            {...props}
            render={({ field }) => (
              <Input
                {...field}
                id={name}
                invalid={isInvalid}
                className={
                  displayType === "text"
                    ? `form-control-plaintext bg-white shadow-none border-white ${className}`
                    : className
                }
                readonly={displayType === "text" ? "readonly" : readonly}
              />
            )}
          />
        );

      case "password":
        return (
          <Controller
            {...props}
            render={({ field }) => (
              <InputPassword
                {...field}
                id={name}
                invalid={isInvalid}
                onInput={onInput}
                className={
                  displayType === "text"
                    ? `form-control-plaintext bg-white shadow-none border-white ${className}`
                    : className
                }
                readonly={displayType === "text" ? "readonly" : readonly}
              />
            )}
          />
        );

      case "idcard":
      case "tel":
      case "barcode":
      case "fda":
        if (type === "idcard") format = "# #### ##### ## #";
        else if (type === "tel") {
          format = "### ### ####";
        } else if (type === "barcode") {
          format = "# ###### ######";
        } else if (type === "fda") {
          format = "## # ##### # ####";
        }
        return (
          <Controller
            {...props}
            render={({ field: { value, onChange } }) => {
              return (
                <NumberFormat
                  className={
                    displayType === "text"
                      ? "form-control-plaintext border-white"
                      : `form-control ${isInvalid && "border border-warning"}`
                  }
                  name={name}
                  value={value}
                  format={format}
                  onValueChange={(e) => onChange(e.value)}
                  displayType={displayType}
                />
              );
            }}
          />
        );

      case "number":
      case "money":
        return (
          <Controller
            {...props}
            render={({ field: { value, onChange } }) => {
              return (
                <NumberFormat
                  className={`form-control ${
                    isInvalid && "border border-warning"
                  }`}
                  name={name}
                  value={value}
                  thousandSeparator={true}
                  prefix={type === "money" ? "฿ " : ""}
                  allowNegative={false}
                  decimalScale={type === "money" ? 2 : 0}
                  onValueChange={(e) => onChange(e.floatValue)}
                />
              );
            }}
          />
        );

      case "switch":
        return (
          <div>
            <label className="custom-toggle">
              <Controller
                {...props}
                render={({ field: { value, onChange } }) => {
                  return (
                    <input
                      id={name}
                      type="checkbox"
                      onChange={(e) => onChange(e.target.checked)}
                      checked={value}
                    />
                  );
                }}
              />
              <span
                className="custom-toggle-slider rounded-circle"
                data-label-off={t("OFF")}
                data-label-on={t("ON")}
              />
            </label>
          </div>
        );

      case "select":
        return (
          <Controller
            {...props}
            render={({ field: { onChange, value } }) => {
              return (
                <Select
                  id={name}
                  data-minimum-results-for-search={!isSearchable && "Infinity"}
                  data={option}
                  multiple={multiple}
                  invalid={isInvalid}
                  disabled={disabled}
                  defaultValue={value}
                  onChange={(e) => {
                    if (multiple) {
                      const selectedOptions = [...e.target.selectedOptions];
                      const selectedValues =
                        selectedOptions.length > 0
                          ? selectedOptions.map((x) => Number(x.value))
                          : [];
                      onChange(selectedValues);
                    } else {
                      const value = !isEmpty(e.target.value)
                        ? e.target.value
                        : null;
                      onChange(value);
                    }
                  }}
                  options={{
                    language: {
                      errorLoading: () => t("SELECT__ERROR_LOADING"),
                      inputTooLong: ({ input, maximum }) => {
                        const overChars = input.length - maximum;
                        const message = t("SELECT__INPUT_TOO_LONG", {
                          overChars,
                        });
                        return message;
                      },
                      inputTooShort: ({ input, minimum }) => {
                        const remainingChars = minimum - input.length;
                        const message = t("SELECT__INPUT_TOO_SHORT", {
                          remainingChars,
                        });
                        return message;
                      },
                      loadingMore: () => t("SELECT__LOADING_MORE"),
                      maximumSelected: ({ maximum }) => {
                        const message = t("SELECT__MAXIMUM_SELECTED", {
                          maximum,
                        });
                        return message;
                      },
                      noResults: () => t("SELECT__NO_RESULTS"),
                      searching: () => t("SELECT__SEARCHING"),
                      removeAllItems: () => t("SELECT__REMOVE_ALL_ITEMS"),
                    },
                  }}
                />
              );
            }}
          />
        );

      case "editor":
        return (
          <Controller
            {...props}
            render={({ field }) => (
              <TextEditor
                {...field}
                id={name}
                invalid={isInvalid}
              />
            )}
          />
        );

      case "file":
        return (
          <Controller
            {...props}
            render={({ field }) => (
              <FileUpload
                {...field}
                id={name}
                invalid={isInvalid}
              />
            )}
          />
        );

      case "address":
        return (
          <Controller
            {...props}
            render={({ field }) => (
              <Address
                {...field}
                id={name}
                name={name}
                invalid={isInvalid}
                onChangeAddress={onChangeAddress}
              />
            )}
          />
        );

      default:
        return null;
    }
  };

  const defaultErrorMessage = () => {
    const labelText = label.charAt(0).toLowerCase() + label.slice(1);
    if (type === "select") {
      return t("REQUIRED__SELECT", { label: labelText });
    } else {
      return t("REQUIRED__INPUT", { label: labelText });
    }
  };

  return (
    <FormGroup>
      {label && (
        <label className="form-control-label" htmlFor={name}>
          {label}{" "}
          {isRequired && displayType !== "text" && (
            <span className="text-danger">*</span>
          )}
        </label>
      )}
      {renderInput()}
      {isInvalid && displayType !== "text" && (
        <div className="invalid-feedback d-block">
          {errorMessage || defaultErrorMessage()}
        </div>
      )}
    </FormGroup>
  );
};

FormInput.defaultProps = {
  label: "",
  name: "",
  type: "text",
  control: {},
  option: [],
  multiple: false,
  disabled: false,
  isSearchable: false,
  onChange: noop,
  displayType: "input",
  className: null,
  readonly: false,
};

FormInput.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  control: PropTypes.object,
  value: PropTypes.any,
  option: PropTypes.array,
  multiple: PropTypes.bool,
  disabled: PropTypes.bool,
  isSearchable: PropTypes.bool,
  onChange: PropTypes.func,
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  displayType: PropTypes.string,
  className: PropTypes.string,
  readonly: PropTypes.bool,
};

const IconEye = styled.i`
  position: absolute;
  top: 0px;
  right: 16px;
  font-size: 20px;
  color: #adb5bd;
  line-height: 46px;
  z-index: 3;
  cursor: pointer;
  &:hover {
    color: #999;
  }
`;

const InputPassword = (props) => {
  const [isShowPassword, setIsShowPassword] = useState(false);
  const toggleShowPassword = () => {
    setIsShowPassword(!isShowPassword);
  };
  return (
    <div className="position-relative">
      <Input {...props} type={isShowPassword ? "text" : "password"} />
      {props.displayType !== "text" && (
        <IconEye
          className={`fas fa-eye${isShowPassword ? "" : "-slash"}`}
          onClick={toggleShowPassword}
        ></IconEye>
      )}
    </div>
  );
};

InputPassword.defaultProps = {
  displayType: "",
};

InputPassword.propTypes = {
  displayType: PropTypes.string,
};

export default InputPassword;
