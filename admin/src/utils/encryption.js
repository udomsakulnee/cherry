import CryptoJS from "crypto-js";

export const encrypt = (plainText) => {
  if (plainText == null) {
    return null;
  } else {
    const secret = CryptoJS.lib.WordArray.create(
      "496a323f7bb18e3874a2a6b9fc8369d0"
    );
    const iv = CryptoJS.lib.WordArray.create(
      "0b0b2495c812e228363701b87fbbc1a8"
    );
    const cipherText = CryptoJS.AES.encrypt(plainText, secret, { iv: iv });
    return cipherText.toString();
  }
};

export const decrypt = (cipherText) => {
  if (cipherText == null) {
    return null;
  } else {
    const secret = CryptoJS.lib.WordArray.create(
      "496a323f7bb18e3874a2a6b9fc8369d0"
    );
    const iv = CryptoJS.lib.WordArray.create(
      "0b0b2495c812e228363701b87fbbc1a8"
    );
    const plainText = CryptoJS.AES.decrypt(cipherText, secret, { iv: iv });
    return plainText.toString(CryptoJS.enc.Utf8);
  }
};

export const hash = (plainText) => {
  if (plainText == null) {
    return null;
  } else {
    const cipherText = CryptoJS.SHA256(plainText);
    return cipherText.toString();
  }
};
