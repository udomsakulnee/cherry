import { decrypt, encrypt, hash } from "./encryption";

const parse = (jsonString) => {
  if (jsonString == null) {
    return null;
  } else {
    const [type, value] = JSON.parse(decrypt(jsonString));
    switch (type) {
      case "s":
        return String(value);
      case "n":
        return Number(value);
      case "b":
        return Boolean(value);
      case "o":
        return value;
      default:
        return value;
    }
  }
};

const stringify = (value) => {
  const data = [(typeof value).charAt(0), value];
  return encrypt(JSON.stringify(data));
};

export const getStorage = (key) => {
  const jsonString = localStorage.getItem(hash(key));
  return parse(jsonString);
};

export const setStorage = (key, value) => {
  localStorage.setItem(hash(key), stringify(value));
};

export const getSession = (key) => {
  const jsonString = sessionStorage.getItem(hash(key));
  return parse(jsonString);
};

export const setSession = (key, value) => {
  sessionStorage.setItem(hash(key), stringify(value));
};

export const clearSession = () => {
  sessionStorage.clear();
};
