export const passwordPattern =
  /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d*.!@#$%&(){}\\[\]:;<>,.?\\/~_+-=]{8,}$/;

export const checkPasswordStrength = (setState) => (e) => {
  const password = e.target.value;

  let strength = 0;
  if (password.match(/[a-z]+/)) {
    strength++;
  }
  if (password.match(/[A-Z]+/)) {
    strength++;
  }
  if (password.match(/[0-9]+/)) {
    strength++;
  }
  if (password.match(/[*.!@#$%&(){}\\[\]:;<>,.?\\/~_+-]+/)) {
    strength++;
  }

  switch (strength) {
    case 1:
      setState("WEAK");
      break;
    case 2:
      setState("MEDIUM");
      break;
    case 3:
      setState("STRONG");
      break;
    case 4:
      setState("VERY_STRONG");
      break;
    default:
      setState("NOT_FOUND");
  }
};
