import d from "dayjs";
import "dayjs/locale/th";
import advancedFormat from "dayjs/plugin/advancedFormat";
import buddhistEra from "dayjs/plugin/buddhistEra";
import customParseFormat from "dayjs/plugin/customParseFormat";
import updateLocale from "dayjs/plugin/updateLocale";
import relativeTime from "dayjs/plugin/relativeTime";
import { KEY_LOCALE } from "config";
import { getStorage } from "./storage";
import { LOCALE_THAI } from "i18n";

const buddhistYear = (option, dayjsClass, dayjsFactory) => {
  const oldFormat = dayjsClass.prototype.format;
  dayjsClass.prototype.format = function (pattern) {
    let format = pattern;
    if (dayjsFactory.locale() === "th") {
      format = format.replace(/Y/g, "B");
    }
    const result = oldFormat.bind(this)(format);
    return result;
  };
};

d.extend(advancedFormat);
d.extend(buddhistEra);
d.extend(customParseFormat);
d.extend(updateLocale);
d.extend(relativeTime);
d.extend(buddhistYear);

const defaultLocale = getStorage(KEY_LOCALE) || LOCALE_THAI;
d.locale(defaultLocale.toLowerCase());

d.updateLocale("th", {
  meridiem: (hour) => (hour > 11 ? "หลังเที่ยง" : "ก่อนเที่ยง"),
});

export const dayjs = d;
