'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const {findLimitController} = require('../../controllers');

module.exports = {
  async findLimit(ctx) {
    return await findLimitController('transaction-order', ctx);
  },
  
  async createOrder(ctx) {
    const {tOrderLists = [], totalQuantity = 0, totalPrice = 0, totalPoint = 0, shippingFee = 0, isPickup = false, paymentMethod = 'TRANSFER', address, user} = ctx.request.body;
  
    if (user && tOrderLists.length > 0) {
      const allProducts = await strapi.query('product').find();
      let isOk = true;
      let updateProducts = [];
      allProducts.map((product) => {
        const orderList = tOrderLists.filter((item) => String(product.id) === String(item.product));
        if (orderList.length > 0) {
          const order = orderList.reduce((a, b) => ({
            product: product.id,
            quantity: a.quantity + b.quantity,
          }));
          const quantity = product.stock.quantity - order.quantity;
          if (quantity < 0) isOk = false;
          updateProducts = [...updateProducts, {
            id: product.id,
            quantity: quantity
          }];
        }
      });

      if (isOk) {
        for (let i = 0; i < updateProducts.length; i++) {
          strapi.query('stock').update(
            {product: updateProducts[i].id},
            {
              quantity: updateProducts[i].quantity,
            }
          );
        }

        let orderLists = [];
        for (let j = 0; j < tOrderLists.length; j ++) {
          const {product, quantity = 0, totalPrice = 0, totalPoint = 0} = tOrderLists[j];
          const response = await strapi.query('transaction-order-list').create({
            product,
            quantity,
            totalPrice,
            totalPoint,
          });
          orderLists = [...orderLists, response.id];
        }
  
        const response = await strapi.query('transaction-order').create({
          user,
          tOrderLists: orderLists,
          totalQuantity,
          totalPrice,
          totalPoint,
          shippingFee,
          isPickup,
          paymentMethod,
          address,
          status: paymentMethod === 'COD' ? 'INPROGRESS' : 'PENDING',
        });
        return ctx.send(response);
      }
      return ctx.send([]);
    }
  
    return ctx.throw(404);
  }
};
