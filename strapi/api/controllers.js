'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

module.exports = {
  async findLimitController(contentType, ctx) {
    const total = await strapi.query(contentType).count({_publicationState: 'live', ...ctx.request.query});
    const entities = await strapi.query(contentType).find({_publicationState: 'live', ...ctx.request.query});
    const data = entities.map(entity => sanitizeEntity(entity, {model: strapi.models[contentType]}));
    return ctx.send({total, data});
  }
};
