'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async quantity(ctx) {
    const {id} = ctx.params;

    const receivedLists = await strapi.query('transaction-goods-received-list').find({product: id});
    let received = {
      quantity: 0,
      totalCost: 0,
    };
    if (receivedLists && receivedLists.length > 0) {
      received = receivedLists.reduce((a, b) => ({
        quantity: a.quantity + b.quantity,
        totalCost: a.totalCost + b.totalCost,
      }));
    }
  
    const issuedLists = await strapi.query('transaction-goods-issued-list').find({product: id});
    let issued = {
      quantity: 0
    };
    if (issuedLists && issuedLists.length > 0) {
      issued = issuedLists.reduce((a, b) => ({
        quantity: a.quantity + b.quantity,
      }));
    }

    const orderLists = await strapi.query('transaction-order-list').find({product: id});
    let order = {
      quantity: 0
    };
    if (orderLists && orderLists.length > 0) {
      order = orderLists.reduce((a, b) => ({
        quantity: a.quantity + b.quantity,
      }));
    }

    const stock = await strapi.query('stock').update(
      {product: id},
      {
        quantity: received.quantity - issued.quantity - order.quantity,
        averageCost: received.totalCost / received.quantity,
        product: id,
      }
    );
    return ctx.send(stock);
  }
};
  