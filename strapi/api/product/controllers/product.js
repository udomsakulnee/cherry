'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const {findLimitController} = require('../../controllers');

module.exports = {
  async findLimit(ctx) {
    return await findLimitController('product', ctx);
  }
};
