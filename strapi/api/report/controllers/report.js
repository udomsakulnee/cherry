'use strict';

const {sanitizeEntity} = require('strapi-utils');

/**
 * A set of functions called "actions" for `report`
 */

module.exports = {
  async findProductSales(ctx) {
    const entities = await strapi.query('transaction-order').find({_publicationState: 'live', ...ctx.request.query}, ['user', 'tOrderLists.product.stock']);
    const data = entities.map(entity => sanitizeEntity(entity, {model: strapi.models['transaction-order']}));
    ctx.send(data);
  },
};