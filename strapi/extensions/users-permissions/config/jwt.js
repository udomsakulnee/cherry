module.exports = {
  jwtSecret: process.env.JWT_SECRET || '192c97de-fa3a-46bc-94ca-0cdaf9576087',
  jwt: {
    expiresIn: '1800s',
  }
};