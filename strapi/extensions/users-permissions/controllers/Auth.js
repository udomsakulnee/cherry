const { sanitizeEntity } = require('strapi-utils');
const CryptoJS = require('crypto-js');

module.exports = {
  async refreshToken (ctx) {
    const {token} = ctx.request.body;
    const {id} = await strapi.plugins['users-permissions'].services.jwt.verify(token);
    const query = {
      id,
    };
    const user = await strapi.query('user', 'users-permissions').findOne(query);
    const jwt = strapi.plugins['users-permissions'].services.jwt.issue({
      id: user.id,
    });
    ctx.send({
      jwt,
      user: sanitizeEntity(user.toJSON ? user.toJSON() : user, {
        model: strapi.query('user', 'users-permissions').model,
      }),
    });
  },

  async getUser (ctx) {
    const {token} = ctx.request.body;
    const users = await strapi.query('user', 'users-permissions').find();
    const user = users.filter((user) => CryptoJS.SHA256(user.username).toString() === token);
    if (user.length === 1) {
      const {id, firstnameEN, firstnameTH, lastnameEN, lastnameTH, mLevel} = user.reduce(item => item);
      ctx.send({
        id,
        nameEN: `${firstnameEN} ${lastnameEN} (${mLevel.nameEN})`,
        nameTH: `${firstnameTH} ${lastnameTH} (${mLevel.nameTH})`,
      });
    }
    else {
      return ctx.throw(404);
    }
  }
};