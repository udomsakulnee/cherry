const { sanitizeEntity } = require('strapi-utils');

const formatError = error => [
  { messages: [{ id: error.id, message: error.message, field: error.field }] },
];

module.exports = {
  async getMe (ctx) {
    const authorization = ctx.request.headers.authorization;
    const token = authorization.replace('Bearer ', '');
    const {id} = await strapi.plugins['users-permissions'].services.jwt.verify(token);
    const query = {
      id,
    };
    const user = await strapi.query('user', 'users-permissions').findOne(query);
    ctx.send(
      sanitizeEntity(user, {
        model: strapi.query('user', 'users-permissions').model,
      }),
    );
  },
    
  async updateMe (ctx) {
    const authorization = ctx.request.headers.authorization;
    const data = ctx.request.body;
    const token = authorization.replace('Bearer ', '');
    const {id} = await strapi.plugins['users-permissions'].services.jwt.verify(token);
    const query = {
      id,
    };
    const user = await strapi.query('user', 'users-permissions').update(query, data);
    ctx.send(
      sanitizeEntity(user, {
        model: strapi.query('user', 'users-permissions').model,
      }),
    );
  },

  async passwordMe (ctx) {
    const authorization = ctx.request.headers.authorization;
    const {oldPassword, newPassword} = ctx.request.body;
    const token = authorization.replace('Bearer ', '');
    const {id} = await strapi.plugins['users-permissions'].services.jwt.verify(token);
    const {password} = await strapi.query('user', 'users-permissions').findOne({id});
    const validPassword = await strapi.plugins['users-permissions'].services.user.validatePassword(oldPassword, password);
    console.log('validPassword', validPassword);
    if (validPassword) {
      const password = await strapi.plugins['users-permissions'].services.user.hashPassword({password: newPassword});
      const user = await strapi.query('user', 'users-permissions').update({id}, {resetPasswordToken: null, password});
      ctx.send(
        sanitizeEntity(user, {
          model: strapi.query('user', 'users-permissions').model,
        }),
      );
    }
    else {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.password.matching',
          message: 'Passwords do not match.',
        })
      );
    }
  }
};