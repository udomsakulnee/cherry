# DEV CHERRY
https://strapi.io/documentation/developer-docs/latest/installation/digitalocean-one-click.html
# FRONT END
scp -r packages/admin/build/* root@159.65.140.185:/var/www/html/cherry
# BACK END ระวังโฟล์เดอร์ public
scp packages/strapi/package.json root@159.65.140.185:/srv/strapi/strapi-production
scp -r packages/strapi/api/* root@159.65.140.185:/srv/strapi/strapi-production/api
scp -r packages/strapi/extensions/* root@159.65.140.185:/srv/strapi/strapi-production/extensions

# ALLOW REMOTE POSTGRES
https://medium.com/@rimsovankiry/enable-remote-access-to-postgresql-ccb6c5199a47
vim /etc/postgresql/10/main/postgresql.conf
listen_addresses = '*'

vim /etc/postgresql/10/main/pg_hba.conf
host    all      all              0.0.0.0/0                    md5
host    all      all              ::/0                         md5

systemctl restart postgresql.service

sudo ufw enable
sudo ufw status verbose
sudo ufw allow 5432/tcp
https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-18-04